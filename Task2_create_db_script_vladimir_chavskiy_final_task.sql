--TASK 2. Create physical database design (DDL scripts for tables). Make sure your database is in 3NF and has meaningful keys 
--and constraints. Use ALTER TABLE to add constraints (except NOT NULL, UNIQUE and keys). Give meaningful names to your 
--CHECK constraints. Use DEFAULT and STORED AS where appropriate.
CREATE DATABASE realestate; --I've decided to create a new DB


--Creating all 12 tables
CREATE TABLE rental_payment
(
    payment_id SERIAL PRIMARY KEY, 
    rental_id INTEGER NOT NULL,
    amount NUMERIC(7,2) NOT NULL,
    payment_timestamp TIMESTAMPTZ NOT NULL
);

CREATE TABLE customer
(
    customer_id SERIAL PRIMARY KEY, 
    first_name VARCHAR NOT NULL, 
    last_name VARCHAR NOT NULL,
    patro_name VARCHAR NULL,       
    date_of_birth DATE NOT NULL,
    document_type INTEGER NOT NULL, 
    ssn VARCHAR NOT NULL UNIQUE,
    address_id INTEGER NOT NULL,
    tel_number VARCHAR NULL,
    email VARCHAR NULL
);

CREATE TABLE real_estate_agent
(
    agent_id SERIAL PRIMARY KEY, 
    first_name VARCHAR NOT NULL, 
    last_name VARCHAR NOT NULL,
    patro_name VARCHAR NULL,       
    date_of_birth DATE NOT NULL,
    document_type INTEGER NOT NULL, 
    ssn VARCHAR NOT NULL UNIQUE,
    address_id INTEGER NOT NULL,
    tel_number VARCHAR NOT NULL,
    email VARCHAR GENERATED ALWAYS AS (first_name||'.'||last_name||'.'||agent_id::TEXT||'@ESTATE.COM') STORED --There is no way to create GENERATED field by ALTER query
);

CREATE TABLE document_type
(
	document_type_id SERIAL PRIMARY KEY,
	document_type_title VARCHAR NOT NULL UNIQUE
);

CREATE TABLE rental
(
    rental_id SERIAL PRIMARY KEY,
    estate_id INTEGER NOT NULL,
    customer_id INTEGER NOT NULL,
    agent_id INTEGER NOT NULL,
    rental_date_start DATE NOT NULL,
    rental_date_end DATE NULL
);

CREATE TABLE sale
(
    sale_id SERIAL PRIMARY KEY,
    estate_id INTEGER NOT NULL,
    customer_id INTEGER NOT NULL,
    agent_id INTEGER NOT NULL,
    sale_date DATE NOT NULL
);

CREATE TABLE estate
(
    estate_id SERIAL PRIMARY KEY,
    address_id INTEGER NOT NULL,
    zone_id INTEGER NOT NULL,
    type_id INTEGER NOT NULL,
    rental_rate NUMERIC(7,2) NOT NULL,
    sale_cost NUMERIC(14,2) NOT NULL,
    number_of_rooms INTEGER NOT NULL,
    square NUMERIC(8,2) NOT NULL,
    sold BOOLEAN NOT NULL
);

CREATE TABLE sale_payment
(
    payment_id SERIAL PRIMARY KEY, 
    sale_id INTEGER NOT NULL,
    amount NUMERIC(14,2) NOT NULL,
    payment_timestamp TIMESTAMPTZ NOT NULL
);

CREATE TABLE address
(
	address_id SERIAL PRIMARY KEY,
	zip VARCHAR(9) NULL,    
    street VARCHAR(255) NULL,
    house VARCHAR NOT NULL,  
    house_index VARCHAR NULL,
    apartment VARCHAR NULL,
    city_id INTEGER NOT NULL
);

CREATE TABLE zone_type 
(
	zone_id SERIAL PRIMARY KEY,
	zone_title VARCHAR(255) NOT NULL,    
    zone_description TEXT NULL
);

CREATE TABLE estate_type 
(
	type_id SERIAL PRIMARY KEY,
	type_title VARCHAR(255) NOT NULL,    
    type_description TEXT NULL
);

CREATE TABLE city
(
	city_id SERIAL PRIMARY KEY,
	city VARCHAR(255) NOT NULL,    
    country_id INTEGER NOT NULL
);


CREATE TABLE country
(
	country_id SERIAL PRIMARY KEY,
	country VARCHAR(255) NOT NULL
);



--Adding  constraints. According to the task I was supposed to do it with ALTER query (no during creating tables).

ALTER TABLE rental_payment ALTER COLUMN payment_timestamp SET DEFAULT NOW();

ALTER TABLE customer ADD CONSTRAINT chk_customer_id_date_birth CHECK (date_of_birth <= CURRENT_DATE - INTERVAL '18 year');
													 			 
ALTER TABLE real_estate_agent ADD CONSTRAINT chk_agent_id_date_birth CHECK (date_of_birth <= CURRENT_DATE - INTERVAL '18 year');
													 			  	
ALTER TABLE rental ALTER COLUMN rental_date_start SET DEFAULT CURRENT_DATE;

ALTER TABLE sale ALTER COLUMN sale_date SET DEFAULT CURRENT_DATE;

ALTER TABLE estate ALTER COLUMN sold SET DEFAULT FALSE; --FALSE means estate is sold. When someone buys apartment/house you should show it with TRUE value.

ALTER TABLE sale_payment ALTER COLUMN payment_timestamp SET DEFAULT NOW();



--Adding FK
ALTER TABLE rental_payment
ADD FOREIGN KEY (rental_id) REFERENCES rental(rental_id);

ALTER TABLE customer
ADD FOREIGN KEY (address_id) REFERENCES address(address_id);

ALTER TABLE customer
ADD FOREIGN KEY (document_type) REFERENCES document_type(document_type_id);

ALTER TABLE real_estate_agent
ADD FOREIGN KEY (address_id) REFERENCES address(address_id);

ALTER TABLE real_estate_agent
ADD FOREIGN KEY (document_type) REFERENCES document_type(document_type_id);

ALTER TABLE rental
ADD FOREIGN KEY (customer_id) REFERENCES customer(customer_id);

ALTER TABLE rental
ADD FOREIGN KEY (estate_id) REFERENCES estate(estate_id);

ALTER TABLE rental
ADD FOREIGN KEY (agent_id) REFERENCES real_estate_agent(agent_id);

ALTER TABLE sale
ADD FOREIGN KEY (customer_id) REFERENCES customer(customer_id);

ALTER TABLE sale
ADD FOREIGN KEY (agent_id) REFERENCES real_estate_agent(agent_id);

ALTER TABLE sale
ADD FOREIGN KEY (estate_id) REFERENCES estate(estate_id);

ALTER TABLE estate
ADD FOREIGN KEY (address_id) REFERENCES address(address_id);

ALTER TABLE estate
ADD FOREIGN KEY (zone_id) REFERENCES zone_type(zone_id);

ALTER TABLE estate
ADD FOREIGN KEY (type_id) REFERENCES estate_type(type_id);

ALTER TABLE sale_payment
ADD FOREIGN KEY (sale_id) REFERENCES sale(sale_id);

ALTER TABLE address
ADD FOREIGN KEY (city_id) REFERENCES city(city_id);

ALTER TABLE city
ADD FOREIGN KEY (country_id) REFERENCES country(country_id);
