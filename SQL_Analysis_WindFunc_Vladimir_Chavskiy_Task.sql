--TASK 1. Build the query to generate a report about regions with the maximum number of products sold 
--(quantity_sold) for each channel for the entire period.

SELECT channel_desc,
	   country_region,
	   to_char(sales, '9,999,999,999,999.99') AS sales,
	   "SALES %"
FROM (
	SELECT ch.channel_desc,
		   c2.country_region,
		   sum(s.quantity_sold) AS sales, --number of sold units in distinct channel and country region
		   max(sum(s.quantity_sold)) OVER w AS sales_max, --Max sales value over channel (OVER w where 'w' is PARTITION BY ch.channel_desc)
		   to_char(sum(s.quantity_sold) / sum(sum(s.quantity_sold)) OVER w * 100, '990.99%') AS "SALES %" --percentage of maximum sales in this region (SALES column) of total sales per channel
	FROM sh.sales s
	JOIN sh.channels ch ON s.channel_id = ch.channel_id 
	JOIN sh.customers c ON s.cust_id = c.cust_id 
	JOIN sh.countries c2 ON c.country_id = c2.country_id
	GROUP BY ch.channel_desc, c2.country_region
	WINDOW w AS (PARTITION BY ch.channel_desc)) AS preparation
WHERE sales = sales_max; -- only max values from each channel get into final set



--TASK 2 Define subcategories of products (prod_subcategory) for which sales for 1998-2001 have always 
--been higher (sum(amount_sold)) compared to the previous year. The final dataset must include only one 
--column (prod_subcategory).
SELECT prod_subcategory_desc
	FROM (
		SELECT p2.prod_subcategory_desc,
			   p2.prod_subcategory_id, --It might be several subcategories with the same name (the same subcategory names refer to different categories)
			   t2.calendar_year, 
			   sum(amount_sold) AS amount, --amount for each prod_subcategory_id and calendar_year
			   LAG (sum(amount_sold),1, 0::numeric) OVER w AS amount_pre, --Unnecessary column (just for visualization and commenting). I replace NULL with 0::numeric because I'm going to compare values from different years further
			   sum(amount_sold) - LAG (sum(amount_sold),1, 0::numeric) OVER w AS diff --difference between current and previous year. IF diff < 0 then amount for previous year is bigger
		FROM sh.sales s2 
		JOIN sh.products p2 ON s2.prod_id = p2.prod_id
		JOIN sh.times t2 ON s2.time_id = t2.time_id
		WHERE t2.calendar_year IN (1998, 1999, 2000, 2001)
		GROUP BY p2.prod_subcategory_desc, 
				 p2.prod_subcategory_id, 
				 t2.calendar_year
		WINDOW w AS (PARTITION BY p2.prod_subcategory_id ORDER BY t2.calendar_year)
	) AS aux
GROUP BY prod_subcategory_desc, prod_subcategory_id
HAVING min(diff) > 0; -- If at least one diff value <=0 then at least one amount for previous year is equal or bigger amount for following year.

