--TASK1
--as usual, two approaches))
-- TOP 3 where it is supposed to show only 5 movies as TOP
SELECT c."name" AS top_most_selling_categories_USA, 
	   sum (p2.amount) total_dvd_rental_income, 
	   count(*) AS number_of_renting
FROM category c 
JOIN film_category fc ON fc.category_id = c.category_id
JOIN film f2 ON fc.film_id = f2.film_id
JOIN inventory i2 ON f2.film_id = i2.film_id
JOIN rental r2 ON i2.inventory_id = r2.inventory_id 
JOIN payment p2 ON r2.rental_id = p2.rental_id
JOIN customer c2 ON p2.customer_id = c2.customer_id
JOIN address a2 ON c2.address_id = a2.address_id
JOIN city c3 ON a2.city_id = c3.city_id 
JOIN country c4 ON c3.country_id = c4.country_id 
WHERE c4.country_id = 103 --the USA ID
GROUP BY c."name"
ORDER BY number_of_renting DESC
LIMIT 3;


--TOP3 whitch includes categories with equal number_of_renting
WITH limit_min AS (SELECT c."name" AS top_most_selling_categories_USA, 
	   					  sum (p2.amount) total_dvd_rental_income, 
	   					  count(*) AS number_of_renting
					FROM category c 
					JOIN film_category fc ON fc.category_id = c.category_id
					JOIN film f2 ON fc.film_id = f2.film_id
					JOIN inventory i2 ON f2.film_id = i2.film_id
					JOIN rental r2 ON i2.inventory_id = r2.inventory_id 
					JOIN payment p2 ON r2.rental_id = p2.rental_id
					JOIN customer c2 ON p2.customer_id = c2.customer_id
					JOIN address a2 ON c2.address_id = a2.address_id
					JOIN city c3 ON a2.city_id = c3.city_id 
					JOIN country c4 ON c3.country_id = c4.country_id 
					WHERE c4.country_id = 103 --the USA ID
					GROUP BY c."name"
					ORDER BY number_of_renting DESC
					LIMIT 3)

SELECT c."name" AS top_most_selling_categories_USA, 
	   sum (p2.amount) total_dvd_rental_income, 
	   count(*) AS number_of_renting
FROM category c 
JOIN film_category fc ON fc.category_id = c.category_id
JOIN film f2 ON fc.film_id = f2.film_id
JOIN inventory i2 ON f2.film_id = i2.film_id
JOIN rental r2 ON i2.inventory_id = r2.inventory_id 
JOIN payment p2 ON r2.rental_id = p2.rental_id
JOIN customer c2 ON p2.customer_id = c2.customer_id
JOIN address a2 ON c2.address_id = a2.address_id
JOIN city c3 ON a2.city_id = c3.city_id 
JOIN country c4 ON c3.country_id = c4.country_id 
WHERE c4.country_id = 103 --the USA ID
GROUP BY c."name"
HAVING COUNT(*) >= (SELECT min(number_of_renting)
					FROM limit_min lm)
ORDER BY number_of_renting DESC;


--Task2
SELECT c2.first_name ||' '|| c2.last_name AS customer_name, 
	   STRING_AGG( DISTINCT f.title, ', ') AS film_list,     --v2 DISTINCT has been added
	   SUM(p.amount) amount_of_money

FROM customer c2
JOIN rental r ON c2.customer_id = r.customer_id 
JOIN inventory i ON r.inventory_id = i.inventory_id 
JOIN film f ON i.film_id = f.film_id 
JOIN film_category fc ON f.film_id = fc.film_id
JOIN category c ON fc.category_id = c.category_id 
JOIN payment p ON r.rental_id = p.rental_id 
WHERE fc.category_id = 11 -- Horror ID
GROUP BY c2.customer_id;