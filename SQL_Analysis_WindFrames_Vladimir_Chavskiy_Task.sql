SELECT t.calendar_year,
	   t.calendar_quarter_desc,
	   p.prod_category,
	   to_char(sum(s.amount_sold), '9,999,999,999,999.99') AS "sales$", --sum of sales (amount_sold) for product category and quarter
	   CASE WHEN RIGHT (t.calendar_quarter_desc, 2) = '01' THEN 'N/A' --diff_percent is 'N/A' for the first quarter
	   		ELSE to_char(round(sum (s.amount_sold) / FIRST_VALUE(sum (s.amount_sold)) OVER (PARTITION BY t.calendar_year, p.prod_category  ORDER BY t.calendar_quarter_desc)*100-100, 2), '990.00%') --diff_percent is different between sales$ in current quarter and  sales$ in the first quarter 
	   END AS diff_percent,
	   to_char(sum(sum(s.amount_sold)) OVER (PARTITION BY t.calendar_year ORDER BY t.calendar_quarter_desc GROUPS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW), '9,999,999,999,999.99') AS "cum_sum$"--the cumulative sum of sales by quarters
FROM sh.sales s 
JOIN sh.times t ON s.time_id = t.time_id 
JOIN sh.products p ON s.prod_id = p.prod_id
JOIN sh.channels c ON s.channel_id = c.channel_id
WHERE p.prod_category IN ('Electronics', 'Hardware', 'Software/Other') AND --That are all needed filters according to the task
	  t.calendar_year IN (1999, 2000) AND
	  c.channel_desc IN ('Partners', 'Internet')
GROUP BY t.calendar_year,
	     t.calendar_quarter_desc,
	     p.prod_category
ORDER BY t.calendar_year, --Proper data order
	     t.calendar_quarter_desc,
	     p.prod_category;

