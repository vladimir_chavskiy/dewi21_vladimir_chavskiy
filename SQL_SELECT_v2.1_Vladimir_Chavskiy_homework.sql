--All comedy movies released between 2000 and 2004, alphabetical

SELECT f2.title AS all_comedies_released_between_2000_2004  --I've selected only title acording to the task "All comedy movies"
FROM
	film f2
JOIN film_list fl ON f2.title = fl.title                    --There is a need to filter movies with attributes from two tables. 'release_year' from 'film' and 'category' from 'film_list'
WHERE                                                       --Filter by 'release_year' and 'category'
	f2.release_year BETWEEN 2000 AND 2004
	AND UPPER(fl.category) = 'COMEDY'                       --UPPER is perfect for reducing the probability of error with uppercase and lowercase
ORDER BY                                                    --Ordering in an alphabetical way
	f2.title;
	


--Revenue of every rental store for year 2017 (columns: address and address2 � as one column, revenue)

SELECT CONCAT(a.address,' ', a.address2) AS store_adress, SUM(p.amount) AS revenue_for_2017
FROM
	payment p
JOIN staff s2 ON p.staff_id = s2.staff_id                                   --fll needed the information (address and amount of money) is in two tables which relate through two other tables   
JOIN store st ON s2.store_id = st.store_id
JOIN address a ON st.address_id = a.address_id
WHERE
	p.payment_date BETWEEN '2017-01-01 00:00:00' AND '2017-12-31 23:59:59'  -- revenue for 2017 accordingly the task
GROUP BY
	st.store_id, CONCAT(a.address,' ', a.address2);                         -- group by store_in and address (not only by address) because several stores might have the same address
	
	
	
--Top-3 actors by number of movies they took part in (columns: first_name, last_name, number_of_movies, sorted by number_of_movies in descending order)

SELECT a.first_name, a.last_name, COUNT(*) AS number_of_movies   --COUNT count all rows after JOIN for GROUP to count number of movies for each actor 
FROM
	actor a
JOIN film_actor fa ON a.actor_id = fa.actor_id                   --JOIN two tables to extract information about names and amount of movies
GROUP BY
	a.actor_id                                                   --actor_id is unique. Grouping by first_name + last_name might cause errors (several actors might have the same names)
ORDER BY
	number_of_movies DESC
LIMIT 3;



--Number of comedy, horror and action movies per year (columns: release_year, number_of_action_movies, number_of_horror_movies, number_of_comedy_movies), sorted by release year in descending order

SELECT
	f2.release_year AS release_year,
	SUM(CASE WHEN c2."name" = 'Action' THEN 1 ELSE 0 END) AS number_of_action_movies, --incrementing SUM by 1 every time to count all Action movies when 'Action' in record
	SUM(CASE WHEN c2."name" = 'Horror' THEN 1 ELSE 0 END) AS number_of_horror_movies,
	SUM(CASE WHEN c2."name" = 'Comedy' THEN 1 ELSE 0 END) AS number_of_comedy_movies
FROM
	film f2 
JOIN film_category fc ON f2.film_id = fc.film_id                                       --joining two tables (f2 and c2) through another table (fc) to get the attributes release_year and category together 
JOIN category c2 ON fc.category_id = c2.category_id
GROUP BY
	f2.release_year                                                                    --grouping movies by year
ORDER BY
	f2.release_year DESC;



--Which staff members made the highest revenue for each store and deserve a bonus for 2017 year?

WITH each_assistant_revenues AS (SELECT s2.first_name, s2.last_name, s2.staff_id, SUM(p2.amount) AS amount_of_money, s2.store_id 		--CTE to get revenues each staff members made. each_assistant_revenues contains information about all staff and all stores (not only the best shop assistent)
							 	 FROM payment p2
							 	 JOIN staff s2 ON p2.staff_id = s2.staff_id
							 	 GROUP BY
							 	 s2.store_id, s2.staff_id)
							 
SELECT ear.first_name ||' '|| ear.last_name as top_assistant_for_each_store, top_assistants.store_num, ear.amount_of_money 
FROM
                			(SELECT MAX(amount_of_money) AS max_revenue,    															--subquery extracts the best shop assistant for each shop. top_assistants contains one record for each store
                					store_id as store_num    
							 FROM each_assistant_revenues
							 GROUP BY store_id) AS top_assistants
JOIN each_assistant_revenues ear																										--JOIN to palce staff_id back (top_assistants doesn't contain staff_id)
ON ear.amount_of_money = top_assistants.max_revenue AND ear.store_id = top_assistants.store_num;					--JOIN to match sfatt_id with staff name									
						
						
						
--Which 5 movies were rented more than others and what's expected audience age for those movies?
--The 1st approach. It includes moveis with equal boundary values
WITH limit_min as (SELECT count(*) as count_aux													--CTE is for counting all rating values for getting into TOP5. Next it can be used for extracting boundary (min) rating value for getting into TOP5.
							FROM film f2 									
							JOIN inventory i2 ON f2.film_id = i2.film_id 
							JOIN rental r2 ON i2.inventory_id = r2.inventory_id
							GROUP BY f2.film_id										
							ORDER BY count_aux DESC										
							LIMIT 5)
							
							
SELECT f2.title AS top_5_rented_movies, count(*),
	   CASE
	   WHEN f2.rating = 'G' THEN '0+'
	   WHEN f2.rating = 'PG' THEN 'Parental guidance suggested'
	   WHEN f2.rating = 'PG-13' THEN '13+'
	   WHEN f2.rating = 'R' THEN '17+'
	   WHEN f2.rating = 'NC-17' THEN 'No One 17 & Under Admitted'
	   END AS expected_audience_age
FROM film f2
JOIN inventory i2 ON f2.film_id = i2.film_id 
JOIN rental r2 ON i2.inventory_id = r2.inventory_id
GROUP BY f2.film_id																				--group by film_id (not by title) because several moves might have the same name
HAVING COUNT(*) >= (SELECT min(lm.count_aux)
					FROM limit_min lm)
ORDER BY COUNT DESC;

--The 2nd approach. It excludes movies with equal boundary values. This approach is perfect for APP/web page/advertisement banner or something where where it is supposed to show only 5 movies as TOP. Additional criteria is revenue a movie has reached.
SELECT f2.title AS top_5_rented_movies, count(*), SUM (p2.amount),
	   CASE
	   WHEN f2.rating = 'G' THEN '0+'
	   WHEN f2.rating = 'PG' THEN 'Parental guidance suggested'
	   WHEN f2.rating = 'PG-13' THEN '13+'
	   WHEN f2.rating = 'R' THEN '17+'
	   WHEN f2.rating = 'NC-17' THEN 'No One 17 & Under Admitted'
	   END AS expected_audience_age
FROM film f2 
JOIN inventory i2 ON f2.film_id = i2.film_id 
JOIN rental r2 ON i2.inventory_id = r2.inventory_id
JOIN payment p2 ON r2.rental_id = p2.rental_id
GROUP BY f2.film_id, f2.rating												--group by film_id (not by title) because several moves might have the same name
ORDER BY COUNT DESC, SUM desc												--several movies might have the same COUNT value. In this case ordering by revenue (SUM(p2.amount)) helps to find movie that is better than other movies
LIMIT 5;
						
						
						
--Which actors/actresses didn't act for a longer period of time than others?

WITH 

aux AS (
SELECT																				  --aux auxiliary table to get together actor_id, film_id, release_year (title, first_name and last_name are just for visualuzation and checking)
	a.actor_id,
	f.release_year
FROM
	actor a
JOIN film_actor fa ON a.actor_id = fa.actor_id
JOIN film f ON fa.film_id = f.film_id), 

aux2021 AS (																		  --aux2021 auxiliary table shows carrier gap between last film and current date 
SELECT
	a.actor_id,
	EXTRACT (year from current_date) - max(f.release_year) AS period_without_action   -- max(f.release_year) is equal to the last date when an actor acted starred in a film. Current date - max(f.release_year) = period_in_years_without_action 
FROM actor a
JOIN film_actor fa ON a.actor_id = fa.actor_id
JOIN film f ON  fa.film_id = f.film_id
GROUP BY a.actor_id)


SELECT aux4.actor_id, a3.first_name || ' ' || a3.last_name as actor_name,                                                        -- We need actor_id to identify actors with the same name
	   CASE WHEN aux4.max_gap >= aux2021.period_without_action 
	   THEN aux4.max_gap 
	   ELSE aux2021.period_without_action END AS max_gap_in_career
FROM
	(SELECT aux3.actor_id, MAX(deff) as max_gap																					  --finding max gap between all moves for each actor. The interval "current_date - last_film_release" is not taken into account yet
	 FROM
			(SELECT aux2.actor_id, aux2.release_y, 
			 MIN(deff) as deff														      										  --collapse table with release_year_filmX-release_year_filmY = min. As result we have all gaps between all movies for each actor
			 FROM
							(SELECT a1.actor_id,                                                                                  --CROSS JOIN to get all variations of release years joining one to one
									a1.release_year AS release_y, 
									a1.release_year - a2.release_year as deff 
							 FROM aux a1
							 CROSS JOIN aux a2
							 WHERE a1.actor_id = a2.actor_id AND a1.release_year > a2.release_year) AS aux2
			 GROUP BY actor_id, release_y) AS aux3
	GROUP BY actor_id) AS aux4
JOIN actor a3 ON aux4.actor_id = a3.actor_id
JOIN aux2021 ON aux2021.actor_id = a3.actor_id
ORDER BY max_gap DESC;

----Which actors/actresses didn't act for a longer period of time than others? If we need period without action from the last film

SELECT
	a.first_name || ' ' || a.last_name as actor_name,
	EXTRACT (year from current_date) - max(f.release_year) AS period_in_years_without_action   -- max(f.release_year) is equal to the last date when an actor acted starred in a film. Current date - max(f.release_year) = period_in_years_without_action 
FROM actor a
JOIN film_actor fa ON a.actor_id = fa.actor_id
JOIN film f ON  fa.film_id = f.film_id
GROUP BY a.actor_id
ORDER BY period_in_years_without_action DESC;
