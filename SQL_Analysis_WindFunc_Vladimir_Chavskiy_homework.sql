SET search_path TO sh; -- to set sh schema
--TASK 1. Build the query to generate a report about the most significant customers (which have maximum 
--sales) through various sales channels. The 5 largest customers are required for each channel. Column 
--sales_percentage shows percentage of customerís sales within channel sales 
SELECT info.channel_desc,
	   info.cust_id,
	   info.cust_last_name, 
	   info.cust_first_name,
	   info.cust_amount,
	   round(info.percent_in_cat, 5) AS sales_percentage --leaving 5 digits after '.' according to math rules.
FROM 
	(SELECT c2.channel_desc,
		   c.cust_id,
		   c.cust_last_name, 
		   c.cust_first_name,
		   sum(s.amount_sold) cust_amount,  --It counts amount money exactly for each customer in each CHANNEL because of GROUP BY clause below
		   sum(s.amount_sold) / (sum(sum(s.amount_sold)) OVER (PARTITION BY c2.channel_desc))*100 AS percent_in_cat, --A window function counts whole amount of money for each channel
		   RANK () OVER (PARTITION BY c2.channel_desc ORDER BY sum(s.amount_sold) DESC, c.cust_id) AS rank_value --My approach is bases on finding absolutely top5 customers for each channel. It means more than 1 customer might take fifth place but anyway only 5 customers get into TOP5. A customer with lowest get into TOP5 amount if there are more than 1 customer at the fifth place.
	 FROM sales s 
	 JOIN customers c ON s.cust_id = c.cust_id 
	 JOIN channels c2 ON s.channel_id = c2.channel_id 
	 GROUP BY c2.channel_desc, c.cust_id) AS info
WHERE rank_value <= 5;



--TASK 2. Compose query to retrieve data for report with sales totals for all products in Photo category 
--in Asia (use data for 2000 year). Calculate report total (YEAR_SUM).
CREATE EXTENSION IF NOT EXISTS tablefunc; -- installing an extension tablefunc

SELECT result_tab.product_name,
	   COALESCE (result_tab.q1, 0) AS q1, --It might by situation when some product has never been sold in a quartet
	   COALESCE (result_tab.q2, 0) AS q2,
	   COALESCE (result_tab.q3, 0) AS q3,
	   COALESCE (result_tab.q4, 0) AS q4,
	   COALESCE (result_tab.q1, 0)+ COALESCE (result_tab.q2, 0)+COALESCE (result_tab.q3, 0)+ COALESCE (result_tab.q4, 0) year_sum
FROM crosstab (
	$$
	SELECT p2.prod_name,  								--This query is to get data about all PHOTO products for ASIA in 2000. 
		   t2.calendar_quarter_number AS calendar_num,
		   sum (s2.amount_sold)
	FROM products p2 
	JOIN sales s2 ON p2.prod_id = s2.prod_id
	JOIN times t2 ON s2.time_id = t2.time_id 
	JOIN customers c2 ON s2.cust_id = c2.cust_id 
	JOIN countries c3 ON c2.country_id = c3.country_id
	WHERE p2.prod_category = 'Photo' AND
		  c3.country_subregion = 'Asia' AND 
		  t2.calendar_year = 2000
	GROUP BY p2.prod_id, t2.calendar_quarter_number
	ORDER BY p2.prod_name, t2.calendar_quarter_number$$ --to avoid unpredictable creating columns result
	) AS result_tab (product_name varchar, 
					 q1 numeric,
					 q2 numeric,
					 q3 numeric,
					 q4 numeric);

																				  
																				  
--TASK 3. Build the query to generate a report about customers who were included into TOP 300 (based on 
--the amount of sales) in 1998, 1999 and 2001. This report should separate clients by sales channels, and, 
--at the same time, channels should be calculated independently (i.e. only purchases made on selected 
--channel are relevant).
SELECT DISTINCT top_300.channel_desc, 
			    top_300.cust_id, 
				top_300.cust_last_name, 
				top_300.cust_first_name,
				top_300.amount_whole
FROM
		(SELECT all_rank.*,
			    max(all_rank.top_rank) OVER (PARTITION BY all_rank.cust_id, all_rank.channel_desc) AS max_rank, --'customers who were included into TOP 300 (based on the amount of sales) in 1998, 1999 and 2001' means we should 1. either check all ranks for all periods with condition it is less than 300 or not 2. or find MAX rank and if it >400 than customer "out of the game"
			    count(*) OVER (PARTITION BY all_rank.cust_id, all_rank.channel_desc) --Checking if customer bought any goods through channel in each year. COUNT(*)<3 means customer bought nothing in within some year or years. 
		 FROM
			   (SELECT c2.channel_desc, 
					   c.cust_id, 
					   c.cust_last_name, 
					   c.cust_first_name,
					   t2.calendar_year,
					   sum(s.amount_sold) AS amount_year, --amount of money for each customer in each CHANNEL and each CALENDAR_YEAR
					   sum(sum(s.amount_sold)) OVER (PARTITION BY c2.channel_desc, c.cust_id) amount_whole, --total customer amount of money for each CHANNEL and whole period (1998 + 1999 + 2001)
					   row_number () OVER (PARTITION BY c2.channel_desc, t2.calendar_year ORDER BY sum(s.amount_sold) DESC) AS top_rank --ranking all customers within each CHANNEL and CALENDAR_YEAR
				FROM sales s 
				JOIN customers c ON s.cust_id = c.cust_id 
				JOIN channels c2 ON s.channel_id = c2.channel_id
				JOIN times t2 ON s.time_id = t2.time_id 
				WHERE t2.calendar_year IN (1998, 1999, 2001) --According to a task "customers who were included into TOP 300 (based on the amount of sales) in 1998, 1999 and 2001"
				GROUP BY c2.channel_desc, c.cust_id, t2.calendar_year
			    )AS all_rank
		 )AS top_300
WHERE top_300.max_rank <= 300 AND top_300.count = 3 --I've describe it before in an ALL_RANK subquery (in a SELECT section)
ORDER BY top_300.amount_whole DESC; --ordering by amount of money


--One more approach for the TASK 3
--I've mentioned before "'customers who were included into TOP 300 (based on the amount of sales) in 1998, 1999 and 2001' means we should 1. either check all ranks for all periods with condition it is less than 300 or not 2. or find MAX rank and if it >400 than customer "out of the game"
--This approach is based on statement "we should check all ranks for all periods with condition it is less than 300 or not"
SELECT aux.channel_desc, 
	   aux.cust_id, 
	   aux.cust_last_name, 
	   aux.cust_first_name,
	   aux.sum_1 + aux.sum_2 + aux.sum_3 AS amout_sold
FROM
	(SELECT c2.channel_desc, 
		   c.cust_id, 
		   c.cust_last_name, 
		   c.cust_first_name,
		   sum(CASE WHEN t2.calendar_year = 1998 THEN s.amount_sold ELSE 0 END) AS sum_1, --three SUM to calculate each customer amount of money for each channel_desc and each cust_id
		   sum(CASE WHEN t2.calendar_year = 1999 THEN s.amount_sold ELSE 0 END) AS sum_2,
		   sum(CASE WHEN t2.calendar_year = 2001 THEN s.amount_sold ELSE 0 END) AS sum_3,
		   ROW_NUMBER () OVER (PARTITION BY c2.channel_desc ORDER BY (sum(CASE WHEN t2.calendar_year = 1998 THEN s.amount_sold ELSE 0 END)) DESC) AS top_1, --ranking all customers within each CHANNEL and one CALENDAR_YEAR
		   ROW_NUMBER () OVER (PARTITION BY c2.channel_desc ORDER BY (sum(CASE WHEN t2.calendar_year = 1999 THEN s.amount_sold ELSE 0 END)) DESC) AS top_2,
		   ROW_NUMBER () OVER (PARTITION BY c2.channel_desc ORDER BY (sum(CASE WHEN t2.calendar_year = 2001 THEN s.amount_sold ELSE 0 END)) DESC) AS top_3
	FROM sales s 
	JOIN customers c ON s.cust_id = c.cust_id 
	JOIN channels c2 ON s.channel_id = c2.channel_id
	JOIN times t2 ON s.time_id = t2.time_id 
	WHERE t2.calendar_year IN (1998, 1999, 2001)
	GROUP BY c2.channel_desc, c.cust_id) AS aux
WHERE top_1 <= 300 AND top_2 <= 300 AND top_3 <= 300 AND --checking if all ranks for all periods are less than 300 or not
	  aux.sum_1 * aux.sum_2 * aux.sum_3 != 0 --Checking if customer bought any goods through channel in each year. It is equal to 0 if customer bought nothing in within some year or years. 
ORDER BY amout_sold DESC;



--TASK 4. Build the query to generate the report about sales in America and Europe:
WITH crosstb_to_sep AS (
    SELECT *
    FROM crosstab (
       	$$
       	SELECT calendar_month_desc ||'_'|| prod_category, --There are two ROW_NAMEN. My approach is joining ROW_NAME 'month' and ROW_NAME 'category' then create 'regular' crosstab (1xROWA_NAME 1xCATEGORY 1xVALUE) and that separate them (month and category) back.  
       		   country_region,
       		   sales_amount
       	FROM (
		         SELECT t2.calendar_month_desc, --query to get sales in AMERICAS and EUROPE region in 01-03 month (2000) through all categories
				   	    p.prod_category,
				   	    c2.country_region,
					    sum (s.amount_sold) sales_amount
				 FROM sales s
				 JOIN times t2 ON s.time_id = t2.time_id 
				 JOIN products p ON s.prod_id = p.prod_id
				 JOIN customers c ON s.cust_id = c.cust_id 
				 JOIN countries c2 ON c.country_id = c2.country_id
				 WHERE t2.calendar_month_desc IN ('2000-01', '2000-02', '2000-03') AND
				       c2.country_region IN ('Americas', 'Europe')
				 GROUP BY t2.calendar_month_desc, p.prod_category, c2.country_region
		) AS amout_by_country_month_category
		ORDER BY 1,2
        $$,
        $$VALUES ('Americas'), ('Europe')$$ --Category names order
    ) AS ct(date_category text, Americas numeric, Europe numeric)
)

SELECT split_part(date_category, '_', 1) as calendar_month_desc, --This query gets '2000-01' from '2000-01_Electronics' 
       split_part(date_category, '_', 2) as prod_category, --This query gets 'Electronics' from '2000-01_Electronics' 
       round(COALESCE(Americas, 0), 0) AS americas_sales, --Change NULL into '0' in case if there is no sales in a region
       round(COALESCE(Europe, 0), 0) AS europe_sales
FROM crosstb_to_sep;

	

--The 2nd approach. It is based creating columns for Americas and Europe region 
--using window functions and then getting distinct rows 
SELECT DISTINCT *
FROM
	(SELECT t2.calendar_month_desc, 
		   p.prod_category,
		   round(sum (CASE WHEN c2.country_region = 'Americas' THEN s.amount_sold ELSE 0 END) OVER (PARTITION BY t2.calendar_month_desc, p.prod_category), 0) AS americas_sales,
		   round(sum (CASE WHEN c2.country_region = 'Europe' THEN s.amount_sold ELSE 0 END) OVER (PARTITION BY t2.calendar_month_desc, p.prod_category), 0) AS europe_sales
	FROM sales s
	JOIN times t2 ON s.time_id = t2.time_id 
	JOIN products p ON s.prod_id = p.prod_id
	JOIN customers c ON s.cust_id = c.cust_id 
	JOIN countries c2 ON c.country_id = c2.country_id
	WHERE t2.calendar_month_desc IN ('2000-01', '2000-02', '2000-03')) aux
ORDER BY calendar_month_desc, prod_category;