--TASK 3. Read about row-level security (https://www.postgresql.org/docs/12/ddl-rowsecurity.html) and configure it for your database, so that the
--customer can only access his own data in "rental" and "payment" tables (verify using the personalized role you previously created)

ALTER TABLE payment ENABLE ROW LEVEL SECURITY; --queries to switch on ROW LEVEL SECURITY 
ALTER TABLE rental ENABLE ROW LEVEL SECURITY;


ALTER ROLE developer BYPASSRLS; --As ROW LEVEL SECURITY is enable we should pay attention about all roles which need to access to tables with RLS.
								--I've applied BYPASSRLS attribute for a developer role. And now a developer role has access to tables bypassing policy  We can use USING condition as well.
								--But USING requires attention about all roles when we create a new POLICY.
								--I used USING for a tester role just to practice several conditions inside USING.
								
GRANT SELECT ON customer TO customer; --As a customer refers to payments and rentals through USING conditional by the first name and the last name
									  --the customer has to have access to CUSTOMER table because neither payment nor rental table don't contain
									  --the first name and the last name. They only  contain customer_id. 

ALTER TABLE customer ENABLE ROW LEVEL SECURITY; --It allows customers to select rows is belonged to them only.
CREATE POLICY customer_policy ON customer 
USING (lower('client_'||customer.first_name||'_'||customer.last_name) = current_user 
	   OR current_user = 'tester');				--I commented on testers below. It was just to practice.



GRANT SELECT ON payment TO customer;

CREATE POLICY customer_policy_pay ON payment
USING (customer_id IN (SELECT customer_id FROM customer c) --IN is for case if there are two and more customers with the same name. It will be great to add ID into USERNAME to prevent situation like this.
	   OR current_user = 'tester');						   --But a role name must be client_{first_name}_{last_name} according to the task. 

	  
	  
GRANT SELECT ON rental TO customer;

CREATE POLICY customer_policy_rent ON rental 
USING (customer_id IN (SELECT customer_id FROM customer c)
	   OR current_user = 'tester');


--Queries for checking are below.
SET ROLE postgres;
SET ROLE customer; 
SET ROLE client_MARY_SMITH;
SET ROLE developer;
SET ROLE tester;

SELECT * FROM public.film;
SELECT * FROM public.actor;
SELECT * FROM public.payment;
SELECT * FROM public.category;
SELECT * FROM public.rental;
SELECT * FROM public.customer;

DO --DO block to insert and then update a row in the payment table.

$$

DECLARE

	pay_id integer;
	
BEGIN
	
	INSERT INTO public.payment (customer_id, staff_id, rental_id, amount, payment_date)
	VALUES ((SELECT customer_id FROM public.customer LIMIT 1), 1, (SELECT rental_id FROM public.rental LIMIT 1), 1.99, to_date('2017-02-21', 'YYYY-MM-DD'))
	RETURNING payment_id INTO pay_id;

	RAISE NOTICE 'A new payment % has been added', pay_id;

	UPDATE public.payment 
	SET staff_id = 2
	WHERE payment_id = pay_id;

	RAISE NOTICE 'An payment % has been updated', pay_id;
	
END $$;