--Task_1_1 Choose your top-3 favorite movies and add them to 'film' table. Fill rental rates with 4.99, 9.99 and 19.99 and rental durations with 1, 2 and 3 weeks respectively
 
			-- I tried to insert records using different ways
			-- Following query does not include several attributes which might have NULL or NOT NULL but DEFAULT value to observe that attribute values after performing INSERT query.
          	-- If we examine the database we can make conclusion: rental_duration in the DB is period of time represented in days
INSERT INTO film (title, release_year, language_id, rental_duration, rental_rate, length, rating)
VALUES ('CATCH ME IF YOU CAN',2002, 1, 7, 4.99, 141, 'PG-13')
RETURNING *;

		  	--Here I played around inserting two rows with one INSERT query, UPPER, default value for the first row and manual value for the second row (replacement_cost atribute)  
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features)
VALUES ('FORREST GUMP', 'Forrest Gump is a simple man with a low I.Q. but good intentions. He is running through childhood with his best and only friend Jenny.' , 1994, 1, 14, 9.99, 142, DEFAULT, 'PG-13', '{Drama}'),
	   (UPPER('The Usual Suspects'), 'Following a truck hijack in New York, five criminals are arrested and brought together for questioning. As none of them are guilty, they plan a revenge operation against the police.', 1995, 1, 21, 19.99, 105, 20.05, 'R', '{snull}')
RETURNING *;

	  
--Task_1_2 Add actors who play leading roles in your favorite movies to 'actor' and 'film_actor' tables (6 or more actors in total)	  
										 
INSERT INTO actor (first_name, last_name) 
VALUES ('LEO NARDO', 'DICAPRIO'),					     -- CATCH ME IF YOU CAN
	   (UPPER('Tom'), UPPER('Hanks')),					 -- CATCH ME IF YOU CAN, FORREST GUMP
	   ('CHRISTOPHER', 'WALKEN'),						 -- CATCH ME IF YOU CAN
	   ('KEVIN', 'SPACEY'),								 -- THE USUAL SUSPECTS
	   ('KEVIN', 'POLLAK'),								 -- THE USUAL SUSPECTS
	   ('BENICIO', 'DEL TORO')							 -- THE USUAL SUSPECTS
RETURNING *;	

       --Next I inserted records in the film_actor table using different approaches (INSERT+SELECT+JOIN, INSERT+SELECT+WHERE, INSERT+SELECT+WHERE+UNION)
	   
INSERT  INTO film_actor (actor_id, film_id)
SELECT actor_id, film_id 
FROM actor a 
JOIN film f ON a.first_name||a.last_name = 'LEO NARDODICAPRIO' AND f.title = 'CATCH ME IF YOU CAN'
RETURNING *;

INSERT  INTO film_actor (actor_id, film_id)
SELECT a.actor_id, f.film_id 
FROM actor a, film f 
WHERE a.first_name||a.last_name = 'TOMHANKS' AND f.title = 'CATCH ME IF YOU CAN'
RETURNING *;

INSERT  INTO film_actor (actor_id, film_id)
SELECT a.actor_id, f.film_id 
FROM actor a, film f 
WHERE a.first_name||a.last_name = 'TOMHANKS' AND f.title = 'FORREST GUMP'
RETURNING *;

INSERT  INTO film_actor (actor_id, film_id)
SELECT a.actor_id, f.film_id 
FROM actor a, film f 
WHERE a.first_name||a.last_name = 'CHRISTOPHERWALKEN' AND f.title = 'CATCH ME IF YOU CAN'
RETURNING *;

INSERT  INTO film_actor (actor_id, film_id)
SELECT a.actor_id, f.film_id 
FROM actor a, film f 
WHERE a.first_name||a.last_name = 'KEVINSPACEY' AND f.title = 'THE USUAL SUSPECTS'

UNION 

SELECT a.actor_id, f.film_id 
FROM actor a, film f 
WHERE a.first_name||a.last_name = 'KEVINPOLLAK' AND f.title = 'THE USUAL SUSPECTS'

UNION

SELECT a.actor_id, f.film_id 
FROM actor a, film f 
WHERE a.first_name||a.last_name = 'BENICIODEL TORO' AND f.title = 'THE USUAL SUSPECTS'
RETURNING *;
	  
	  
--Task_1_3 Add your favorite movies to any store's inventory.

INSERT INTO inventory (film_id, store_id)
SELECT film_id, 2
FROM film f 
WHERE f.title = 'CATCH ME IF YOU CAN'

UNION

SELECT film_id, 1
FROM film f 
WHERE f.title = 'CATCH ME IF YOU CAN'

UNION

SELECT film_id, 1
FROM film f 
WHERE f.title = 'FORREST GUMP'

UNION

SELECT film_id, 1
FROM film f 
WHERE f.title = 'THE USUAL SUSPECTS'
RETURNING *;



--Task_1_4 Alter any existing customer in the database who has at least 43 rental and 43 payment records. Change his/her personal data to yours (first name,

			-- This query to find all customers in the database which have at least 43 rental and 43 payment records
SELECT rn.customer_id, COUNT (*) AS pay_num             
FROM  
	  (SELECT c.customer_id, COUNT(*) AS rental_num
	   FROM customer c
			JOIN rental r ON c.customer_id = r.customer_id 
			GROUP BY c.customer_id
			HAVING COUNT(*)>=43                            -- All customers which have at least 43 rental records
	   ) AS rn
JOIN payment p ON rn.customer_id = p.customer_id
GROUP BY rn.customer_id
HAVING COUNT(*)>=43;								   	   -- All customers which have at least 43 payment records

			--If numder of rental records = number of payment records we can you following approach. I mean like in the task 1_4 (we should find customers which have >= 43 payment records). We can use it because one payment record is supposed to existing at least one rental record. 
			--But previous query is better in case we need for instance >=50 rental records and >=43 payment records
SELECT customer_id 
FROM payment p
GROUP BY customer_id 
HAVING COUNT (customer_id)>=43;

            -- My choice is a customer with customer_id = 477.
UPDATE customer 
SET store_id = 2, first_name = 'VLADIMIR', last_name = 'CHAVSKIY', email = 'vladimir.l.chavskiy@gmail.com', address_id = 32, create_date = CURRENT_DATE   --The value '32' as the address id has been received with randomizer
WHERE customer_id = 477;



--Task_1_5 Remove any records related to you (as a customer) from all tables except 'Customer' and 'Inventory'

          	-- I could remove a record from address table but it might belong to another person moreover it is. From this point of view it is prohibited to remove an address record from address table.
DELETE FROM payment        -- "delete all payment records" sounds crazy, but we have to do it
WHERE customer_id = 477;

DELETE FROM rental
WHERE customer_id = 477;



--Task_1_6 Rent you favorite movies from the store they are in and pay for them (add corresponding records to the database to represent this activity)

--All DVDs have to have an unique id that holds information about film title and shop film is stored in. It's inventory_id. My approach is marking all DVDs by a bar/QR-code which contains inventory_id information. 
--It works in following way: a shop assistant scan code, APP transfers inventory_id into DBCS and DBCS inserts a new record into rental table. 
--Returning process is performed with inventory_id as well.


		--Checking. There is any DVD copy of CATCH ME IF YOU CAN on stock or not
WITH 
num_dvd_copies AS (SELECT count(*) AS dvd_copies                 --counting numbers of 'CATCH ME IF YOU CAN' DVD copies
				   FROM inventory i2
				   WHERE i2.store_id = 1 AND i2.film_id in 
      					(SELECT film_id
						 FROM film
						 WHERE title = 'CATCH ME IF YOU CAN')
                  ),

num_in_rent AS (SELECT SUM(CASE WHEN r.return_date IS NULL THEN 1 ELSE 0 END) AS in_rent   --counting numbers of 'CATCH ME IF YOU CAN' DVD copies in rent
				FROM rental r 															   --I check returning date. If it's NULL then a DVD copy out of the stock
				WHERE r.inventory_id IN (SELECT i3.inventory_id
				FROM inventory i3
				WHERE i3.store_id = 2 AND i3.film_id IN 
        				(SELECT film_id
        				FROM film
        				WHERE title = 'CATCH ME IF YOU CAN')
		                )
		       )
	
SELECT CASE
	   		WHEN dc.dvd_copies =0 THEN 'There is not a movie with such title at stock'
	        WHEN dc.dvd_copies - in_rent > 0 OR dc.dvd_copies - ir.in_rent IS NULL THEN 'Great. The movie is ready to be rented'
    		WHEN dc.dvd_copies - in_rent = 0 AND dc.dvd_copies != 0 THEN 'Oh no. There is not any DVD copy at stock'
    		ELSE 'No-no-no-no an accountant is going to kill us. We have less DVD copies in stock in total then we have rented out ' 
       END AS status
FROM num_dvd_copies dc, num_in_rent ir



		--Next I'm visiting Store_id=2 and going to rent  CATCH ME IF YOU CAN
INSERT INTO rental (rental_date, inventory_id, customer_id, staff_id)
SELECT CURRENT_TIMESTAMP, i2.inventory_id, 477, s.staff_id 
FROM inventory i2 
JOIN film f2 ON i2.film_id = f2.film_id AND f2.title = 'CATCH ME IF YOU CAN'
JOIN staff s ON i2.store_id = s.store_id
WHERE i2.store_id = 2 AND NOT EXISTS  --customer is able to rent movie If inventory_id does not exist in RENTED id 
		(SELECT i.inventory_id    --getting all inventory_id we do not have on stock
		 FROM inventory i 
		 JOIN film f ON i.film_id = f.film_id AND f.title = 'CATCH ME IF YOU CAN'
		 JOIN rental r2 ON i.inventory_id = r2.inventory_id AND r2.return_date IS NULL 
		 WHERE i2.inventory_id=i.inventory_id) 
LIMIT (1)
RETURNING *;

		   --Creating partitions of payment table for January and February of 2021
CREATE TABLE payment_p2021_01 PARTITION OF payment
FOR VALUES FROM ('2021-01-01 00:00:00+3:00') TO ('2021-02-01 00:00:00+3:00');

ALTER TABLE payment_p2021_01 OWNER TO postgres;

CREATE TABLE payment_p2021_02 PARTITION OF payment
FOR VALUES FROM ('2021-02-01 00:00:00+3:00') TO ('2021-03-01 00:00:00+3:00');

ALTER TABLE payment_p2021_02 OWNER TO postgres;

	--Returning process
WITH rent_id_in_rent AS
	(SELECT r2.rental_id   --I need any rental_id with return_date = NULL + for 'CATCH ME IF YOU CAN' + for my (customer_id=477) 
	 FROM rental r2
	 JOIN
		(SELECT i3.inventory_id
	 	 FROM inventory i3
	 	 WHERE i3.store_id = 2 AND i3.film_id IN 
     	   		(SELECT film_id
				 FROM film
				 WHERE title = 'CATCH ME IF YOU CAN')
		) iid ON r2.inventory_id = iid.inventory_id AND r2.customer_id =477 AND r2.return_date IS NULL
LIMIT 1
	 )
		                
UPDATE rental 
SET return_date = CURRENT_TIMESTAMP
WHERE rental_id = (SELECT rental_id FROM rent_id_in_rent)
RETURNING *;
          
	 -- Creating payment record
WITH my_rent AS                   -- I need rental_id to insect new record into payment
	(SELECT r2.rental_id   
	 FROM rental r2
	 JOIN (SELECT i3.inventory_id
	 	   FROM inventory i3
	 	   WHERE i3.store_id = 2 AND i3.film_id in 
     	   		(SELECT film_id
				 FROM film
				 WHERE title = 'CATCH ME IF YOU CAN')
		   ) iid ON r2.inventory_id = iid.inventory_id AND r2.customer_id =477
	 ORDER BY r2.last_update desc --I'm looking for the last updated row for 'CATCH ME IF YOU CAN' + for my (customer_id=477) 
 	 LIMIT 1
	 )		   


INSERT INTO payment (customer_id, staff_id, rental_id, amount, payment_date) --Inserting row from SELECT into table
SELECT * 
FROM (SELECT 447 AS customer_id,							-- That is my ID
	   		 2 AS staff_id,                                 -- A shop assistant with staff_id=2 logged in a payment system when I was returning DVD
	   		 aux.rental_id,
	   		 CASE WHEN aux.days_num <= aux.rental_duration THEN aux.rental_rate                                           --1) Customer is supposed to pay rental_rate within a rental_duration pireod
	   			  WHEN aux.days_num > aux.rental_duration THEN aux.rental_rate+(aux.days_num - aux.rental_duration)       --2) Next rental cost is ONE DOLLAR FOR EVERY DAY
	  		 END AS amount,																								  --3) I've matched amount of money from DB and amount I've counted but I did not find evidence of following statement IF A FILM IS MORE THAN RENTAL_DURATION * 2 OVERDUE, CHARGE THE REPLACEMENT_COST
	   		 CURRENT_TIMESTAMP AS payment_date
	  FROM (SELECT r2.rental_id,                                             --Extracting rental period in days. I need rental_rate, rental_duration as well (for counting amount of money) 
	  	   		   EXTRACT(DAY FROM r2.return_date - r2.rental_date) AS days_num,
	  	   		   f2.rental_rate,
	  	   		   f2.rental_duration
	       	FROM rental r2
	       	JOIN inventory i2 ON r2.inventory_id = i2.inventory_id 
	       	JOIN film f2 ON i2.film_id = f2.film_id 
	       	JOIN my_rent mr ON r2.rental_id = mr.rental_id
	       ) aux
	 ) rent_amount
RETURNING *;



       --One more payment approach
    --Please rent one more time with the following query (the same with previous one)
INSERT INTO rental (rental_date, inventory_id, customer_id, staff_id)
SELECT CURRENT_TIMESTAMP, i2.inventory_id, 477, s.staff_id 
FROM inventory i2 
JOIN film f2 ON i2.film_id = f2.film_id AND f2.title = 'CATCH ME IF YOU CAN'
JOIN staff s ON i2.store_id = s.store_id
WHERE i2.store_id = 2 AND NOT EXISTS  --customer is able to rent movie If inventory_id does not exist in RENTED id 
		(SELECT i.inventory_id    --getting all inventory_id we do not have on stock
		 FROM inventory i 
		 JOIN film f ON i.film_id = f.film_id AND f.title = 'CATCH ME IF YOU CAN'
		 JOIN rental r2 ON i.inventory_id = r2.inventory_id AND r2.return_date IS NULL 
		 WHERE i2.inventory_id=i.inventory_id) 
LIMIT (1)
RETURNING *;
       
   --Return it please with following

WITH rent_id_in_rent AS
	(SELECT r2.rental_id   --I need any rental_id with return_date = NULL + for 'CATCH ME IF YOU CAN' + for my (customer_id=477) 
	 FROM rental r2
	 JOIN
		(SELECT i3.inventory_id
	 	 FROM inventory i3
	 	 WHERE i3.store_id = 2 AND i3.film_id IN 
     	   		(SELECT film_id
				 FROM film
				 WHERE title = 'CATCH ME IF YOU CAN')
		) iid ON r2.inventory_id = iid.inventory_id AND r2.customer_id =477 AND r2.return_date IS NULL
LIMIT 1
	 )
		                
UPDATE rental 
SET return_date = CURRENT_TIMESTAMP
WHERE rental_id = (SELECT rental_id FROM rent_id_in_rent)
RETURNING *;

	--And pay

INSERT INTO payment (customer_id, staff_id, rental_id, amount, payment_date) --Inserting row from SELECT into table
SELECT * 
FROM (SELECT 447 AS customer_id,							-- That is my ID
	   		 2 AS staff_id,                                 -- A shop assistant with staff_id=2 logged in a payment system when I was returning DVD
	   		 aux.rental_id,
	   		 CASE WHEN aux.days_num <= aux.rental_duration THEN aux.rental_rate                                           --1) Customer is supposed to pay rental_rate within a rental_duration pireod
	   			  WHEN aux.days_num > aux.rental_duration THEN aux.rental_rate+(aux.days_num - aux.rental_duration)       --2) Next rental cost is ONE DOLLAR FOR EVERY DAY
	  		 END AS amount,																								  --3) I've matched amount of money from DB and amount I've counted but I did not find evidence of following statement IF A FILM IS MORE THAN RENTAL_DURATION * 2 OVERDUE, CHARGE THE REPLACEMENT_COST
	   		 CURRENT_TIMESTAMP AS payment_date
	  FROM (SELECT r2.rental_id,                                             --Extracting rental period in days. I need rental_rate, rental_duration as well (for counting amount of money) 
	  	   		   EXTRACT(DAY FROM r2.return_date - r2.rental_date) AS days_num,
	  	   		   f2.rental_rate,
	  	   		   f2.rental_duration
	       	FROM rental r2
	       	JOIN inventory i2 ON r2.inventory_id = i2.inventory_id 
	       	JOIN film f2 ON i2.film_id = f2.film_id 
	       	WHERE r2.rental_id = (SELECT LASTVAL()) -- SELECT LASTVAL() returns the last rental_id
	       ) aux
	 ) rent_amount
RETURNING *;