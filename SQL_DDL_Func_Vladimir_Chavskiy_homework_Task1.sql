--TASK 1. Create a function that will return a list of more popular films in the different countries
--I was looking at customer adress to find out witch film is more popular in each given region. That is because customer represent region and region preferences as result..
CREATE OR REPLACE FUNCTION the_best_film_in_country (IN country_title CHARACTER[]) --According to the task input is ARRAY['country1',...'countryN']
												  --(VARIADIC country_title CHARACTER[]). For variadic it is possible to use a query without 'ARRAY' -> ('country1',...'countryN')
RETURNS TABLE (country TEXT,
			   film TEXT,
			   rating MPAA_RATING,
			   film_language BPCHAR(20),
			   film_length INT2,
			   release_year YEAR)

AS $$

SELECT DISTINCT ON (f_stat.country) f_stat.country, --My criteria are rental quantity (the most important), revenue, release year (the earlier the better for rating). If they are not enough and several movies have the same rental quantity + revenue + release year the first movie from a list gets into final selection.
				   f_stat.title, 
				   f_stat.rating, 
				   f_stat."name", 
				   f_stat.length, 
				   f_stat.release_year
FROM 
		(SELECT cnt.country,
				f.title, 
				f.rating, 
				l2."name", 
				f.length, 
				f.release_year, 
				COUNT(*) AS num, 
				SUM(p2.amount) tot_amount 
		 FROM film f 
		 JOIN inventory i ON f.film_id = i.film_id 
		 JOIN rental r ON i.inventory_id = r.inventory_id 
		 JOIN customer c ON r.customer_id = c.address_id 
		 JOIN address a ON c.address_id = a.address_id 
		 JOIN city ct ON a.city_id = ct.city_id
		 JOIN country cnt ON ct.country_id = cnt.country_id
		 JOIN payment p2 ON r.rental_id = p2.rental_id
		 JOIN "language" l2 ON f.language_id = l2.language_id
		 GROUP BY cnt.country_id, f.film_id, l2."name" --As result contains language column we should group by language.name as well. Otherwise which language we should show as final result if film is represented in more then one languague.
		 ORDER BY cnt.country, num DESC, tot_amount DESC, f.release_year) f_stat
WHERE f_stat.country = ANY (country_title)

$$
LANGUAGE SQL;

--queries to execute and check function behavior
SELECT * FROM the_best_film_in_country (ARRAY['Afghanistan', 'Belarus', 'Yemen', 'Armenia'])

/* IMPORTANT. I used two SELECTs. The 1st one to filter all films and the 2nd one to cut out unnecessary columns + DISTINCT ON to leave 
only unique film titles in the result. So, the point is I applied DISTINCT ON in the 2nd SELECT 
because of Order of execution of a Query (firstly DISTINCT ON then ORDER BY). At least https://sqlbolt.com/lesson/select_queries_order_of_execution says about it.

I've prepared an experiment:

CREATE TABLE my_table (
	id_1 int,
	id_2 int
);

INSERT INTO my_table
VALUES (1,5),
	   (1,1),
	   (1,4),
	   (1,3),
	   (2,4),
	   (2,1),
	   (2,5);

	   	   	   
SELECT DISTINCT ON (id_1) *      
FROM my_table;
	-Result is (1,5)
			   (2,4)
	  	  
SELECT DISTINCT ON (id_1) * 
FROM my_table
ORDER BY id_1, id_2;
	-Expected result as if DISTINCT ON is the 1st and next ORDER BY
	-But result is (1,1)
				   (2,1).
	-So, according to the result  1. ORDER BY  2. DISTINCT ON
	
DROP TABLE my_table;
	
Is it mistake on the website or something else?
*/



--TASK 2. Create function that will return a list of films in stock by TITLE as an input parameter
CREATE OR REPLACE FUNCTION ten_last_films_with_filter (IN like_filter TEXT) 
RETURNS TABLE (row_num integer, --I could use SETOF with a customized class but my choice was not create a new class.
			   film_title TEXT,
			   "language" TEXT,
		       customer_name TEXT,
			   rental_date TIMESTAMPTZ)
LANGUAGE plpgsql

AS $_$

DECLARE

	rr RECORD;

BEGIN
	
	IF like_filter IS NULL OR like_filter = '' THEN --Raising if film_title IS NULL, empty or consists more than one 'space'. One 'space' is OK because title might contains two or more words.
		RAISE NOTICE 'no NULL or empty value for the "film_title" is allowed. Please fill in the "film_title" field';
		RETURN; --Interruption if error
	ELSIF like_filter LIKE '%  %' AND like_filter ~ '^[  ]+$' THEN  --This IF in case when several spaces in a row is allowed. If not then "LIKE '%  %'" is enough (but with another notification: 'several spaces in row is not allowed') 
		RAISE NOTICE 'no empty the "film_title" field is allowed. Please fill in the "film_title" field';
		RETURN;
	END IF;

	CREATE TEMPORARY TABLE tmp_films (row_num SERIAL, --row_num field is generated counter field. I decided to create temporary table to organize this column. I could do it with CREATE SEQUENCE as well. But I wanted to check TEMPORARY TABLE + INSERT + DROP behaviour.
							  	      film_title TEXT,
						  		      "language" TEXT,
								      customer_name TEXT,
								      rental_date TIMESTAMPTZ);
									     
	RETURN query

	INSERT INTO tmp_films (film_title, "language", customer_name, rental_date)
	SELECT DISTINCT ON (ten_films.title) *
	FROM 
		(SELECT f2.title,
				l2."name",
				c.first_name||' '||c.last_name,
				r.rental_date 
	 	 FROM film f2 
 	 	 JOIN inventory i ON f2.film_id = i.inventory_id 
	 	 LEFT JOIN rental r ON i.inventory_id = r.inventory_id --LEFT JOIN becouse a film might be new and it has never been in rent.  "order by RENTAL_DATE field" does not mean we do not need movies with NULL value. 
	 	 LEFT JOIN customer c ON r.customer_id = c.customer_id
	 	 JOIN "language" l2 ON f2.language_id  =l2.language_id
	 	 WHERE f2.title LIKE UPPER('%'||like_filter||'%') 
					  	AND NOT EXISTS (SELECT r2.inventory_id     --This SELECT returns all inventories that are not in stock. An INVENTORY out of the stock if return_date IS NULL
			   				   			FROM rental r2             
			   				   			WHERE r2.return_date IS NULL AND i.inventory_id = r2.inventory_id) 
	ORDER BY r.rental_date DESC) ten_films
	LIMIT 10
	RETURNING *;

	DROP TABLE tmp_films;
   
RETURN;
END 
$_$;

--queries to execute and check function behavior 
SELECT * FROM ten_last_films_with_filter (' ')


--TASK 3. Create function that inserts new movie with the given name in �film� table. �release_year�, �language� are optional arguments and default to current year and Russian respectively. The function must return film_id of the inserted movie. 
CREATE OR REPLACE FUNCTION add_new_film_def (film_title TEXT, 
										     fun_release_year INT DEFAULT EXTRACT (YEAR FROM CURRENT_DATE),
										     film_language TEXT DEFAULT 'Russian')
RETURNS INTEGER
	
LANGUAGE plpgsql
	
	AS $$
	
DECLARE 

	check_language INTEGER; --variable to check there is language 'film_language' in the LANGUAGE table or not
	l_id INTEGER; --variable to hold language_id
	f_id INTEGER; --variable to return
	temp_id TEXT; --variable to hold movie ID(-s) is with the same title, release year and language with the given movie

BEGIN
	
--NOTICE. Function argument values might be NULL in my approach. Maybe it is redundancy but One of the reason I wanted to have practise with NULL values 
	IF fun_release_year IS NULL THEN    --checking if fun_release_year is NULL or not
	fun_release_year := EXTRACT (YEAR FROM CURRENT_DATE);
	END IF;

	IF film_language IS NULL THEN    --checking if film_language is NULL or not
	film_language := 'Russian';
	END IF;
	
	SELECT id INTO temp_id				--movie ID(-s) is with the same title, release year and language with the given movie
	FROM (SELECT 1, string_agg(f.film_id::text,', ') AS id
		  FROM film f
		  JOIN "language" l2 ON f.language_id = l2.language_id 
		  WHERE f.title = film_title
		 		AND f.release_year = fun_release_year
		 		AND l2."name" = film_language) the_same_name;

	IF temp_id IS NOT NULL --Checking if there is one or more movies with the same title, release year and language
		THEN 
		RAISE NOTICE 'NOTICE! You have added a new movie with the title, release year and language the same with movie(-s) having id: %', temp_id; 
	--Function inserts given movie even if it maths with another one. It is allowed being several movies with the same title release year and language. We just have to rise notice
	END IF;

	IF film_title IS NULL THEN --Raising if film_title IS NULL
		RAISE NOTICE 'NULL value for the "film_title" field is not allowed. Please fill in the "film_title" field';
		RETURN NULL; --Interruption if error
	END IF;
	
	SELECT COALESCE (count(*),0) INTO check_language --Preparation for a CHECK block to check there is language 'film_language' in the LANGUAGE table (or not). If so check_language>0 
	FROM "language" l 
	WHERE UPPER(l."name") = UPPER(film_language);

	IF check_language = 0 THEN --If there is not language 'film_language' in the LANGUAGE table we have to insert it and return id to use it father
		INSERT INTO "language" ("name")
		SELECT film_language
		RETURNING language_id INTO l_id;
	ELSE--If there is language 'film_language' in the LANGUAGE table we do not have to insert it but we have to return id to use it father
		SELECT language_id INTO l_id
		FROM "language"
		WHERE UPPER("name") = UPPER(film_language)
		LIMIT 1;
	END IF;

	INSERT INTO film (title, release_year, language_id)
	SELECT film_title, fun_release_year, l_id
	RETURNING film.film_id INTO f_id;

RETURN f_id; 
END $$;

--queries to execute and check function behavior 
SELECT * FROM add_new_film_default (null, null, null);
SELECT * FROM add_new_film_def ('Forsage  25', null, null);
SELECT * FROM add_new_film_def ('Forsage 25', 2011, null);
SELECT * FROM add_new_film_def ('Forsage 25', null, 'Russian');
SELECT * FROM add_new_film_def ('Forsage 25', 2021, 'English');
SELECT * FROM add_new_film_def ('Forsage 25');
SELECT * FROM add_new_film_def ('Forsage 25', 2018);
SELECT * FROM add_new_film_def ('Forsage 25',null);