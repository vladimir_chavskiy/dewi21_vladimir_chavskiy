SET SEARCH_path TO sh;



--TASK 1. Analyze annual sales by channels and regions. Build the query to generate the same report:
SELECT *
FROM (
	SELECT country_region,
		   calendar_year,
		   channel_desc,
		   to_char(amount_sold, '9,999,999,999,999 $') AS amount_sold, -- amount_sold representation as  9,999,999,999,999 $ format
		   to_char(amount_sold / total * 100, '990.99 %') AS "% BY CHANNELS", -- % BY CHANNELS representation as  999.99 % format. '% BY CHANNELS' = 'amount_sold' / 'total'
		   to_char(FIRST_VALUE (amount_sold / total * 100 ) OVER w, '990.99 %') AS "% PREVIOUS PERIOD", --'% BY CHANNELS' for a previous year
		   to_char((amount_sold / total - FIRST_VALUE (amount_sold / total) OVER w) * 100, '990.99 %') AS "% DIFF" -- % DIFF = % BY CHANNELS - % PREVIOUS PERIOD
	FROM (
		SELECT c2.country_region,
			   t.calendar_year,
			   ch.channel_desc,
			   sum(amount_sold) AS amount_sold, --It counts amount of sales for distinct country_region + calendar_year + channel_desc
			   sum(sum(amount_sold)) OVER (PARTITION BY c2.country_region, t.calendar_year) AS total --It counts amount of sales for distinct country_region + calendar_year 
		FROM sales s
		JOIN customers c ON s.cust_id = c.cust_id 
		JOIN countries c2 ON c.country_id = c2.country_id
		JOIN times t ON s.time_id = t.time_id 
		JOIN channels ch ON s.channel_id = ch.channel_id
		WHERE c2.country_region IN ('Americas', 'Asia', 'Europe') --All countings for 'Americas', 'Asia' and 'Europe' (according to a task)
		GROUP BY c2.country_region, t.calendar_year, ch.channel_desc
	) AS tot
	WINDOW w AS (PARTITION BY country_region, channel_desc
		   		 ORDER BY country_region, channel_desc, calendar_year
		   		 ROWS BETWEEN 1 PRECEDING AND CURRENT ROW)
) AS tab
WHERE calendar_year IN (1999, 2000, 2001) --All countings for 1999, 2000 and 2001 years (according to a task)
ORDER BY country_region, calendar_year, channel_desc; --ORDER  according to a task



--TASK 2. Build the query to generate a sales report for the 49th, 50th and 51st weeks of 1999. 
--Add column CUM_SUM for accumulated amounts within weeks. For each day, display the average sales 
--for the previous, current and next days (centered moving average, CENTERED_3_DAY_AVG column). For Monday, 
--calculate average weekend sales + Monday + Tuesday. For Friday, calculate the average sales for 
--Thursday + Friday + weekends
SELECT *	   
FROM (
	SELECT t.calendar_week_number, 
		   t.time_id, 
		   t.day_name,
		   sum(amount_sold) AS sales, --amount of sales for distinct t.time_id (days)
		   sum(sum(amount_sold)) OVER w AS cum_sum, --cumulative amount int.calendar_year + t.calendar_week_number partition
		   round(CASE WHEN t.day_name = 'Monday' THEN avg(sum(amount_sold)) OVER wm -- We have to count data from different Monday and Friday frame sizes from the rest of days.
		   		 	  WHEN t.day_name = 'Friday' THEN avg(sum(amount_sold)) OVER wf -- Moreover we have different boundaries relatively current row for Monday (2 days before and 1 following day) and Friday (1 day defore and 2 following days)
		   		 	  ELSE avg(sum(amount_sold)) OVER w2
		    	 END, 2) AS centered_3_day_avg
	FROM sales s
	JOIN times t ON s.time_id = t.time_id
	GROUP BY t.time_id
	WINDOW w AS (PARTITION BY t.calendar_year, t.calendar_week_number
		   		 ORDER BY t.time_id
		   		 RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW),
		   w2 AS (ORDER BY t.time_id RANGE BETWEEN
		   		  INTERVAL '1' DAY PRECEDING AND
		   		  INTERVAL '1' DAY FOLLOWING),
		   wm AS (ORDER BY t.time_id RANGE BETWEEN
		   		  INTERVAL '2' DAY PRECEDING AND
		   		  INTERVAL '1' DAY FOLLOWING),
		   wf AS (ORDER BY t.time_id RANGE BETWEEN
		   		  INTERVAL '1' DAY PRECEDING AND
		   		  INTERVAL '2' DAY FOLLOWING)) AS tab
WHERE calendar_week_number IN (49, 50, 51) AND --data for 49, 50, 51 weeks from 1999 year
	  EXTRACT (YEAR FROM time_id) = 1999;

	 

--TASK 3.Prepare 3 examples of using window functions with a frame clause (RANGE, ROWS, and GROUPS modes)
--Explain why you used a particular type of frame in each example. It can be one query or 3 separate queries.

--It counts number of customers which are in diapason 5 years older and 5 years younger than current customer (current customer agegroup is included)	 
SELECT cust_id,
	   cust_first_name,
	   cust_last_name,
	   cust_year_of_birth,
	   count (*) OVER (ORDER BY cust_year_of_birth RANGE BETWEEN
	   				   5 PRECEDING AND 5 FOLLOWING)
FROM customers c;

--It counts all sales for previous working day (or day with sales), current day and next working day (or day with sales)
--e.x. It counts all sales for Thursday,Friday and Monday if: 1. Current row is Friday 2. Saturday and Sunday
--are days off. 3. There are sales on Thursday and Monday. 
SELECT count(*) OVER (ORDER BY time_id GROUPS BETWEEN
	   				  1 PRECEDING AND 1 FOLLOWING)
FROM sales;

--It counts AVG amount of sales for previous, current and following day. It works the same way with GROUPS 
--(in this case) because of GROUP BY time_id If there is a gap between days it counts next 'working day' 
--(NOT the same way with RANGE)
SELECT time_id,
	   sum(amount_sold),
	   avg(sum(amount_sold)) OVER (ORDER BY time_id ROWS BETWEEN
	   							   1 PRECEDING AND 1 FOLLOWING)
FROM sales s
GROUP BY time_id;