--TASK 4. Prepare answers to the following questions
--TASK 4.1. How can one restrict access to certain columns of a database table?

--One can create a VIEW only with allowed columns and then grant privileges on the view to roles which is needed certain columns of a database table.

CREATE ROLE accountant; --A new role (accountant)

CREATE VIEW payment_with_no_customer WITH (security_barrier) AS    --The VIEW is the same with the payment table but without customer_id column.
SELECT payment_id, staff_id, amount, payment_date FROM payment; 

GRANT SELECT ON public.payment_with_no_customer TO accountant; --It allows to SELECT payment_with_no_customer to the accountant role.
GRANT USAGE ON SCHEMA public TO accountant;

--Following queries are for checking.
SET ROLE accountant;
SET ROLE postgres;
SET ROLE customer;

SELECT * FROM public.payment_with_no_customer;

/*
 As for security_barrier attribute as I understand it is useful when creating VIEWs from tables with WHERE filter. 
 And data is secure even without  security_barrier if creating VIEWs from tables without WHERE (for example we have 
 created VIEW to give access to all rows but not all columns)
*/
--Example

--I've created 2 views with hidden rows. (All rows with payment_id >= 16054 are hidden) 
SET ROLE postgres;

CREATE VIEW payment_not_all_rows_s WITH (security_barrier) AS
	SELECT payment_id, amount 
	FROM payment
	WHERE payment_id <16054;

CREATE VIEW payment_not_all_rows  AS
	SELECT payment_id, amount 
	FROM payment
	WHERE payment_id <16054;


--Creating function with 'low' cost. This tricky moment forces the SQL optimizer to calculate FUNCTION amount_all_rows before a WHERE payment_id <16054 filter.
--As I've created function with RAISE it sends payment_id and amount into OUTPUT space row by row.
--And only then SQL execute query according to WHERE filter
DROP FUNCTION amount_all_rows;

CREATE OR REPLACE FUNCTION amount_all_rows(integer, numeric) RETURNS boolean AS $$
BEGIN
  RAISE NOTICE 'ID is: %. Amount is: %',$1, $2;
  RETURN true;
END;
$$ COST 0.00001 LANGUAGE plpgsql;

SELECT * FROM payment_not_all_rows_s;

SELECT * FROM payment_not_all_rows;

SELECT * FROM payment_not_all_rows_s WHERE amount_all_rows(payment_id, amount); -- as payment_not_all_rows_s is WITH security_barrier we see rows with payment_id<16054 in OUTPUT space only.
																				-- Because security_barrier forces the SQL optimizer to calculate WHERE  payment_id <16054 filter first of all.
SELECT * FROM payment_not_all_rows WHERE amount_all_rows(payment_id, amount); -- payment_not_all_rows is without security_barrier. So we can see all row (even with payment_id>=16054) in OUTPUT space.




--TASK 4.2. What is the difference between user identification and user authentication?
/*
Identification is a process when one indicate his/her/its unique indicator for system and a system checks if this 
indicator exists or not (ID/login/SSN/username/email might be an indificator).

Authentication is a identification process when one proves that a unique indicator (ID/login/SSN/username/email) is belong to him/her/it. Password/ face scan /fingerprint
might be a proving.

It works the following way.

Someone with name Bod Nikols tries to get into working monitoring process sistemn. He tipes down his login into the system.
Sustem cheks login and recognaze it. For now system 'knows' that Bob Nikols wants to get into the sistem. It is identification.

That is great but Bod Nikols still does not have access into the system. Next he typs down his password and the system checks
if this password matches with stored one. If so Bod Nikols gets access. It means Bod Nikols is authorized 
and he are able to read/use information from the system.  It is authentication.
*/



--TASK 4.3. What are the recommended authorization protocols for PostgreSQL?
/*
'Recommended' means all protocol which are menshend in the afficial documentation. There are eleven authentification method: 

TRUST AUTHENTICATION. Anyone has access to a BD who has access to a server. This method is convenient in case when local 
connection with single-user workstation is. We can use trust method with multi-user workstation if we restrict access to 
the Unix-socket file on the  file-system level. All created restrictions apply.

PASSWORD AUTHENTICATION. A user can get access to a DB with username and password. We can CREATE USER WITH PASSWORD or 
CREATE USER and then ALTER ROLE to set a password. If a user does not have a password he are not able to connect to a BD.

GSSAPI AUTHENTICATION. That is standard protocol for secure authentication. And Postgres supports it.GSSAPI provides us with 
secure automatic authentication for systems which support GSSAPI. GSSAPI is secure but we should 
use GSSAPI encryption to send data in a secure way.

SSPI AUTHENTICATION. SSPI is WINDOWS authentication method with single sign-on. It works in case when both client and server 
are running WINDOWS or a platform where GSSAPI is available

KERBEROS AUTHENTICATION. This method gets operating system username from IDENT server and uses it as allowed DB username. It is supported only on 
TCP/IP connections.

PEER AUTHENTICATION. This method gets operating system username from a kernel and uses it as allowed DB username. It is supported only on 
local connections.

LDAP AUTHENTICATION. LDAP works in a similar to PASSWORD way. It is used for username and password pair verification. So username and password must 
exist before we use LDAP. 

RADIUS AUTHENTICATION. RADIUS works in a similar to PASSWORD way. It is used for username and password pair verification. So username and 
password must exist before we use RADIUS.

CERTIFICATE AUTHENTICATION. This method is available only for SSL connections. Client certificate is used to get access. Password is not 
required. User must have valid certificate.  In SSL case the Common Name attribute of the certificate is compared to the database username.

PAM AUTHENTICATION. PAM method works in a similar to PASSWORD way. It uses PAM mechanism for username and password pair verification. It  can verify username/password pair and computer name or its IP.So username and 
password must exist before we use PAM.

BSD AUTHENTICATION. BSD method works in a similar to PASSWORD way. It uses BSD mechanism for username and password pair verification. So username and 
password must exist before we use BSD. It only available on OpenBSD.

In conclusion i'd like to add that documentation recommends peer or even trust (for some cases) method for local connections 
and password authentication for distance connections.
*/



--TASK 4.4.What is proxy authentication in PostgreSQL and what is it for? Why does it make the previously discussed role-based access control easier to implement?
/*
Let's imagine one uses APP. It might be a non-authorized user, admin, manager or seller. There is one more user type 
and it is web_app_user. APP connects to BD with this role (with web_app_user) and then invokes the SET ROLE statement 
to set  one of roles I've mentiont above (non-authorized user, admin, manager or seller). It invokes the SET ROLE based on 
a user type which has authorized into APP. This authentication method is called as proxy authentication.

APP might to implement SET SESSION AUTHORIZATION <user_name> but it requires superuser rights.

It is widely used to provide users with access to a DB according to user roles/rights.

As I understand it is easier to grant the application rights with which it can SET ROLE or SET SESSION AUTHORIZATION 
based on a user type which has authorized into APP. It means to delegate users authentication to the APP and let APP 
to SET ROLE or SET SESSION AUTHORIZATION.
*/



