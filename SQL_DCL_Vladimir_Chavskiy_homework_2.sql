-- TASK 2.1. Create group roles: DB developer, backend tester (read-only), customer (read-only for film and actor)

CREATE ROLE developer;

CREATE ROLE tester;

CREATE ROLE customer;



--TASK 2.2. Create personalized role for any customer already existing in the dvd_rental database. Role name must be client_{first_name}_{last_name}
--(omit curly brackets). Customer's payment and rental history must not be empty.

DO $$ --I used anonymous code block to create a user role. 
	
DECLARE

tmp_fnln TEXT;

BEGIN
	
	tmp_fnln:= (SELECT 'client_'||c.first_name||'_'||c.last_name
				FROM customer c
				JOIN payment p2 ON c.customer_id = p2.customer_id 
				JOIN rental r2 ON p2.rental_id = r2.rental_id
				LIMIT 1);

	EXECUTE 'CREATE USER '||tmp_fnln;

	RAISE NOTICE 'New user % has been successfully created', tmp_fnln; --A client_MARY_SMITH user has been created

END $$;



--TASK 2.3. Assign proper privileges to each role.

GRANT INSERT, UPDATE ON ALL TABLES IN SCHEMA public TO developer; --I've assigned INSERT and UPDATE privileges just according to the video. But if we imagine a 
																  --situation when there isn't an architect in a project it seems we should grant CREATE 
																  --privilege. Some sources say developers might get SELECT. So it depends.
GRANT USAGE ON SCHEMA public TO developer;						  --We have to have access to schema where tables are to get access to tables (this case is only about tables
																  --because we grant INSERT and UPDATE ON ALL TABLES IN SCHEMA public). 
GRANT USAGE ON ALL SEQUENCES IN SCHEMA public TO developer;		  --We have to GRANT UGANE on all SEQUENCES IN SCHEMA to let DEVELOPER role insert new rows without specifying an ID 

GRANT SELECT ON ALL TABLES IN SCHEMA public TO tester;			  --It's usual case when testers get only SELECT privilege. But I came across a comment in the Internet and 
																  --says automatized testers might get DELETE and TRUNCATE privilege to clean up after tests. 
GRANT USAGE ON SCHEMA public TO tester;

GRANT SELECT ON public.actor, public.film TO customer;
GRANT USAGE ON SCHEMA public TO customer;

GRANT customer TO client_mary_smith;							  --It makes the client_mary_smith user CUSTOMER role member. It means a client_MARY_SMITH user has all CUSTOMER's privileges.

--ALTER USER client_MARY_SMITH PASSWORD 'password@_r45'           --If we assign a password for a USER the USER are able to connect to a DB with a USERNAME and a PASSWORD.



--TASK 2.4. Verify that all roles are working as intended.

SET ROLE postgres;
SET ROLE developer;
SET ROLE tester;
SET ROLE customer; 
SET ROLE client_MARY_SMITH;

SELECT * FROM public.film;
SELECT * FROM public.actor;
SELECT * FROM public.payment;
SELECT * FROM public.category;

GRANT SELECT ON ALL TABLES IN SCHEMA public TO developer; --It seems a developer role aren't able to update table without SELECT
														  --privilege if WHERE filter is needed. 


DO --I used anonymous code block to insert and then update a row in the inventory table.

$$

DECLARE

	invent_id integer;
	
BEGIN
	
	INSERT INTO public.inventory (film_id, store_id)
	VALUES (100, 1)
	RETURNING inventory_id INTO invent_id;

	RAISE NOTICE 'A new inventory % has been added', invent_id;

	UPDATE public.inventory
	SET store_id = 2
	WHERE inventory_id = invent_id;

	RAISE NOTICE 'An inventory % has been updated', invent_id;
	
END $$;

--I've connected with username 'client_mary_smith' and password 'password@_r45' to a DB and checked behavior as well.