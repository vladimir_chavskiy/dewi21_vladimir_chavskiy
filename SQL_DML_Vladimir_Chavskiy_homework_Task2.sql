--Task_2_1 Create table �table_to_delete� and fill it with the following query:

CREATE TABLE table_to_delete AS
SELECT 'veeeeeeery_long_string' || x AS col
FROM generate_series(1,(10^7)::int) x; -- generate_series() creates 10^7 rows of sequential numbers from 1 to 10000000 (10^7)
	-- Creating and filling process took 20.933s



--Task_2_2 Lookup how much space this table consumes with the following query:

SELECT *, pg_size_pretty(total_bytes) AS total,
		  pg_size_pretty(index_bytes) AS INDEX,
		  pg_size_pretty(toast_bytes) AS toast,
		  pg_size_pretty(table_bytes) AS TABLE
FROM ( SELECT *, total_bytes-index_bytes-COALESCE(toast_bytes,0) AS table_bytes
	   FROM (SELECT c.oid,
					nspname AS table_schema,
					relname AS TABLE_NAME,
					c.reltuples AS row_estimate,
					pg_total_relation_size(c.oid) AS total_bytes,
					pg_indexes_size(c.oid) AS index_bytes,
					pg_total_relation_size(reltoastrelid) AS toast_bytes
			 FROM pg_class c
			 LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
			 WHERE relkind = 'r'
             ) a
      ) a
WHERE table_name LIKE '%table_to_delete%';
	--Table table_to_delete table consumes 575M on disc space



--Task_2_3 Issue the following DELETE operation on �table_to_delete�:
--Task_2_3_a. Note how much time it takes to perform this DELETE statement;
--Task_2_3_b. Lookup how much space this table consumes after previous DELETE;

DELETE FROM table_to_delete
WHERE REPLACE(col, 'veeeeeeery_long_string','')::int % 3 = 0; -- removes 1/3 of all rows
	--a. Deleting from table table_to_delete 1/3 of all rows process took 20.6s. It takes 10.489s to delete 10^7 rows
	--b. It seems DELETE marks rows as 'ready to be rewritten' but does not clean disc space. The table_to_delete table after DELETE query consumes the same 575M on disc space
	
--Task_2_3_c. Perform the following command (if you're using DBeaver, press Ctrl+Shift+O to observe server output (VACUUM results)):
		--VACUUM FULL VERBOSE table_to_delete;
--Task_2_3_d. Check space consumption of the table once again and make conclusions;

VACUUM FULL VERBOSE table_to_delete;
	--d. VACUUM query output: vacuuming "public.table_to_delete" "table_to_delete": found 0 removable, 6666667 nonremovable row versions in 73530 pages
	--Table table_to_delete table consumes 383M on disc space afrer a VACUUM query
	--VACUUM reclaims disc space occupied by deleted rows (dead tuples).  

--Task_2_3_e. Recreate �table_to_delete� table;

DROP TABLE table_to_delete;   	 --Dropping table table_to_delete

CREATE TABLE table_to_delete as  --Creating table table_to_delete one more time
SELECT 'veeeeeeery_long_string' || x AS col
FROM generate_series(1,(10^7)::int) x;

--Task_2_4. Issue the following TRUNCATE operation: 
--Task_2_4_a. Note how much time it takes to perform this TRUNCATE statement.
--Task_2_4_b. Compare with previous results and make conclusion.
--Task_2_4_c. Check space consumption of the table once again and make conclusions;

TRUNCATE table_to_delete;
	--a. TRUNCATE statement took 11ms. 
	--b. TRUNCATE was performed almost immediately if we compare with DELETE statement. Deleting 10^7 rows from the table_to_delete table took 10.489s
	--c. Space consumption of the table_to_delete table after TRUNCATE is 0M (8192 bytes)
	


--Task_2_5. Hand over your investigation's results to your trainer. The results must include:
--Task_2_5_a. Space consumption of �table_to_delete� table before and after each operation;
--Task_2_5_b. Duration of each operation (DELETE, TRUNCATE)


--    ---------------------------------------------------------------------------------------------------
--   |                                        COMPARATIVE TABLE                                          |
--    ---------------------------------------------------------------------------------------------------
--   | Statement name                                                        |   DELETE    |   TRUNCATE  |
--    ---------------------------------------------------------------------------------------------------
--   | Rows number to work with                                              |    10^7     |     10^7    |
--   | Space consumption of �table_to_delete� table before operation, bytes  | 602 554 368 | 602 554 368 |
--   | Space consumption of �table_to_delete� table after operation, bytes   | 602 554 368 |     8 192   |
--   | Execution time, ms                                                    |    10 489   |      11     |
--    --------------------------------------------------------------------------------------------------- 

-- let's sum up: 1. TRUNCATE is a statement that removes all records from the table as if we used DETETE without WHERE
--               2. Table structure and constraints remain intact after TRUNCATE and DELETE. BUT NOTICE:
--						2.1. Auto-increment values will be reset when truncating
--                      2.2. Auto-increment values will not be affected when deleting
--               3. DELETE removes rows row by row. TRUNCATE marks whole table as 'ready to be rewritten'.
--					DELETE takes 10 489ms to remove 10^7 rows. TRUNCATE takes just 11ms to remove the same rows number
--					But if we try to remove table that contains only two rows the result is 2 and 11ms for DELETE and TRUNCATE correspondently

-- Concludion:   DELETE is useful for deleting all records from shot tables or in cases when we need WHERE condition
--				 TRUNCATE delivers quick results for deleting all records from huge tables 
