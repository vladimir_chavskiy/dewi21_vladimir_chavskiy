DROP FUNCTION  customer_rent_info;

CREATE OR REPLACE FUNCTION customer_rent_info (client_id integer, 
											   left_boundary TEXT DEFAULT '2001-01-01', 
											   right_boundary TEXT DEFAULT '2021-06-01')
RETURNS TABLE (metric_name TEXT, metric_value TEXT)

LANGUAGE plpgsql

AS $_$

DECLARE

	left_boundary_d DATE;
	right_boundary_d DATE;
	cust_full_name_mail TEXT;

BEGIN
	
	left_boundary_d := to_date(left_boundary, 'YYYY-MM-DD'); --We have discussed this approach at QA and I decided to use it
	right_boundary_d := to_date(right_boundary, 'YYYY-MM-DD');
	
	IF NOT EXISTS (SELECT * FROM customer c2 WHERE c2.customer_id = client_id) THEN  --That is RAISE in case if there is not a customer with given ID; 
		RAISE NOTICE 'There is not a customer with given ID';
		RETURN;
	END IF;

	--I could use INITCAP as I did further (with film titles) but I've used following structure to practice with LEFT and SUBSTR
	SELECT UPPER(LEFT(c.first_name,1))||LOWER(SUBSTR(c.first_name,2))||' '||
		   UPPER(LEFT(c.last_name,1))||LOWER(SUBSTR(c.last_name,2))||', '||
		   LOWER(c.email) INTO cust_full_name_mail --UPPER cases for the first letters in a first name and in a last name
	FROM customer c 
	WHERE c.customer_id = client_id;
	
	CREATE TEMPORARY TABLE cust_info_aux (cust_info TEXT, rent_num INT, film_list TEXT, pay_num integer, amount NUMERIC); --Temporary table to keep final info and reduce following code

		INSERT INTO cust_info_aux
		SELECT cust_full_name_mail,
			   COALESCE(
			   		SUM(CASE WHEN r2.rental_date BETWEEN left_boundary_d AND right_boundary_d THEN 1 ELSE 0 END), 0
			   		   ), 
			   COALESCE(
			   		STRING_AGG(DISTINCT(CASE WHEN r2.rental_date BETWEEN left_boundary_d AND right_boundary_d THEN INITCAP(f2.title) END),', '), 'There is not any rental in this period of time'
			   		   ),
			   COALESCE(COUNT(p2_period.payment_id)::INT, 0), --it returns 0 if number of payments is NULL
			   COALESCE(SUM(p2_period.amount), 0)
		FROM customer c
		JOIN rental r2 ON c.customer_id = r2.customer_id
		JOIN inventory i2 ON r2.inventory_id = i2.inventory_id 
		JOIN film f2 ON i2.film_id = f2.film_id
		LEFT JOIN (SELECT * FROM --Filtering all payments according to a period (where payment date is BETWEEN left_boundary_d AND right_boundary_d)
				   payment p2 
				   WHERE p2.payment_date BETWEEN  left_boundary_d AND right_boundary_d) p2_period ON r2.rental_id = p2_period.rental_id -- LEFT JOIN for case if a customer has no payments
		WHERE c.customer_id = client_id
		GROUP BY c.customer_id;

	CREATE TEMPORARY TABLE fin_info (metric_name TEXT, metric_value TEXT); --Temporary table to present data according to a format from the task.

	RETURN query
	
	INSERT INTO fin_info
	VALUES ('customer''s info', (SELECT cust_info::TEXT FROM cust_info_aux)),
		   ('num. of films rented', (SELECT rent_num::TEXT FROM cust_info_aux)),
		   ('rented films'' titles', (SELECT film_list::TEXT FROM cust_info_aux)),
		   ('num. of payments', (SELECT pay_num::TEXT FROM cust_info_aux)),
		   ('payments'' amount', (SELECT amount::TEXT FROM cust_info_aux))
	RETURNING *;

	DROP TABLE cust_info_aux; --We need to drop table before finish
	DROP TABLE fin_info; 
		  
RETURN;
END 
$_$;

SELECT * FROM customer_rent_info (5, '2015-01-31', '2017-02-10')
SELECT * FROM customer_rent_info (5, '2000-01-31', '2017-02-10')
SELECT * FROM customer_rent_info (5, '2000-01-31', '2020-02-10')
SELECT * FROM customer_rent_info (10)