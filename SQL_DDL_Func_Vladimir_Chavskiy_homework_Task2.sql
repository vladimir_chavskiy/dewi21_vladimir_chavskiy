--TASK 4.1
--What operations do the following functions perform: film_in_stock, film_not_in_stock, inventory_in_stock, get_customer_balance,
--inventory_held_by_customer, rewards_report, last_day

--First of all I should explain INVENTORY_IN_STOCK function. This function returns boolean value true if an inventory with given ID is in a stock and false if an inventory out of a stock.
--Firstly it checks if there is any inventory record in rental. If not then 'IF v_rentals' statement returns TRUE, the function stops. It means inventory is in a stock.
--If 'IF v_rentals' statement does not return TRUE ('does not return TRUE' is crucial. No using RETURN FALSE is allowed because it is going to interrupt function)
-- we look at all records for 'inventory join rental' and check. NULL value in return_date means the inventory out of a stock 
CREATE FUNCTION inventory_in_stock(p_inventory_id integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_rentals INTEGER;
    v_out     INTEGER;
BEGIN
    -- AN ITEM IS IN-STOCK IF THERE ARE EITHER NO ROWS IN THE rental TABLE
    -- FOR THE ITEM OR ALL ROWS HAVE return_date POPULATED

    SELECT count(*) INTO v_rentals
    FROM rental
    WHERE inventory_id = p_inventory_id;

    IF v_rentals = 0 THEN
      RETURN TRUE;
    END IF;

    SELECT COUNT(rental_id) INTO v_out
    FROM inventory LEFT JOIN rental USING(inventory_id)
    WHERE inventory.inventory_id = p_inventory_id
    AND rental.return_date IS NULL;

    IF v_out > 0 THEN
      RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;
END $$;



--FILM_IN_STOCK
--The function returns all available inventory_id for given film_id and store_id. 
-- It selects all inventory for given film_id and store_is and check if it is in the given store using function INVENTORY_IN_STOCK by 'AND inventory_in_stock(inventory_id)'
-- 'inventory_in_stock(inventory_id)' gets inventory_id from 'SELECT inventory_id'
CREATE FUNCTION film_in_stock(p_film_id integer, p_store_id integer, OUT p_film_count integer) RETURNS SETOF integer
    LANGUAGE sql
    AS $_$
     SELECT inventory_id
     FROM inventory
     WHERE film_id = $1
     AND store_id = $2
     AND inventory_in_stock(inventory_id);
$_$;



--FILM_NOT_IN_STOCK
--The FILM_NOT_IN_STOCK works in the same way with the FILM_IN_STOCK but returns all inventory id for given film_id and store_id that out of a stock.
--That is because AND NOT inventory_in_stock(inventory_id). As we already know inventory_in_stock(inventory_id) returns all available inventories.
CREATE FUNCTION film_not_in_stock(p_film_id integer, p_store_id integer, OUT p_film_count integer) RETURNS SETOF integer
    LANGUAGE sql
    AS $_$
    SELECT inventory_id
    FROM inventory
    WHERE film_id = $1
    AND store_id = $2
    AND NOT inventory_in_stock(inventory_id);
$_$;



--GET_CUSTOMER_BALANCE
--The function returns v_rentfees + v_overfees - v_payments value for a given customer from scratch till a given date.
--A customer is supposed to pay v_rentfees + v_overfees. A customer has paied v_payments. A customer have to pay dierences between v_rentfees + v_overfees and v_payments if v_rentfees + v_overfees - v_payments >0  
--I've left all my comments below.
--It does not work with IF and date intervals (IF date1-date2 > date3). I realized it when I did previous home task. I do not know why but I am going to figure out it. 
CREATE FUNCTION get_customer_balance(p_customer_id integer, p_effective_date timestamp with time zone) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
       --#OK, WE NEED TO CALCULATE THE CURRENT BALANCE GIVEN A CUSTOMER_ID AND A DATE
       --#THAT WE WANT THE BALANCE TO BE EFFECTIVE FOR. THE BALANCE IS:
       --#   1) RENTAL FEES FOR ALL PREVIOUS RENTALS
       --#   2) ONE DOLLAR FOR EVERY DAY THE PREVIOUS RENTALS ARE OVERDUE
       --#   3) IF A FILM IS MORE THAN RENTAL_DURATION * 2 OVERDUE, CHARGE THE REPLACEMENT_COST
       --#   4) SUBTRACT ALL PAYMENTS MADE BEFORE THE DATE SPECIFIED
DECLARE
    v_rentfees DECIMAL(5,2); --#FEES PAID TO RENT THE VIDEOS INITIALLY
    v_overfees INTEGER;      --#LATE FEES FOR PRIOR RENTALS
    v_payments DECIMAL(5,2); --#SUM OF PAYMENTS MADE PREVIOUSLY
BEGIN
    SELECT COALESCE(SUM(film.rental_rate),0) INTO v_rentfees  --The sum of all values where given p_effective_date and not later then given p_effective_date
    FROM film, inventory, rental							  --It works similar to JOIN
    WHERE film.film_id = inventory.film_id					  --If SUM is NULL COALESCE returns 0
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

    SELECT COALESCE(SUM(
    					  IF((rental.return_date - rental.rental_date) > (film.rental_duration * '1 day'::interval), --Cheching if rental is overdue or not
        				    ((rental.return_date - rental.rental_date) - (film.rental_duration * '1 day'::interval)),--If so 1$ over fee is applied 
        				    0)																						 --No over fee if rental has been returned on time				
        			    ),0) INTO v_overfees																		 --If SUM is NULL COALESCE returns 0
    FROM rental, inventory, film
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

    SELECT COALESCE(SUM(payment.amount),0) INTO v_payments --It sum all payments for given customer and not later then given p_effective_date 
    FROM payment
    WHERE payment.payment_date <= p_effective_date
    AND payment.customer_id = p_customer_id;

    RETURN v_rentfees + v_overfees - v_payments;
END
$$;



--INVENTORY_HELD_BY_CUSTOMER
--The function returns a customer_id who holds a film with given inventory_id using a record from rental where return_date IS NULL (it means invetrory is in rent) 
--The function returns NULL if film with given inventory_id is in a stock
CREATE FUNCTION inventory_held_by_customer(p_inventory_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_customer_id INTEGER;
BEGIN

  SELECT customer_id INTO v_customer_id
  FROM rental
  WHERE return_date IS NULL
  AND inventory_id = p_inventory_id;

  RETURN v_customer_id;
END $$;



--REWARDS_REPORT
--The function returns all customers records (and shows all attributes from the table) from the the customer table which satisfy the following conditions for period witch was 3 months ago: 
--A customer made more > than min_monthly_purchases, and spent > then  min_dollar_amount_purchased
--I've left more comments below.
CREATE FUNCTION rewards_report(min_monthly_purchases integer, min_dollar_amount_purchased numeric) RETURNS SETOF customer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $_$
DECLARE
    last_month_start DATE;
    last_month_end DATE;
rr RECORD;
tmpSQL TEXT;
BEGIN

    /* Some sanity checks... */                   --The following checks input and interrupts function if 0 values
    IF min_monthly_purchases = 0 THEN
        RAISE EXCEPTION 'Minimum monthly purchases parameter must be > 0';
    END IF;
    IF min_dollar_amount_purchased = 0.00 THEN
        RAISE EXCEPTION 'Minimum monthly dollar amount purchased parameter must be > $0.00';
    END IF;

    last_month_start := CURRENT_DATE - '3 month'::interval;  --Calculating the first and the last day of month witch was 3 month ago.
    last_month_start := to_date((extract(YEAR FROM last_month_start) || '-' || extract(MONTH FROM last_month_start) || '-01'),'YYYY-MM-DD');
    last_month_end := LAST_DAY(last_month_start);

    /*
    Create a temporary storage area for Customer IDs.
    */
    CREATE TEMPORARY TABLE tmpCustomer (customer_id INTEGER NOT NULL PRIMARY KEY);
   
    /*
    Find all customers meeting the monthly purchase requirements
    */

    tmpSQL := 'INSERT INTO tmpCustomer (customer_id)
        SELECT p.customer_id
        FROM payment AS p
        WHERE DATE(p.payment_date) BETWEEN '||quote_literal(last_month_start) ||' AND '|| quote_literal(last_month_end) || '
        GROUP BY customer_id
        HAVING SUM(p.amount) > '|| min_dollar_amount_purchased || '
        AND COUNT(customer_id) > ' ||min_monthly_purchases ;

    EXECUTE tmpSQL;

    /*
    Output ALL customer information of matching rewardees.
    Customize output as needed.
    */
    FOR rr IN EXECUTE 'SELECT c.* FROM tmpCustomer AS t INNER JOIN customer AS c ON t.customer_id = c.customer_id' LOOP
    RETURN NEXT rr;
    END LOOP;

    /* Clean up */
    tmpSQL := 'DROP TABLE tmpCustomer';
    EXECUTE tmpSQL;

RETURN;
END
$_$;


--LAST_DAY
--The function returns the last day for a month which contains the timestamp from input
--I've left more comments below.
CREATE FUNCTION last_day(timestamp with time zone) RETURNS date
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
  SELECT CASE
    WHEN EXTRACT(MONTH FROM $1) = 12 THEN  
      (((EXTRACT(YEAR FROM $1) + 1) operator(pg_catalog.||) '-01-01')::date - INTERVAL '1 day')::date --subtracting 1 day from the 1th day of following year (YYYY-MM-DD format) 
    ELSE
      ((EXTRACT(YEAR FROM $1) operator(pg_catalog.||) '-' operator(pg_catalog.||) (EXTRACT(MONTH FROM $1) + 1) operator(pg_catalog.||) '-01')::date - INTERVAL '1 day')::date --subtracting 1 day from the 1th day of following month (YYYY-MM-DD format) 
    END
$_$;



--TASK 4.2. Why does �rewards_report� function return 0 rows? Correct and recreate the function, so that it's able to return rows properly.
--That is because there is nothing to select from the period three months ago.
--I've added one mone parametr (period_before_month) to the function to correct it. period_before_month is number of months we substract from current date to get the month we are interested in.
-- I could replace 3 with 1 month and it would work. But my decision was adding one more variable to create flexible solution
--PLEASE NOTICE: I've renamed the function rewards_report to rewards_report_task
--I've left SELECT statement below to check it
SELECT * FROM rewards_report_task (5,20.00,48) --48 months before

CREATE OR REPLACE FUNCTION rewards_report_task(min_monthly_purchases INTEGER, min_dollar_amount_purchased NUMERIC, period_before_month INTEGER) RETURNS SETOF customer
LANGUAGE plpgsql SECURITY DEFINER
    AS $_$
DECLARE
    last_month_start DATE;
    last_month_end DATE;
rr RECORD;
tmpSQL TEXT;
BEGIN

    /* Some sanity checks... */                   --The following checks input and interrupts function if 0 values
    IF min_monthly_purchases = 0 THEN
        RAISE EXCEPTION 'Minimum monthly purchases parameter must be > 0';
    END IF;
    IF min_dollar_amount_purchased = 0.00 THEN
        RAISE EXCEPTION 'Minimum monthly dollar amount purchased parameter must be > $0.00';
    END IF;

    last_month_start := CURRENT_DATE - (period_before_month||' '||'month')::interval;  --Calculating the first and the last day of month witch was 3 month ago.
    last_month_start := to_date((extract(YEAR FROM last_month_start) || '-' || extract(MONTH FROM last_month_start) || '-01'),'YYYY-MM-DD');
    last_month_end := LAST_DAY(last_month_start);

    /*
    Create a temporary storage area for Customer IDs.
    */
    CREATE TEMPORARY TABLE tmpCustomer (customer_id INTEGER NOT NULL PRIMARY KEY);
   
    /*
    Find all customers meeting the monthly purchase requirements
    */

    tmpSQL := 'INSERT INTO tmpCustomer (customer_id)
        SELECT p.customer_id
        FROM payment AS p
        WHERE DATE(p.payment_date) BETWEEN '||quote_literal(last_month_start) ||' AND '|| quote_literal(last_month_end) || '
        GROUP BY customer_id
        HAVING SUM(p.amount) > '|| min_dollar_amount_purchased || '
        AND COUNT(customer_id) > ' ||min_monthly_purchases ;

    EXECUTE tmpSQL;

    /*
    Output ALL customer information of matching rewardees.
    Customize output as needed.
    */
FOR rr IN EXECUTE 'SELECT c.* FROM tmpCustomer AS t INNER JOIN customer AS c ON t.customer_id = c.customer_id' LOOP
RETURN NEXT rr;
END LOOP;

    /* Clean up */
    tmpSQL := 'DROP TABLE tmpCustomer';
    EXECUTE tmpSQL;

RETURN;
END
$_$;



--TASK 4.3.Is there any function that can potentially be removed from the dvd_rental codebase? If so, which one and why?
--We could remove _group_concat and group_concat functions for sure.
--We can create aggregate function if there is no a proper one in General-Purpose Aggregate Functions list.
--But there is STRING_AGG(EXPRESSION, DELIMITER) function and we do not need to create _group_concat and group_concat as result.
--There are more comments in a TASK 4.5 clause 
CREATE FUNCTION _group_concat(text, text) RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $_$
SELECT CASE
  WHEN $2 IS NULL THEN $1
  WHEN $1 IS NULL THEN $2
  ELSE $1 || ', ' || $2
END
$_$;

CREATE AGGREGATE group_concat(text) (
    SFUNC = _group_concat,
    STYPE = TEXT

--I've edited code from FILM_LIST and created FILM_LIST_WITH_STRING_AGG view. I used string_agg insted of GROUP_CONCAT
 CREATE VIEW film_list_with_string_agg AS
 SELECT film.film_id AS fid,
    film.title,
    film.description,
    category.name AS category,
    film.rental_rate AS price,
    film.length,
    film.rating,
    string_agg(((actor.first_name)::text || ' '::text) || (actor.last_name)::text, ', ') AS actors
   FROM ((((category
     LEFT JOIN film_category ON ((category.category_id = film_category.category_id)))
     LEFT JOIN film ON ((film_category.film_id = film.film_id)))
     JOIN film_actor ON ((film.film_id = film_actor.film_id)))
     JOIN actor ON ((film_actor.actor_id = actor.actor_id)))
  GROUP BY film.film_id, film.title, film.description, category.name, film.rental_rate, film.length, film.rating;
 
 SELECT * FROM film_list_with_string_agg;

--As for last_day I can say that there is not function which returns the last month day for given day  (https://www.postgresql.org/docs/9.1/functions-datetime.html)



--TASK 4.4.The �get_customer_balance� function describes the business requirements for calculating the client balance. Unfortunately, not all of
--them are implemented in this function. Try to change function using the requirements from the comments
--It seems clause 3 (IF A FILM IS MORE THAN RENTAL_DURATION * 2 OVERDUE, CHARGE THE REPLACEMENT_COST) is not implemented
--Let's add it. We can get REPLACEMENT_COST from the film table.
--PLEASE NOTICE: I've renamed the function get_customer to get_customer_balance_task
CREATE OR REPLACE FUNCTION public.get_customer_balance_task(p_customer_id integer, p_effective_date timestamp with time zone)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
       --#OK, WE NEED TO CALCULATE THE CURRENT BALANCE GIVEN A CUSTOMER_ID AND A DATE
       --#THAT WE WANT THE BALANCE TO BE EFFECTIVE FOR. THE BALANCE IS:
       --#   1) RENTAL FEES FOR ALL PREVIOUS RENTALS
       --#   2) ONE DOLLAR FOR EVERY DAY THE PREVIOUS RENTALS ARE OVERDUE
       --#   3) IF A FILM IS MORE THAN RENTAL_DURATION * 2 OVERDUE, CHARGE THE REPLACEMENT_COST
       --#   4) SUBTRACT ALL PAYMENTS MADE BEFORE THE DATE SPECIFIED
DECLARE
    v_rentfees DECIMAL(5,2); --#FEES PAID TO RENT THE VIDEOS INITIALLY
    v_overfees INTEGER;      --#LATE FEES FOR PRIOR RENTALS
    v_payments DECIMAL(5,2); --#SUM OF PAYMENTS MADE PREVIOUSLY
-- I've declared v_replacementfees    
    v_replacementfees DECIMAL(5,2); --fees if film is more then ENTAL_DURATION * 2 overdue
BEGIN
    SELECT COALESCE(SUM(film.rental_rate),0) INTO v_rentfees
    FROM film, inventory, rental
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

    SELECT COALESCE(SUM(CASE 
                           WHEN (rental.return_date - rental.rental_date) > (film.rental_duration * '1 day'::interval)
                           THEN EXTRACT(epoch FROM ((rental.return_date - rental.rental_date) - (film.rental_duration * '1 day'::interval)))::INTEGER / 86400 -- * 1 dollar
                           ELSE 0
                        END),0) 
    INTO v_overfees
    FROM rental, inventory, film
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

    SELECT COALESCE(SUM(payment.amount),0) INTO v_payments
    FROM payment
    WHERE payment.payment_date <= p_effective_date
    AND payment.customer_id = p_customer_id;

--SELECT statement is to sum replacement fees in case if they should be applied
SELECT COALESCE(SUM(CASE 
                           WHEN (rental.return_date - rental.rental_date) > (film.rental_duration * 2 * '1 day'::interval)
                           THEN film.replacement_cost -- * + replacement_cost
                           ELSE 0
                        END),0) 
    INTO v_replacementfees
    FROM rental, inventory, film
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

-- And finally I've added v_replacementfees to a balance
    RETURN v_rentfees + v_overfees - v_payments + v_replacementfees;
END
$function$
;

SELECT * FROM get_customer_balance_task (4,'2020-12-25')
UNION ALL
SELECT * FROM get_customer_balance (4,'2021-12-25');


--TASK4.5. How do �group_concat� and �_group_concat� functions work? (database creation script might help) Where are they used?
--It's easyer to start with _GROUP_CONCAT function.
--It gets two text arguments and contacts them together according to CASE condition($1 and $2 may be NULL. If both of them are NULL result is NULL)
CREATE FUNCTION _group_concat(text, text) RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $_$
SELECT CASE
  WHEN $2 IS NULL THEN $1
  WHEN $1 IS NULL THEN $2
  ELSE $1 || ', ' || $2
END
$_$;

--GROUP_CONCAT is an aggregate function. It was defined as CREATE AGGREGATE group_concat(text)...
--GROUP_CONCAT(TEXT) means input arguments must have TEXT data type, STYPE = text means output has TEXT data type, SFUNC = _group_concat means AGGREGATE is executed according to rules from _group_concat(text, text) function.
--So far we have figured out it is aggregate function. So that group_concat(text) is executed after GROUP BY and we must put it between SELECT and FROM
SELECT group_concat (title) FROM film; --we can use group_concat to aggregate all titles according to _group_concat rules

--As GROUP_CONCAT is aggregate function it is executed row by row and after GROUP BY (1st step: row1+row2=output1,
																					--2nd ster: output1+row3=output2,
																					--3rd ster: output2+row4=output3
																					--and so on till the end
--It works in the same way with string_agg (with the same behaviour around NULL values)
CREATE AGGREGATE group_concat(text) (
    SFUNC = _group_concat,
    STYPE = text
);

--group_concat is used for creating actor_info, film_list, nicer_but_slower_film_list vies
--For actor_info: group_concat creates 'film_info' column with all films divided by category an actor took part in
--For film_list: group_concat creates 'actors'  column with all actors took part in a film. All names in UPPERcase
--For nicer_but_slower_film_list: it creates 'actors' column with all actors took part in a film. 1st letters from first_name and lats_name are UPPER and the other letters are LOWER. SUBSTRING method is applied 



--TASK 4.6.  What does �last_updated� function do? Where is it used?
--LAST_UPDATED() is a trigger function with no argument because TRIGGER type (trigger when altering data) is supposed to have no argument.
--When the UPDATE statement in an actor/film/country/etc. tables  is run a NEW variable: 1. get row for insert or update (in our case for update)
																					   --2. NEW.last_update = CURRENT_TIMESTAMP statement added CURRENT_TIMESTAMP for last_update attribute in NEW
																					   --3. finally last_update function returns full NEW row for for insert or update 
CREATE FUNCTION last_updated() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    NEW.last_update = CURRENT_TIMESTAMP;
    RETURN NEW;
END $$;
--LAST_UPDATED() is used to create following triggers:
/*
CREATE TRIGGER last_updated BEFORE UPDATE ON actor FOR EACH ROW EXECUTE PROCEDURE last_updated();
CREATE TRIGGER last_updated BEFORE UPDATE ON address FOR EACH ROW EXECUTE PROCEDURE last_updated();
CREATE TRIGGER last_updated BEFORE UPDATE ON category FOR EACH ROW EXECUTE PROCEDURE last_updated();
CREATE TRIGGER last_updated BEFORE UPDATE ON city FOR EACH ROW EXECUTE PROCEDURE last_updated();
CREATE TRIGGER last_updated BEFORE UPDATE ON country FOR EACH ROW EXECUTE PROCEDURE last_updated();
CREATE TRIGGER last_updated BEFORE UPDATE ON customer FOR EACH ROW EXECUTE PROCEDURE last_updated();
CREATE TRIGGER last_updated BEFORE UPDATE ON film FOR EACH ROW EXECUTE PROCEDURE last_updated();
CREATE TRIGGER last_updated BEFORE UPDATE ON film_actor FOR EACH ROW EXECUTE PROCEDURE last_updated();
CREATE TRIGGER last_updated BEFORE UPDATE ON film_category FOR EACH ROW EXECUTE PROCEDURE last_updated();
CREATE TRIGGER last_updated BEFORE UPDATE ON inventory FOR EACH ROW EXECUTE PROCEDURE last_updated();
CREATE TRIGGER last_updated BEFORE UPDATE ON language FOR EACH ROW EXECUTE PROCEDURE last_updated();
CREATE TRIGGER last_updated BEFORE UPDATE ON rental FOR EACH ROW EXECUTE PROCEDURE last_updated();
CREATE TRIGGER last_updated BEFORE UPDATE ON staff FOR EACH ROW EXECUTE PROCEDURE last_updated();
CREATE TRIGGER last_updated BEFORE UPDATE ON store FOR EACH ROW EXECUTE PROCEDURE last_updated();
*/



--TASK 4.7. What is tmpSQL variable for in �rewards_report� function? Can this function be recreated without EXECUTE statement and dynamic SQL? Why?

/*
tmpSQL variable is to hold string to execute code from string with execute statement. Dynamic code does not execute without EXECUTE statement. For this reason
if we speak about  dynamic SQL we cannot recreate it without EXECUTE for sure. But without dynamic SQL we can recreate it. 
As  prove I've attached following code.
*/

CREATE OR REPLACE FUNCTION rewards_report_home_task_w (min_monthly_purchases integer, min_dollar_amount_purchased numeric) RETURNS SETOF customer
    LANGUAGE plpgsql SECURITY DEFINER
    AS $_$
DECLARE
    last_month_start DATE;
    last_month_end DATE;
rr RECORD;
tmpSQL TEXT;
BEGIN

    /* Some sanity checks... */
    IF min_monthly_purchases = 0 THEN
        RAISE EXCEPTION 'Minimum monthly purchases parameter must be > 0';
    END IF;
    IF min_dollar_amount_purchased = 0.00 THEN
        RAISE EXCEPTION 'Minimum monthly dollar amount purchased parameter must be > $0.00';
    END IF;

    last_month_start := CURRENT_DATE - '48 month'::interval;
    last_month_start := to_date((extract(YEAR FROM last_month_start) || '-' || extract(MONTH FROM last_month_start) || '-01'),'YYYY-MM-DD');
    last_month_end := LAST_DAY(last_month_start);

    /*
    Create a temporary storage area for Customer IDs.
    */
    CREATE TEMPORARY TABLE tmpCustomer (customer_id INTEGER NOT NULL PRIMARY KEY);

    /*
    Find all customers meeting the monthly purchase requirements. And insert values into tmpCustomer
    */

    	INSERT INTO tmpCustomer (customer_id)
        SELECT p.customer_id
        FROM payment AS p
        WHERE DATE(p.payment_date) BETWEEN last_month_start AND last_month_end
        GROUP BY customer_id
        HAVING SUM(p.amount) > min_dollar_amount_purchased
        AND COUNT(customer_id) > min_monthly_purchases;

    /*
    Output ALL customer information of matching rewardees.
    Customize output as needed.
    */
    RETURN query
    
    SELECT c.* 
    FROM tmpCustomer AS t 
    INNER JOIN customer AS c ON t.customer_id = c.customer_id;

    /* Clean up */
    DROP TABLE tmpCustomer;

RETURN;
END
$_$;

SELECT * FROM rewards_report_home_task_w (3,40) -- Query to test


/* I've put following code to expalain why we do not need dinamic SQL in �rewards_report� function
 As for �rewards_report� function we are able to use variable names (not variable values) becouse  operators >, <, =, BETWEEN 
 compared variable values even if we use variable names in queries.
Calling tables another story. We have to use variable values to alter tables. 
It means we cannot use  variable names. 
For example: variable_name = 'customers' and
"SELECT * FROM variable_name" does not work. 
"SELECT * FROM customers" works.
 */

CREATE OR REPLACE FUNCTION test_func (test_string TEXT) 
RETURNS TEXT
    LANGUAGE plpgsql
    AS $_$
DECLARE
    
	rr TEXT;

BEGIN
	--Dynamic SQL to INSERT into table with "dynamic name"
	rr := 'INSERT INTO '|| test_string ||' (country) 
		   VALUES ($$New_country$$)';
	EXECUTE rr;  
 
RETURN rr;	
END
$_$;

SELECT * FROM test_func ('country');