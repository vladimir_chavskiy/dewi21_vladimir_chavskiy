--TASK 1. Create database that 
--1.1. monitors workload
--1.2. capabilities and activities of our city's health institutions. 

--The database needs to represent 
--2.1. institutions
--2.2. their locations
--2.3. staffing
--2.4. capacity
--2.5. capabilities
--2.6. patients' visits.

/*
As for 1.1 and 1.2 my model contains VISIT, SCHEDUAL and PROCEDURE_LIST table. 
With VISIT (+several JOINs) we easily can evaluate workload for period of time we are interested in.

PROCEDURE_LIST shows activities, and SCHEDUAL let us to calculate capabilities 
(because PROCEDURE_LIST contains attribute �procedure_duration�)

INSTITUTION and STAFF hold information we need for clauses 2.1, 2.2, 2.3. 

SCHEDULE and PROCEDURE_LIST let us to get capacity (2.4) data (as we know schedule and procedure duration.

INSTITUTION together with PROCEDURE_INSTITUTION hold data about capabilities (2.5)

Finally there is VISIT for 2.6
*/

--So far so good. Let's start

CREATE DATABASE hospital;


--CREATING all tables
CREATE TABLE address 
(
	address_id SERIAL PRIMARY KEY,
	zip VARCHAR(9),    
    place VARCHAR NOT NULL,
    street VARCHAR,
    house VARCHAR NOT NULL,  
    house_index VARCHAR,
    apartment VARCHAR
);

CREATE TABLE institution
(
    institution_id SERIAL PRIMARY KEY, 
    title VARCHAR NOT NULL,
    status VARCHAR CHECK (status IN ('HOSPITAL', 'LABORATORY', 'POLYCLINIC')),
    address_id INTEGER NOT NULL,
    tel_number VARCHAR NOT NULL --I've left tel_numders in institution, visitor and staff tables. It is NOT NULL for institution (that is reason).
);

CREATE TABLE visitor
(
    visitor_id SERIAL PRIMARY KEY, 
    first_name VARCHAR NOT NULL, 
    last_name VARCHAR NOT NULL,
    patro_name VARCHAR,       
    date_of_birth DATE NOT NULL,
    document_type VARCHAR CHECK (document_type IN ('PASSPORT', 'ID', 'RESIDENCE PERMIT')), 
    ssn VARCHAR NOT NULL UNIQUE,
    address_id INTEGER NOT NULL,
    tel_number VARCHAR
);

CREATE TABLE staff
(
    staff_id SERIAL PRIMARY KEY, 
    first_name VARCHAR NOT NULL, 
    last_name VARCHAR NOT NULL,
    patro_name VARCHAR,
    date_of_birth DATE CHECK (date_of_birth <= CURRENT_DATE - INTERVAL '18 year'),
    document_type VARCHAR CHECK (document_type IN ('PASSPORT', 'ID', 'RESIDENCE PERMIT')), 
    ssn VARCHAR NOT NULL UNIQUE,
    tel_number VARCHAR,
    address_id INTEGER NOT NULL,
    email VARCHAR GENERATED ALWAYS AS (first_name||'.'||last_name||'.'||staff_id::TEXT||'@MEDICINE.COM') STORED
);

CREATE TABLE procedure_list
(
	procedure_id SERIAL PRIMARY KEY,
	procedure_title VARCHAR NOT NULL,
	procedure_duration DECIMAL(5,2) NOT NULL
);

CREATE TABLE procedure_staff
(
	procedure_staff_id SERIAL PRIMARY KEY,
	procedure_id INTEGER NOT NULL,
	staff_id INTEGER NOT NULL
);

CREATE TABLE procedure_institution
(
	procedure_institution_id SERIAL PRIMARY KEY,
	procedure_id INTEGER NOT NULL,
	institution_id INTEGER NOT NULL
);

CREATE TABLE visit
(
	visit_id SERIAL PRIMARY KEY,
	visitor_id INTEGER NOT NULL,
	institution_id INTEGER NOT NULL,
	procedure_staff_id INTEGER NOT NULL,
	visit_date_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE schedule 
(
	id SERIAL PRIMARY KEY,
	procedure_staff_id INTEGER NOT NULL,
	day_of_week_num SMALLINT CHECK (day_of_week_num BETWEEN 1 AND 7),
	from_time SMALLINT CHECK (from_time BETWEEN 0 AND 23),
	till_time SMALLINT CHECK (till_time BETWEEN 0 AND 23),
	institution_id INTEGER NOT NULL
);

CREATE TABLE institution_staff
(
	institution_staff_id SERIAL PRIMARY KEY,
	institution_id INTEGER NOT NULL,
	staff_id INTEGER NOT NULL
);

ALTER TABLE institution
ADD FOREIGN KEY (address_id) REFERENCES address(address_id);

ALTER TABLE visitor 
ADD FOREIGN KEY (address_id) REFERENCES address(address_id);

ALTER TABLE staff
ADD FOREIGN KEY (address_id) REFERENCES address(address_id);

ALTER TABLE institution_staff
ADD FOREIGN KEY (institution_id) REFERENCES institution(institution_id);

ALTER TABLE institution_staff
ADD FOREIGN KEY (staff_id) REFERENCES staff(staff_id);

ALTER TABLE procedure_institution 
ADD FOREIGN KEY (institution_id) REFERENCES institution(institution_id);

ALTER TABLE procedure_institution 
ADD FOREIGN KEY (procedure_id) REFERENCES procedure_list(procedure_id);

ALTER TABLE procedure_staff 
ADD FOREIGN KEY (staff_id) REFERENCES staff(staff_id);

ALTER TABLE procedure_staff 
ADD FOREIGN KEY (procedure_id) REFERENCES procedure_list(procedure_id);

ALTER TABLE visit 
ADD FOREIGN KEY (visitor_id) REFERENCES visitor(visitor_id);

ALTER TABLE visit 
ADD FOREIGN KEY (institution_id) REFERENCES institution(institution_id);

ALTER TABLE visit 
ADD FOREIGN KEY (procedure_staff_id) REFERENCES procedure_staff(procedure_staff_id);

ALTER TABLE schedule
ADD FOREIGN KEY (procedure_staff_id) REFERENCES procedure_staff(procedure_staff_id);

ALTER TABLE schedule
ADD FOREIGN KEY (institution_id) REFERENCES institution(institution_id);

--Inserting into address
INSERT INTO address (zip, place, street, house, house_index, apartment)
VALUES ('212014', 'MOGILEV', 'MIRA', '12', NULL, '125'),
	   ('212014', 'MOGILEV', 'MIRA', '18', NULL, '132'),
	   ('212014', 'MOGILEV', 'MIRA', '19', NULL, '175'),
	   ('212000', 'MOGILEV', 'PUSINA', '12', NULL, NULL),
	   ('212000', 'MOGILEV', 'PUSINA', '125', NULL, NULL),
	   ('212000', 'MOGILEV', 'PUSINA', '1', NULL, NULL),
	   ('212000', 'MOGILEV', 'PUSINA', '132', NULL, NULL),
	   ('212000', 'MOGILEV', 'PUSINA', '112', NULL, NULL);

--Inserting into visitors
INSERT INTO visitor (first_name , last_name, patro_name, date_of_birth, document_type, ssn, address_id, tel_number)
VALUES ('NIKITA', 'POPOV', 'ALEKSANDROVICH', '1988-09-10', 'PASSPORT', 'DA0829834567567', (SELECT address_id FROM address WHERE street = 'MIRA' AND house = '12' AND apartment = '125'), '00319586754'),
	   ('EKATERINA', 'POPOVA', 'ALEKSANDROVNA', '1989-09-10', 'PASSPORT', 'DB0829834567561', (SELECT address_id FROM address WHERE street = 'MIRA' AND house = '12' AND apartment = '125'),'00319586755'),
	   ('ANDREY', 'KROT', 'ALEKSANDROVICH', '1995-01-12', 'ID', 'SQ0829834567567', (SELECT address_id FROM address WHERE street = 'MIRA' AND house = '12' AND apartment = '125'), '375293457654'),
	   ('SVETRANA', 'LOMONOS', 'ALEKSANDROVNA', '1989-12-10', 'PASSPORT', 'UY08QW834567561', (SELECT address_id FROM address WHERE street = 'MIRA' AND house = '12' AND apartment = '125'), '0295768543'),
	   ('LEONID', 'OSTROVSKIY', 'VLADIMIROVICH', '2000-09-10', 'PASSPORT', 'LQ0829834567567', (SELECT address_id FROM address WHERE street = 'MIRA' AND house = '12' AND apartment = '125'), '375296879876'),
	   ('OLGA', 'KAZAKOVA', 'ALEKSANDROVNA', '1975-12-04', 'PASSPORT', 'DB0829834567545', (SELECT address_id FROM address WHERE street = 'MIRA' AND house = '12' AND apartment = '125'), '375254657678');

--Inserting into institutions
INSERT INTO institution (title, status, address_id, tel_number)
VALUES ('NORTH H+', 'HOSPITAL', (SELECT address_id FROM address WHERE street = 'PUSINA' AND house = '12'), '0222425689'),
	   ('SOUTH H+', 'LABORATORY', (SELECT address_id FROM address WHERE street = 'PUSINA' AND house = '125'), '0222456765'),
	   ('WEST H+', 'HOSPITAL', (SELECT address_id FROM address WHERE street = 'PUSINA' AND house = '1'), '0222234543'),
	   ('EAST H+', 'HOSPITAL', (SELECT address_id FROM address WHERE street = 'PUSINA' AND house = '132'), '0222213456'),
	   ('CENTER H+', 'POLYCLINIC', (SELECT address_id FROM address WHERE street = 'PUSINA' AND house = '112'), '0222489765');

--Inserting into procedure_list
INSERT INTO procedure_list (procedure_title, procedure_duration)
VALUES ('HEART ULTRASONIC SCAN', 1.5),
	   ('BLOOD TEST', 0.5),
	   ('DIET CONSULTATION', 1),
	   ('MASSAGE', 1),
	   ('X-RAY', 0.25);

--Inserting into staff	  
INSERT INTO staff (first_name , last_name, patro_name, date_of_birth, document_type, ssn, address_id, tel_number)
VALUES ('GLEB', 'POPOV', 'ALEKSANDROVICH', '1988-09-10', 'PASSPORT', 'AD0829834567567', (SELECT address_id FROM address WHERE street = 'MIRA' AND house = '18' AND apartment = '132'), '00319534754'),
	   ('DARIYA', 'POPOVA', 'ALEKSANDROVNA', '1989-09-10', 'PASSPORT', 'DF0829834567561', (SELECT address_id FROM address WHERE street = 'MIRA' AND house = '18' AND apartment = '132'), '00319586765'),
	   ('DENIS', 'KROT', 'ALEKSANDROVICH', '1995-01-12', 'PASSPORT', 'ER0829834567567', (SELECT address_id FROM address WHERE street = 'MIRA' AND house = '18' AND apartment = '132'), '375293127654'),
	   ('GALINA', 'LOMONOS', 'ALEKSANDROVNA', '1989-12-10', 'PASSPORT', 'UY0843534567561', (SELECT address_id FROM address WHERE street = 'MIRA' AND house = '19' AND apartment = '175'), '0295938543'),
	   ('VIKTOR', 'OSTROVSKIY', 'VLADIMIROVICH', '2000-09-10', 'PASSPORT', 'LQ0825634567567', (SELECT address_id FROM address WHERE street = 'MIRA' AND house = '19' AND apartment = '175'), '375676879876'),
	   ('ANASTASIYA', 'KAZAKOVA', 'ALEKSANDROVNA', '1975-12-04', 'PASSPORT', 'DB0829568567545', (SELECT address_id FROM address WHERE street = 'MIRA' AND house = '19' AND apartment = '175'), '375209657678');

	  
--Inserting into institution_staff	 	  
INSERT INTO institution_staff (institution_id, staff_id)
SELECT i.institution_id, s.staff_id 
FROM institution i, staff s
WHERE i.title = 'NORTH H+' AND s.ssn = 'AD0829834567567'

UNION

SELECT i.institution_id, s.staff_id 
FROM institution i, staff s
WHERE i.title = 'SOUTH H+' AND s.ssn = 'DF0829834567561'

UNION
SELECT i.institution_id, s.staff_id 
FROM institution i, staff s
WHERE i.title = 'WEST H+' AND s.ssn = 'ER0829834567567'

UNION

SELECT i.institution_id, s.staff_id 
FROM institution i, staff s
WHERE i.title = 'EAST H+' AND s.ssn = 'UY0843534567561'

UNION

SELECT i.institution_id, s.staff_id 
FROM institution i, staff s
WHERE i.title = 'CENTER H+' AND s.ssn = 'LQ0825634567567'

UNION

SELECT i.institution_id, s.staff_id 
FROM institution i, staff s
WHERE i.title = 'CENTER H+' AND s.ssn = 'DB0829568567545';


--Inserting into procedure_institution. There are all procedures in HOSPITALS and POLYCLINICS  
INSERT INTO procedure_institution (procedure_id, institution_id)
SELECT pl.procedure_id, i.institution_id 
FROM procedure_list pl
CROSS JOIN institution i
WHERE i.status != 'LABORATORY';

 		--Inserting BLOOD TEST into LABORATORY
INSERT INTO procedure_institution (procedure_id, institution_id)
SELECT pl.procedure_id, i.institution_id
FROM procedure_list pl, institution i
WHERE pl.procedure_title = 'BLOOD TEST' AND i.status = 'LABORATORY';

--Inserting into procedure_staff
INSERT INTO procedure_staff (procedure_id, staff_id)     --The first 3 doctors are able to work with the first 2 procedures
SELECT pl.procedure_id, s.staff_id 						 
FROM procedure_list pl
CROSS JOIN staff s
WHERE pl.procedure_title IN ('HEART ULTRASONIC SCAN', 'X-RAY') AND s.ssn IN ('AD0829834567567', --And the second 3 doctors are able to work with the last 3 procedures
																			 'DF0829834567561',
																			 'ER0829834567567')
UNION

SELECT pl.procedure_id, s.staff_id 
FROM procedure_list pl
CROSS JOIN staff s
WHERE pl.procedure_title IN ('BLOOD TEST', 'DIET CONSULTATION', 'MASSAGE') AND s.ssn IN ('UY0843534567561',
																					     'LQ0825634567567',
																						 'DB0829568567545');


--Inserting into schedule
INSERT INTO schedule (procedure_staff_id, day_of_week_num, from_time, till_time, institution_id)
SELECT DISTINCT ON (s.staff_id) ps.procedure_staff_id, 1, 13, 21, i.institution_id 
FROM procedure_staff ps
JOIN procedure_list pl ON ps.procedure_id = pl.procedure_id 
JOIN staff s ON ps.staff_id = s.staff_id
JOIN procedure_institution pin ON pl.procedure_id = pin.procedure_id 
JOIN institution i ON pin.institution_id = i.institution_id
WHERE i.status != 'LABORATORY'; --I wanted to add data in the fastest way. As a laboratory offers not all procedures I exepted it.

--Inserting into visit
		--It is just inserting into VISIT data with the same date (it might be because day is long)
--I executed 4 following INSERTs 1-5 time ns for tasting result from TASK 2 (+I inserted several single rows with hardcode approach)
INSERT INTO visit (visitor_id, institution_id, procedure_staff_id, visit_date_time)
SELECT DISTINCT ON (ist.institution_id) vis.visitor_id, ist.institution_id, ps.procedure_staff_id, '2021-01-15'
FROM institution_staff ist
JOIN procedure_staff ps ON ist.staff_id = ps.staff_id
CROSS JOIN (SELECT v.visitor_id 
			FROM visitor v 
			WHERE v.ssn = 'LQ0829834567567') vis;

INSERT INTO visit (visitor_id, institution_id, procedure_staff_id, visit_date_time)
SELECT DISTINCT ON (ist.institution_id) vis.visitor_id, ist.institution_id, ps.procedure_staff_id, '2021-01-15'
FROM institution_staff ist
JOIN procedure_staff ps ON ist.staff_id = ps.staff_id
CROSS JOIN (SELECT v.visitor_id 
			FROM visitor v 
			WHERE v.ssn = 'DB0829834567561') vis;
			
INSERT INTO visit (visitor_id, institution_id, procedure_staff_id, visit_date_time)
SELECT DISTINCT ON (ist.institution_id) vis.visitor_id, ist.institution_id, ps.procedure_staff_id, '2020-12-15'
FROM institution_staff ist
JOIN procedure_staff ps ON ist.staff_id = ps.staff_id
CROSS JOIN (SELECT v.visitor_id 
			FROM visitor v 
			WHERE v.ssn = 'LQ0829834567567') vis;
			
INSERT INTO visit (visitor_id, institution_id, procedure_staff_id, visit_date_time)
SELECT DISTINCT ON (ist.institution_id) vis.visitor_id, ist.institution_id, ps.procedure_staff_id, '2020-11-15'
FROM institution_staff ist
JOIN procedure_staff ps ON ist.staff_id = ps.staff_id
JOIN staff s ON ist.staff_id = s.staff_id 
CROSS JOIN (SELECT v.visitor_id 
			FROM visitor v 
			WHERE v.ssn = 'LQ0829834567567') vis
WHERE s.ssn != 'AD0829834567567';



--TASK 2. Write a query to identify doctors with insufficient workload (less than 5 patients a month for the past few months)
--I could check for AVG number = 5 for several month (it means 10 for 2 month or 15 for 3 month...). But I decided to check that there are at least 5 patients for each doctor in each month for the previous 3 month.

WITH

period_start AS 
	(SELECT (((EXTRACT (YEAR FROM current_date - '3 month'::INTERVAL))||'-'||(EXTRACT (MONTH FROM current_date - '3 month'::INTERVAL))||'-01')::DATE) AS first_day), --the first day of a 3 month period (We can easily change 3 to 4 or 5)
	
period_end AS
	(SELECT ((((EXTRACT (YEAR FROM current_date))||'-'||(EXTRACT (MONTH FROM current_date))||'-01')::DATE)-'1 day'::INTERVAL)::DATE AS last_day), --the last day of a 3 month period

less_three_month AS

    (SELECT not_all_three_month.staff_id, count(not_all_three_month.month_num)          --All doctors having no patients in at least one of a month
	 FROM
		(SELECT s.staff_id, EXTRACT (MONTH FROM v.visit_date_time) AS month_num
	 	 FROM staff s
		 LEFT JOIN procedure_staff ps ON s.staff_id = ps.staff_id 
	 	 LEFT JOIN visit v ON ps.procedure_staff_id = v.procedure_staff_id
	 	 WHERE v.visit_date_time BETWEEN (SELECT first_day FROM period_start) --data for previous 3 months 
	 	   				         AND (SELECT last_day FROM period_end)
	 	   	  OR v.visit_date_time IS NULL --It might be null because LEFT JOIN and we shoud pay attention to it 		         
	 	 GROUP BY s.staff_id, month_num
		 ORDER BY s.staff_id, month_num) not_all_three_month --If the doctor did not work in one of the months COUNT will show this father
	 GROUP BY not_all_three_month.staff_id
	 HAVING count(not_all_three_month.month_num)<3), -- This line is correct but my comment a little bit wrong. COUNT is equal to 0 If all 3 month is NULL (there were not patients in all 3 month).
	 												 -- I could not get 3 NULL if I would not use LEFT JOIN (a doctor with no patients would not appeared in a final selection). It means I calculate them (NULL values).

less_five_patients AS 

	(SELECT s.staff_id, EXTRACT (MONTH FROM v.visit_date_time) AS month_num, count(*) AS patients_num  --All doctors having less than 5  patients in at least one of a month
  	FROM staff s
 	JOIN procedure_staff ps ON s.staff_id = ps.staff_id --We need LEFT JOIN no more. We are not interested in NULL because it counted once in 'less_three_month' CTE and doctors with no patient in at least one month are in the 'less_three_month' selection.
	JOIN visit v ON ps.procedure_staff_id = v.procedure_staff_id --I've checked all months with no patients at all in LESS_THREE_MONTH CTE. For this reason JOIN (not LEFT JOIN) is more than enough
	WHERE v.visit_date_time BETWEEN (SELECT first_day FROM period_start) --data for previous 3 months 
	 	   				    AND (SELECT last_day FROM period_end)
	 	   	  -- OR V.VISIT_DATE_TIME IS NULL -- We need it no more because I've used JOIN (not LEFT JOIN)
	GROUP BY s.staff_id, month_num
	HAVING count(*) < 5)

	
SELECT s.staff_id, s.first_name||' '||s.last_name AS doctor_name   --All doctors  existing in LESS_THREE_MONTH OR LESS_FIVE_PATIENTS
FROM staff s 
WHERE EXISTS (SELECT staff_id  --Just in Russian: �� ��� ����� ������ � ��������;)
					FROM less_three_month 
					WHERE less_three_month.staff_id = s.staff_id)
					
	  OR EXISTS (SELECT staff_id  --And of course we need SUM (not PRODUCT) here
		   	          FROM less_five_patients 
					  WHERE less_five_patients.staff_id = s.staff_id);