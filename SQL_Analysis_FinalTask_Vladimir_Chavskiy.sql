--TASK 1. Build a query to create sales report that shows sales distribution by gender (F / M), marital status and by age (21-30 / 31-40 / 41-50 / 51-60 years). 
--Calculate the total sales based on the marital status of the customers
WITH tab AS ( --CTE to get all data for all age gaps
SELECT c.cust_gender, 
	   lower(c.cust_marital_status) AS cust_marital_status,
	   SUM (CASE WHEN 2000 - c.cust_year_of_birth BETWEEN 21 AND 30 THEN s.amount_sold END) AS "21-30",
	   SUM (CASE WHEN 2000 - c.cust_year_of_birth BETWEEN 31 AND 40 THEN s.amount_sold END) AS "31-40",
	   SUM (CASE WHEN 2000 - c.cust_year_of_birth BETWEEN 41 AND 50 THEN s.amount_sold END) AS "41-50",
	   SUM (CASE WHEN 2000 - c.cust_year_of_birth BETWEEN 51 AND 60 THEN s.amount_sold END) AS "51-60"
FROM sh.sales s 
JOIN sh.times t ON s.time_id = t.time_id 
JOIN sh.customers c ON s.cust_id = c.cust_id 
WHERE t.calendar_year = 2000 AND 
	  lower(cust_marital_status) IN ('single','married')
GROUP BY c.cust_gender,
	     lower(c.cust_marital_status))
	     
SELECT * FROM tab

UNION ALL
	     
SELECT '',
	   'Total for married',
	   sum (tab."21-30"),
	   sum (tab."31-40"),
	   sum (tab."41-50"),
	   sum (tab."51-60")
FROM tab 
WHERE tab.cust_marital_status = 'married'

UNION ALL

SELECT '',
	   'Total for single',
	   sum (tab."21-30"),
	   sum (tab."31-40"),
	   sum (tab."41-50"),
	   sum (tab."51-60")
FROM tab 
WHERE tab.cust_marital_status = 'single';



--TASK 2.Create an annual sales report broken down by calendar years and months. Annual sales for January of each year are presented in monetary units ($), 
--the remaining months should contain the dynamics of sales in percent relative to January.
CREATE EXTENSION IF NOT EXISTS tablefunc;


SELECT * FROM crosstab (
	$$
		SELECT tab.calendar_year, -- SELECT to get amount for Jen and percent for other months
			   tab.calendar_month_number,
			   to_char(CASE WHEN tab.calendar_month_number = 1 THEN tab.sum
			   ELSE tab.pers
			   END, '9999999990.00')::TEXT --proper formatting
		FROM (
			SELECT  t.calendar_year, --SELECT to get amount and percent relative to January
					t.calendar_month_number,
					sum (amount_sold),
					round((sum(amount_sold)/FIRST_VALUE(sum(amount_sold)) OVER (PARTITION BY t.calendar_year ORDER BY t.calendar_month_number))*100-100, 2) AS pers
			FROM sh.sales s 
			JOIN sh.times t ON s.time_id = t.time_id 
			GROUP BY t.calendar_year,
					t.calendar_month_number) tab
	$$) AS result_tab (calendar_year int2, 
					   "Jen $" TEXT,
					   "Feb %" TEXT,
					   "Mar %" TEXT,
					   "Apr %" TEXT,
					   "May %" TEXT,
					   "Jun %" TEXT,
					   "Jul %" TEXT,
					   "Aug %" TEXT,
					   "Sep %" TEXT,
					   "Oct %" TEXT,
					   "Nov %" TEXT,
					   "Dec %" TEXT);

					  
					  
--TASK 3 hich products were the most expensive (MAX (Costs.Unit_Price)) in their product category each year (1998-2001)?
SELECT prod_name
FROM (
	SELECT prod_name, prod_category_id, count(*) --if product has appeared as MAX in each year COUNT is 4
	FROM (
		SELECT DISTINCT *, --it leaves all distinct rows
		FROM (
			SELECT p.prod_name, --SELECT to get unit price and MAX price
			 	   c.unit_price,
				   max(c.unit_price) OVER (PARTITION BY t.calendar_year, p.prod_category_id) AS max_price,
				   t.calendar_year, 
				   p.prod_category_id
			FROM sh.costs c 
			JOIN sh.products p ON c.prod_id = p.prod_id 
			JOIN sh.times t ON c.time_id = t.time_id
			WHERE  t.calendar_year IN (1998, 1999, 2000, 2001)) tab
		WHERE unit_price = max_price) tab_2 --It leaves all products with MAX price
	GROUP BY prod_name, prod_category_id) tab_3
WHERE count=4;
	 
