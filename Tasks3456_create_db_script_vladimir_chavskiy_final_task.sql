--TASK 3. Add fictional data to your database (5+ rows per table, 50+ rows total across all tables). Save your data as DML scripts. 
--Make sure your surrogate keys' values are not included in DML scripts (they should be created runtime by the database, 
--as well as DEFAULT values where appropriate). DML scripts must successfully pass all previously created constraints.

--I used manual method to insert data into COUNTRY, CITY, ADDRESS, ZONE_TYPE, ESTATE_TYPE, CUSTOMER, REAL_ESTATE_AGENT and ESTATE table.
INSERT INTO country (country)
VALUES ('BELARUS'),
	   ('UKRAINE'),
	   ('THE NETHERLANDS'),
	   ('USA'),
	   ('SRI LANKA');
	  
INSERT INTO city (city, country_id)
VALUES ('MOGILEV',(SELECT country_id FROM country WHERE country = 'BELARUS')),
	   ('LVIV',(SELECT country_id FROM country WHERE country = 'UKRAINE')),
	   ('ROTERDAM',(SELECT country_id FROM country WHERE country = 'THE NETHERLANDS')),
	   ('JACKSONVILLE',(SELECT country_id FROM country WHERE country = 'USA')),
	   ('COLOMBO',(SELECT country_id FROM country WHERE country = 'SRI LANKA'));
	  
INSERT INTO address (zip, street, house, apartment, city_id)
VALUES ('212038', 'DNEPROVSKIY', '125', '28', (SELECT city_id FROM city WHERE city = 'MOGILEV')),
	   (NULL, 'SHEVCHENKO', '12', '23', (SELECT city_id FROM city WHERE city = 'LVIV')),
	   ('2053 HA', 'HAAGSEVEER', '1054', NULL, (SELECT city_id FROM city WHERE city = 'ROTERDAM')),
	   (NULL, 'MERRILL', '1034', NULL, (SELECT city_id FROM city WHERE city = 'JACKSONVILLE')),
	   ('00700', 'BASTIAN MAWATHA', '98', '12', (SELECT city_id FROM city WHERE city = 'COLOMBO')),
	   ('212038', 'DNEPROVSKIY', '126', '28', (SELECT city_id FROM city WHERE city = 'MOGILEV')),
	   (NULL, 'SHEVCHENKO', '13', '23', (SELECT city_id FROM city WHERE city = 'LVIV')),
	   ('2053 HA', 'HAAGSEVEER', '1055', NULL, (SELECT city_id FROM city WHERE city = 'ROTERDAM')),
	   (NULL, 'MERRILL', '1035', NULL, (SELECT city_id FROM city WHERE city = 'JACKSONVILLE')),
	   ('00700', 'BASTIAN MAWATHA', '99', '12', (SELECT city_id FROM city WHERE city = 'COLOMBO')),
	   ('212038', 'DNEPROVSKIY', '127', '28', (SELECT city_id FROM city WHERE city = 'MOGILEV')),
	   (NULL, 'SHEVCHENKO', '14', '23', (SELECT city_id FROM city WHERE city = 'LVIV')),
	   ('2053 HA', 'HAAGSEVEER', '1056', NULL, (SELECT city_id FROM city WHERE city = 'ROTERDAM')),
	   (NULL, 'MERRILL', '1036', NULL, (SELECT city_id FROM city WHERE city = 'JACKSONVILLE')),
	   ('00700', 'BASTIAN MAWATHA', '101', '12', (SELECT city_id FROM city WHERE city = 'COLOMBO'));
	  
INSERT INTO zone_type (zone_title)
VALUES ('CITY CENTER A'),
	   ('CITY CENTER B'),
	   ('NEAR THE CITY CENTER'),
	   ('SUBURBS'),
	   ('COUNTRYSIDE');
	  
INSERT INTO estate_type (type_title)
VALUES ('APARTMENT'),
	   ('SEPARED HOUSE'),
	   ('ROW HOUSE'),
	   ('VILLA'),
	   ('SMALL HOUSE');
	  
INSERT INTO document_type (document_type_title)
VALUES ('PASSPORT'),
	   ('ID'),
	   ('RESIDENCE PERMIT');
	  
INSERT INTO customer (first_name , last_name, patro_name, date_of_birth, document_type, ssn, address_id, tel_number)
VALUES ('NIKITA', 'POPOV', 'ALEKSANDROVICH', '1988-09-10', (SELECT document_type_id FROM document_type ORDER BY random() LIMIT 1), 'DA0829834567567', (SELECT address_id FROM address WHERE street = 'DNEPROVSKIY' AND house = '125'), '00319586754'),
	   ('EKATERINA', 'POPOVA', 'ALEKSANDROVNA', '1989-09-10', (SELECT document_type_id FROM document_type ORDER BY random() LIMIT 1), 'DB0829834567561', (SELECT address_id FROM address WHERE street = 'DNEPROVSKIY' AND house = '125'),'00319586755'),
	   ('ANDREY', 'KROT', 'ALEKSANDROVICH', '1995-01-12', (SELECT document_type_id FROM document_type ORDER BY random() LIMIT 1), 'SQ0829834567567', (SELECT address_id FROM address WHERE street = 'SHEVCHENKO' AND house = '12'), '375293457654'),
	   ('SVETRANA', 'LOMONOS', 'ALEKSANDROVNA', '1989-12-10', (SELECT document_type_id FROM document_type ORDER BY random() LIMIT 1), 'UY08QW834567561', (SELECT address_id FROM address WHERE street = 'HAAGSEVEER' AND house = '1054'), NULL),
	   ('LEONID', 'OSTROVSKIY', 'VLADIMIROVICH', '2000-09-10', (SELECT document_type_id FROM document_type ORDER BY random() LIMIT 1), 'LQ0829834567567', (SELECT address_id FROM address WHERE street = 'MERRILL' AND house = '1034'), '375296879876'),
	   ('OLGA', 'KAZAKOVA', 'ALEKSANDROVNA', '1975-12-04', (SELECT document_type_id FROM document_type ORDER BY random() LIMIT 1), 'DB0829834567545', (SELECT address_id FROM address WHERE street = 'BASTIAN MAWATHA' AND house = '98'), '375254657678');
	  
INSERT INTO real_estate_agent (first_name , last_name, patro_name, date_of_birth, document_type, ssn, address_id, tel_number)
VALUES ('GLEB', 'POPOV', 'ALEKSANDROVICH', '1988-09-10', (SELECT document_type_id FROM document_type ORDER BY random() LIMIT 1), 'AD0829834567567', (SELECT address_id FROM address WHERE street = 'DNEPROVSKIY' AND house = '126'), '00319534754'),
	   ('DARIYA', 'POPOVA', 'ALEKSANDROVNA', '1989-09-10', (SELECT document_type_id FROM document_type ORDER BY random() LIMIT 1), 'DF0829834567561', (SELECT address_id FROM address WHERE street = 'DNEPROVSKIY' AND house = '126'), '00319586765'),
	   ('DENIS', 'KROT', 'ALEKSANDROVICH', '1995-01-12', (SELECT document_type_id FROM document_type ORDER BY random() LIMIT 1), 'ER0829834567567', (SELECT address_id FROM address WHERE street = 'SHEVCHENKO' AND house = '13'), '375293127654'),
	   ('GALINA', 'LOMONOS', 'ALEKSANDROVNA', '1989-12-10', (SELECT document_type_id FROM document_type ORDER BY random() LIMIT 1), 'UY0843534567561', (SELECT address_id FROM address WHERE street = 'HAAGSEVEER' AND house = '1055'), '0295938543'),
	   ('VIKTOR', 'OSTROVSKIY', 'VLADIMIROVICH', '2000-09-10', (SELECT document_type_id FROM document_type ORDER BY random() LIMIT 1), 'LQ0825634567567', (SELECT address_id FROM address WHERE street = 'MERRILL' AND house = '1035'), '375676879876'),
	   ('ANASTASIYA', 'KAZAKOVA', 'ALEKSANDROVNA', '1975-12-04', (SELECT document_type_id FROM document_type ORDER BY random() LIMIT 1), 'DB0829568567545', (SELECT address_id FROM address WHERE street = 'BASTIAN MAWATHA' AND house = '99'), '375209657678');
	  
INSERT INTO estate (address_id, zone_id, type_id, rental_rate, sale_cost, number_of_rooms, square)
VALUES ((SELECT address_id FROM address WHERE street = 'DNEPROVSKIY' AND house = '127'), (SELECT zone_id FROM zone_type ORDER BY RANDOM () LIMIT 1), (SELECT type_id FROM estate_type ORDER BY RANDOM () LIMIT 1), 250, 65000, 5, 120.50),
	   ((SELECT address_id FROM address WHERE street = 'SHEVCHENKO' AND house = '14'), (SELECT zone_id FROM zone_type ORDER BY RANDOM () LIMIT 1), (SELECT type_id FROM estate_type ORDER BY RANDOM () LIMIT 1), 350, 70000, 5, 120.50),
	   ((SELECT address_id FROM address WHERE street = 'HAAGSEVEER' AND house = '1056'), (SELECT zone_id FROM zone_type ORDER BY RANDOM () LIMIT 1), (SELECT type_id FROM estate_type ORDER BY RANDOM () LIMIT 1), 550, 900000, 5, 120.50),
	   ((SELECT address_id FROM address WHERE street = 'MERRILL' AND house = '1036'), (SELECT zone_id FROM zone_type ORDER BY RANDOM () LIMIT 1), (SELECT type_id FROM estate_type ORDER BY RANDOM () LIMIT 1), 150, 0, 5, 120.50),
	   ((SELECT address_id FROM address WHERE street = 'BASTIAN MAWATHA' AND house = '101'), (SELECT zone_id FROM zone_type ORDER BY RANDOM () LIMIT 1), (SELECT type_id FROM estate_type ORDER BY RANDOM () LIMIT 1), 0, 100000, 5, 120.50); 

--I used automatic method to fill data into RENTAL, SALE, RENTAL_PAYMENT and SALE_PAYMENT TABLES.
--Anyone can buy or rent apartment anywhere.
DO
$$
DECLARE
     counter INTEGER;
BEGIN
FOR counter IN 1..50
LOOP
    INSERT INTO rental(estate_id , customer_id , agent_id, rental_date_start)  
    VALUES ((SELECT estate_id FROM estate ORDER BY RANDOM() LIMIT 1),   --ramdom ESTATE
   		   (SELECT customer_id FROM customer ORDER BY RANDOM() LIMIT 1), --random CUSTOMER
   		   (SELECT agent_id FROM real_estate_agent rea ORDER BY RANDOM() LIMIT 1), --random AGENT
   		   (now()-random()*('1 year'::interval))); --random rental_date
END LOOP;
END
$$;

DO
$$
DECLARE
    counter INTEGER;
BEGIN
FOR counter IN 1..50
LOOP
    INSERT INTO sale(estate_id , customer_id , agent_id, sale_date)  
    VALUES ((SELECT estate_id FROM estate ORDER BY RANDOM() LIMIT 1), --ramdom ESTATE
   		   (SELECT customer_id FROM customer ORDER BY RANDOM() LIMIT 1), --random CUSTOMER
   		   (SELECT agent_id FROM real_estate_agent rea ORDER BY RANDOM() LIMIT 1), --random AGENT
   		   (now()-random()*('1 year'::interval))); --random sale_date
END LOOP;
END
$$;

DO
$$
DECLARE
     counter INTEGER;
BEGIN
FOR counter IN 1..50
LOOP
    INSERT INTO rental_payment (rental_id, amount, payment_timestamp)  
    VALUES ((SELECT rental_id FROM rental ORDER BY random() LIMIT 1), random()*400, (now()-random()*('1 year'::interval))); --random rental_id, amount and payment_timestamp
END LOOP;
END
$$;

DO
$$
DECLARE
     counter INTEGER;
BEGIN
FOR counter IN 1..50
LOOP
    INSERT INTO sale_payment (sale_id, amount, payment_timestamp)  
    VALUES ((SELECT sale_id FROM sale ORDER BY random() LIMIT 1), random()*20000, (now()-random()*('1 year'::interval))); --random sale_id, amount and payment_timestamp
END LOOP;
END
$$;


 
--TASK 4. Create the following functions:
--� Function that UPDATEs data in one of your tables (input arguments: table's primary key value, column name and column value to UPDATE to).
--� Function that adds new transaction to your transaction table. Come up with input arguments and output format yourself. Make sure all transaction
--attributes can be set with the function (via their natural keys).

-- Create function that UPDATEs data in one of your tables (input arguments: table's primary key value, column name and column value to UPDATE to).
DROP FUNCTION estate_table_update;

CREATE OR REPLACE FUNCTION estate_table_update (pk_value INTEGER, column_name_foo TEXT, new_val TEXT) --Function allows to update estate table
RETURNS TABLE (id_to_update integer, column_to_update TEXT, old_value TEXT, new_value TEXT) --Function returns estate_id, column name to update, old value and new value

LANGUAGE plpgsql

AS $_$

DECLARE 

old_value TEXT;

BEGIN
	
	IF NOT EXISTS (SELECT * FROM estate -- Checking for case when there is not given estate_id
				   WHERE estate_id = pk_value)
	THEN 
		RAISE NOTICE 'There is no record with id: %', pk_value;
 		RETURN;
	END IF;
	
	IF NOT EXISTS (SELECT * FROM information_schema.COLUMNS -- Checking for case when wrong column name has been inserted
				   WHERE table_schema = 'public' AND table_name = 'estate' AND column_name = column_name_foo)
	THEN 
		RAISE NOTICE 'There is no column with name: %', column_name_foo;
 		RETURN;
	END IF;
    
	EXECUTE 'SELECT '||column_name_foo||'::TEXT FROM estate WHERE estate_id = '||pk_value INTO old_value;	--old_values holds old value

	EXECUTE 'UPDATE estate
			 SET '||column_name_foo||' = '||new_val||'
		     WHERE estate_id = '||pk_value;
		    
	RETURN query
	
	SELECT pk_value, column_name_foo, old_value, new_val::TEXT;
	
RETURN;
END 
$_$;

SELECT * FROM estate_table_update (3, 'sale_cost', '22346'); --IN parameters are  estate_id, column name and column value to UPDATE to

--Create Function that adds new transaction to your transaction table. Come up with input arguments and output format yourself.
--Make sure all transaction attributes can be set with the function (via their natural keys).
DROP FUNCTION adding_new_transaction;

CREATE OR REPLACE FUNCTION adding_new_transaction (rent_id INTEGER, amount_mon NUMERIC(7,2))
RETURNS SETOF rental_payment --Function returns a new row

LANGUAGE plpgsql

AS $_$


BEGIN
	IF NOT EXISTS (SELECT * FROM rental r --Checking for case when wrong id has been set
				   WHERE rental_id = rent_id)
	THEN
  		RAISE NOTICE 'rental_id % not found', rent_id;
 		RETURN;
	END IF;

	IF amount_mon > 99999 THEN
		RAISE NOTICE 'Amount must be less than 100000'; --According to the constraint
 		RETURN;
	END IF;
	
	RETURN query
	INSERT INTO rental_payment (rental_id, amount)
	VALUES ((SELECT rent_id), (SELECT amount_mon))
	RETURNING *;

RETURN;
END 
$_$;		     

SELECT * FROM adding_new_transaction(30, 4500);  



--TASK 5. Create view that joins all tables in your database and represents data in denormalized form for the past month. 
--Make sure to omit meaningless fields in the result (e.g. surrogate keys, duplicate fields, etc.).
DROP VIEW all_realestate_data_sales;

CREATE VIEW all_realestate_data_sales  
AS SELECT e.*,			--estate
		  a.zip, 		--address
		  a.street,
		  a.house,
		  a.house_index,
		  a.apartment,
		  c.city,		--city
		  c2.country,   --country
		  et.type_title, --estate_type
		  et.type_description,
		  zt.zone_title, --zone_type
		  zt.zone_description,
		  s.sale_date,	--sale
		  sp.amount,    --sale_payment
		  sp.payment_timestamp,
		  cust.first_name AS customer_first_name,   --customer
		  cust.last_name AS customer_last_name,
		  cust.patro_name AS customer_patro_name,
		  cust.date_of_birth AS customer_date_of_birth,
		  cust.document_type AS customer_document_type,
		  cust.ssn AS customer_ssn,
		  cust.tel_number AS customer_tel_number,
		  cust.email AS customer_email,
		  	acust.zip AS customer_zip, 		--customer address
		  	acust.street AS customer_street,
		  	acust.house AS customer_house,
		  	acust.house_index AS customer_index,
		  	acust.apartment AS customer_appartment,
		  	ccust.city AS customer_city,		--customer city
		  	cntcust.country AS customer_country,   --customer country
		  raa.first_name AS agent_first_name, --real_estate_agent
		  raa.last_name AS agent_last_name,
		  raa.patro_name AS agent_patro_name,
		  raa.date_of_birth AS agent_date_of_birth,
		  raa.document_type AS agent_document_type,
		  raa.ssn AS agent_ssn,
		  raa.tel_number AS agent_tel_number,
		  raa.email AS agent_email,
		  	araa.zip AS agent_zip, 		--agent address
		  	araa.street AS agent_street,
		  	araa.house AS agent_house,
		  	araa.house_index AS agent_index,
		  	araa.apartment AS agent_apartment,
		  	craa.city AS agent_city,		--agent city
		  	cntraa.country AS agent_country   --agent country
   FROM estate e
   JOIN address a ON e.address_id = a.address_id 
   JOIN city c ON a.city_id = c.city_id 
   JOIN country c2 ON c.country_id = c2.country_id
   JOIN estate_type et ON e.type_id = et.type_id 
   JOIN zone_type zt ON e.zone_id = zt.zone_id 
   LEFT JOIN sale s ON e.estate_id = s.estate_id --I used LEFT JOIN for sale and sale_payment because it might be a new estate with no sales or payments
   LEFT JOIN sale_payment sp ON s.sale_id = sp.sale_id 
   JOIN customer cust ON s.customer_id = cust.customer_id
   		JOIN address acust ON cust.address_id = acust.address_id  --joining customers with addresses
   		JOIN city ccust ON acust.city_id = ccust.city_id
   		JOIN country cntcust ON ccust.country_id = cntcust.country_id 
   JOIN real_estate_agent raa ON s.agent_id = raa.agent_id
   		JOIN address araa ON raa.address_id = araa.address_id
   		JOIN city craa ON araa.city_id = craa.city_id
   		JOIN country cntraa ON craa.country_id = cntraa.country_id
   WHERE date_trunc('month', s.sale_date) = date_trunc('month', current_date-'1 month'::interval) --I'm looking at payment date and sale day. I mean all activities around estate saling during a previous month
   		 OR date_trunc('month', sp.payment_timestamp) = date_trunc('month', current_date-'1 month'::interval); -- As for 'For the past month' my decision is a previous calendar month.
  
SELECT * FROM all_realestate_data_sales;

DROP VIEW all_realestate_data_rentals;
  
CREATE VIEW all_realestate_data_rentals 
AS SELECT e.*,			--estate
		  a.zip, 		--address
		  a.street,
		  a.house,
		  a.house_index,
		  a.apartment,
		  c.city,		--city
		  c2.country,   --country
		  et.type_title, --estate_type
		  et.type_description,
		  zt.zone_title, --zone_type
		  zt.zone_description,
		  r.rental_date_start,	--rental
		  r.rental_date_end,
		  rp.amount,    --sale_payment
		  rp.payment_timestamp,
		  cust.first_name AS customer_first_name,   --customer
		  cust.last_name AS customer_last_name,
		  cust.patro_name AS customer_patro_name,
		  cust.date_of_birth AS customer_date_of_birth,
		  cust.document_type AS customer_document_type,
		  cust.ssn AS customer_ssn,
		  cust.tel_number AS customer_tel_number,
		  cust.email AS customer_email,
		  	acust.zip AS customer_zip, 		--customer address
		  	acust.street AS customer_street,
		  	acust.house AS customer_house,
		  	acust.house_index AS customer_index,
		  	acust.apartment AS customer_appartment,
		  	ccust.city AS customer_city,		--customer city
		  	cntcust.country AS customer_country,   --customer country
		  raa.first_name AS agent_first_name, --real_estate_agent
		  raa.last_name AS agent_last_name,
		  raa.patro_name AS agent_patro_name,
		  raa.date_of_birth AS agent_date_of_birth,
		  raa.document_type AS agent_document_type,
		  raa.ssn AS agent_ssn,
		  raa.tel_number AS agent_tel_number,
		  raa.email AS agent_email,
		  	araa.zip AS agent_zip, 		--agent address
		  	araa.street AS agent_street,
		  	araa.house AS agent_house,
		  	araa.house_index AS agent_index,
		  	araa.apartment AS agent_apartment,
		  	craa.city AS agent_city,		--agent city
		  	cntraa.country AS agent_country   --agent country
   FROM estate e --joining ESTATE with all tables (except sale and sale_payment)
   JOIN address a ON e.address_id = a.address_id 
   JOIN city c ON a.city_id = c.city_id 
   JOIN country c2 ON c.country_id = c2.country_id
   JOIN estate_type et ON e.type_id = et.type_id 
   JOIN zone_type zt ON e.zone_id = zt.zone_id
   LEFT JOIN rental r ON e.estate_id = r.estate_id --I used LEFT JOIN for rental and rental_payment because it might be a new estate with no rentals or payments
   LEFT JOIN rental_payment rp ON r.rental_id = rp.rental_id 
   JOIN customer cust ON r.customer_id = cust.customer_id
   		JOIN address acust ON cust.address_id = acust.address_id  --joining customers with addresses
   		JOIN city ccust ON acust.city_id = ccust.city_id
   		JOIN country cntcust ON ccust.country_id = cntcust.country_id 
   JOIN real_estate_agent raa ON r.agent_id = raa.agent_id      --joining agents with addresses
   		JOIN address araa ON raa.address_id = araa.address_id
   		JOIN city craa ON araa.city_id = craa.city_id
   		JOIN country cntraa ON craa.country_id = cntraa.country_id
   WHERE date_trunc('month', r.rental_date_start) = date_trunc('month', current_date-'1 month'::interval) -- I'm looking at payment date, rental start day and rental end day
   		 OR date_trunc('month', r.rental_date_end) = date_trunc('month', current_date-'1 month'::interval) -- I mean all activities around estate renting during a previous month
   		 OR date_trunc('month', rp.payment_timestamp) = date_trunc('month', current_date-'1 month'::interval); -- As for 'For the past month' my decision is a previous calendar month.

SELECT * FROM all_realestate_data_rentals;
   		 
   		 

--TASK 6. Create manager's read-only role. Make sure he can only SELECT from tables in your database. Make sure he can 
--LOGIN as well. Make sure you follow database security best practices when creating role(s).
CREATE ROLE manager; --a new role 'manager' (without connect)

GRANT SELECT ON ALL TABLES IN SCHEMA public TO manager;	--granting on all tables in public to MANAGER role		 
																 
GRANT USAGE ON SCHEMA public TO manager; --MANAGER role has no access to public schema without this query

CREATE USER manager_bob_snail PASSWORD '@%mnhyadfasf*'; --Creating a new USER (role with connect). I've set password.
														--User are not able to connect to BD if password is not set
GRANT manager TO manager_bob_snail;  -- manager_bob_snail gets membership in manager role with this query

SET ROLE postgres;  --checking
SET ROLE manager;
SET ROLE manager_bob_snail;

SELECT * FROM sale s;

INSERT INTO rental_payment (rental_id, amount)
VALUES ((SELECT rental_id FROM rental LIMIT 1), 250);
