--Task 1 Create all tables for the relational model you created while studying DB Basics module (fixed model in 3nf). Create a separate database.

--I've decided to hold addresses for children and representatives not in separate ADDRESSES table but in children and representatives tables corespondantly.
--It seem better for business to create separate table. And if representative address changes it automatically changes for a child and another representative (if a child has several representatives).
--But relationships between representatives might be different. Imagine situation when representatives ('A' and 'B' person) are in divorcing process. 'A' person has changed an address in a kindergarten.
--Address changes automatically for 'B' person and child as well. As result really important letters from the kindergarten for 'B' goes to new address where no 'B' is living. Strange situation. Right?
--So, my decision was leaving address attributes for each tables. It works like as extra security measure.

CREATE DATABASE kindergarten_date;

CREATE SCHEMA kindergarten;

CREATE TABLE kindergarten.children 
(
    child_id SERIAL PRIMARY KEY, -- Surfing the internet helped me to make decisions about SERIAL
    first_name VARCHAR NOT NULL, 
    last_name VARCHAR NOT NULL,
    patro_name VARCHAR,      
    group_id INTEGER NOT NULL, --2^31 is more than enough for ID in a kindergarten
    date_of_birth DATE NOT NULL,
    document_type VARCHAR, -- Next I added the CHECK constraint (document_type IN ('PASSPORT', 'BIRTH CERTIFICATE', 'ID', 'RESIDENCE PERMIT'))
    ssn VARCHAR NOT NULL UNIQUE,
    zip VARCHAR(9),    --zip might include 6 - 9 characters. (9) is for reducing chance of mistakes.
    place VARCHAR NOT NULL,
    street VARCHAR,
    house VARCHAR NOT NULL,  -- It might be digit+letter (Sometimes they mark building as house + house_index but sometimes not and 12A might be house (not house + house_index)
    house_index VARCHAR,   -- It might be a digit or letter
    apartment VARCHAR      -- There are areas in Europe where apartment might mark as 7A, 7B, 7C (7 means a floor, A B C distinguish between flats on the each floor) 
);

CREATE TABLE kindergarten.payments
(
    payments_id SERIAL PRIMARY KEY,
    child_id INTEGER NOT NULL,
    payment_date DATE DEFAULT CURRENT_DATE,
    payment_period DATE NOT NULL,
    amount DECIMAL (6,2) NOT NULL  -- DECIMAL is proper data type for money 
);

CREATE TABLE kindergarten.benefits
(
    benefit_id SERIAL PRIMARY KEY,
    title VARCHAR NOT NULL,
    benefit_size DECIMAL (6,2) NOT NULL
);

CREATE TABLE kindergarten.children_benefits
(
    child_benefits_id SERIAL PRIMARY KEY,
    benefit_id INTEGER NOT NULL,
    child_id INTEGER NOT NULL
);

CREATE TABLE kindergarten.representatives
(
    rep_id SERIAL PRIMARY KEY,
    first_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL,
    patro_name VARCHAR,
    date_of_birth DATE NOT NULL,
    document_type VARCHAR,
    ssn VARCHAR NOT null UNIQUE,
    zip VARCHAR(9),
    place VARCHAR NOT NULL,
    street VARCHAR,
    house VARCHAR NOT NULL,
    house_index VARCHAR,
    apartment VARCHAR,
    tel_number BIGINT, --Further I swapped BIGINT for VARCHAR. BIGINT was my the first decision. Next I analyzed areas preferences about style and BIGINT seemed the proper type no more (BIGINT does not hold 0675943436)   
    email VARCHAR
);

CREATE TABLE kindergarten.children_representatives
(
    child_rep_id SERIAL PRIMARY KEY,
    rep_id INTEGER NOT NULL,
    child_id INTEGER NOT NULL
);

CREATE TABLE kindergarten.diets
(
    diet_id SERIAL PRIMARY KEY,
    diet_title VARCHAR NOT NULL
);

CREATE TABLE kindergarten.children_diets
(
    child_diet_id SERIAL PRIMARY KEY,
    diet_id INTEGER,
    child_id INTEGER NOT NULL
);

CREATE TABLE kindergarten.kind_groups
(
    group_id SERIAL PRIMARY KEY,
    group_title VARCHAR NOT NULL
);

CREATE TABLE kindergarten.staff
(
    staff_id SERIAL PRIMARY KEY,
    first_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL,
    patro_name VARCHAR,
    date_of_birth DATE NOT NULL,
    document_type VARCHAR,
    ssn VARCHAR NOT null UNIQUE,
    position_id INTEGER NOT NULL,
    zip VARCHAR(9),
    place VARCHAR NOT NULL,
    street VARCHAR,
    house VARCHAR NOT NULL,
    house_index VARCHAR,
    apartment VARCHAR,
    tel_number BIGINT,
    email VARCHAR
);

CREATE TABLE kindergarten.staff_position
(
    position_id SERIAL PRIMARY KEY,
    position_title VARCHAR NOT NULL
);

CREATE TABLE kindergarten.groups_staff
(
    group_staff_id SERIAL PRIMARY KEY,
    group_id INTEGER NOT NULL,
    staff_id INTEGER NOT NULL
);

CREATE TABLE kindergarten.courses
(
    course_id SERIAL PRIMARY KEY,
    courses_title VARCHAR NOT NULL
);

CREATE TABLE kindergarten.courses_staff
(
    course_staff_id SERIAL PRIMARY KEY,
    course_id INTEGER NOT NULL,
    staff_id INTEGER NOT NULL
);

CREATE TABLE kindergarten.children_courses
(
    child_course_id SERIAL PRIMARY KEY,
    course_id INTEGER NOT NULL,
    child_id INTEGER NOT NULL
);



-- Task 2 Create all table relationships with primary and foreign keys.

ALTER TABLE kindergarten.payments
ADD FOREIGN KEY (child_id) REFERENCES kindergarten.children;

ALTER TABLE kindergarten.children_benefits
ADD FOREIGN KEY (benefit_id) REFERENCES kindergarten.benefits;

ALTER TABLE kindergarten.children_benefits
ADD FOREIGN KEY (child_id) REFERENCES kindergarten.children;

ALTER TABLE kindergarten.children_representatives
ADD FOREIGN KEY (rep_id) REFERENCES kindergarten.representatives;

ALTER TABLE kindergarten.children_representatives
ADD FOREIGN KEY (child_id) REFERENCES kindergarten.children;

ALTER TABLE kindergarten.children_courses 
ADD FOREIGN KEY (course_id) REFERENCES kindergarten.courses;

ALTER TABLE kindergarten.children_courses 
ADD FOREIGN KEY (child_id) REFERENCES kindergarten.children;

ALTER TABLE kindergarten.children_diets 
ADD FOREIGN KEY (diet_id) REFERENCES kindergarten.diets;

ALTER TABLE kindergarten.children_diets 
ADD FOREIGN KEY (child_id) REFERENCES kindergarten.children;

ALTER TABLE kindergarten.groups_staff
ADD FOREIGN KEY (group_id) REFERENCES kindergarten.kind_groups;

ALTER TABLE kindergarten.groups_staff
ADD FOREIGN KEY (staff_id) REFERENCES kindergarten.staff;

ALTER TABLE kindergarten.staff 
ADD FOREIGN KEY (position_id) REFERENCES kindergarten.staff_position;

ALTER TABLE kindergarten.children 
ADD FOREIGN KEY (group_id) REFERENCES kindergarten.kind_groups;

ALTER TABLE kindergarten.courses_staff 
ADD FOREIGN KEY (staff_id) REFERENCES kindergarten.staff;

ALTER TABLE kindergarten.courses_staff 
ADD FOREIGN KEY (course_id) REFERENCES kindergarten.courses;



--Taks 3 Create at least 5 check constraints, not considering unique and not null, on your tables (in total 5, not for each table).

ALTER TABLE kindergarten.payments 
ADD CHECK (kindergarten.payments.payment_date >= kindergarten.payments.payment_period); --It is supposed to getting invoice before payment

ALTER TABLE kindergarten.children
ADD CHECK (document_type IN ('PASSPORT', 'BIRTH CERTIFICATE', 'ID', 'RESIDENCE PERMIT')); 

ALTER TABLE kindergarten.representatives 
ADD CHECK (document_type IN ('PASSPORT', 'ID', 'RESIDENCE PERMIT'));

ALTER TABLE kindergarten.staff 
ADD CHECK (document_type IN ('PASSPORT', 'ID', 'RESIDENCE PERMIT'));

ALTER TABLE kindergarten.representatives 
ADD CHECK (email LIKE '%@%.%');   --Checking email format

ALTER TABLE kindergarten.representatives 
ADD CHECK (date_of_birth <= CURRENT_DATE - INTERVAL '18 year');  --A representative must be at least 18 years aged;



-- Task 4 Fill your tables with sample data (create it yourself, 20+ rows total in all tables, make sure each table has at least 2 rows)

--I've used different ways to insert data.
		
	   --Inserting into kind_groups
INSERT INTO kindergarten.kind_groups (group_title)
VALUES ('RPE-JUNIOR A'),
	   ('JUNIOR A'),
	   ('UPPER A'),
	   ('RPE-JUNIOR B'),
	   ('JUNIOR B'),
	   ('UPPER B'),
	   ('INTERNATIONAL'),
	   ('LONG DAY GROUP'),
	   ('OFFICE');
	  
	  
	  --Inserting into children
	  --I've applied two approaches for MtoM relation. 1. CTE is with INSERT + RETURN 2. Inserting all children, inserting all representative, filling in a children_representatives table 
	--The 1st approache. ONE child ONE representative
WITH 

child_id_insert AS

	(INSERT INTO kindergarten.children (first_name , last_name, patro_name, group_id, date_of_birth, document_type, ssn, zip, place, street, house, house_index, apartment)
	 SELECT 'NIKITA', 'POPOV', 'ALEKSANDROVICH', kg.group_id, '2017-09-10', 'PASSPORT', 'DA0829834567567', '212014', 'MOGILEV', 'MIRA', '12', NULL, '125'
	 FROM kindergarten.kind_groups kg
	 WHERE kg.group_title = 'JUNIOR A'
	 RETURNING child_id),
	 
rep_id_insert AS

	(INSERT INTO kindergarten.representatives (first_name, last_name, patro_name, date_of_birth, document_type, ssn, zip, place, street, house, house_index, apartment, tel_number, email)
	 VALUES ('NIKOLAY', 'POPOV', 'ANDREEVICH', '1990-01-15'::DATE, 'PASSPORT', 'TR0986754767789', '212000', 'MOGILEV', '30 LET POBEDY', '10', 'A', '12B', 375441871209, NULL)
	 RETURNING rep_id)

INSERT INTO kindergarten.children_representatives (rep_id, child_id)
SELECT r.rep_id, c.child_id 
FROM rep_id_insert r, child_id_insert c;

	--The 1st approache. ONE child TWO representative. It works with MANY children MANY representative from one family as well  
WITH 

child_id_insert AS

	(INSERT INTO kindergarten.children (first_name , last_name, patro_name, group_id, date_of_birth, document_type, ssn, zip, place, street, house, house_index, apartment)
	 SELECT 'NIKA', 'SOKOLOVA', 'DMITRIEVNA', kg.group_id, '2017-03-15', 'PASSPORT', 'DT0829834567567', '212038', 'MOGILEV', 'DIMITROVA', '54', NULL, '81'
	 FROM kindergarten.kind_groups kg
	 WHERE kg.group_title = 'JUNIOR A'
	 RETURNING child_id),
	 
rep_id_insert AS

	(INSERT INTO kindergarten.representatives (first_name, last_name, patro_name, date_of_birth, document_type, ssn, zip, place, street, house, house_index, apartment, tel_number, email)
	 VALUES ('NIKOLAY', 'SOKOLOV', 'LEONIDOVICH', '1988-01-15', 'PASSPORT', 'AS0986754767789', '212038', 'MOGILEV', 'DIMITROVA', '54', NULL, '81', 375252020327, NULL),
	 		('ALEKSANDRA', 'SOKOLOVA', 'GLEBOVNA', '1988-08-10', 'PASSPORT', 'OP0986754767789', '212038', 'MOGILEV', 'DIMITROVA', '54', NULL, '81', 375252020328, NULL)
	 RETURNING rep_id)

INSERT INTO kindergarten.children_representatives (rep_id, child_id)
SELECT * 
FROM rep_id_insert r
CROSS JOIN child_id_insert c;	

	--The 2nd approache.
INSERT INTO kindergarten.children (first_name , last_name, patro_name, group_id, date_of_birth, document_type, ssn, zip, place, street, house, house_index, apartment)
SELECT 'GLEB', 'NIKOLAEV', 'SERGEEVICH', kg.group_id, '2017-01-28'::DATE, 'BIRTH CERTIFICATE', 'MP0829834567567', '212014', 'MOGILEV', 'MIRA', '25', NULL, '12'
FROM kindergarten.kind_groups kg
WHERE kg.group_title = 'JUNIOR A'

UNION 

SELECT 'EKATERINA', 'ZVONOVA', 'VLADIMIROVNA', kg.group_id, '2018-03-18'::DATE, 'BIRTH CERTIFICATE', 'QD0856904327567', '212000', 'MOGILEV', '30 LET POBEDY', '10', 'A', '12B'
FROM kindergarten.kind_groups kg
WHERE kg.group_title = 'RPE-JUNIOR A'

UNION 

SELECT 'OLGA', 'NIKITINA', 'ROMANOVNA', kg.group_id, '2017-06-23'::DATE, 'PASSPORT', 'QD56732043279433', '212038', 'MOGILEV', 'DIMITROVA', '5', '5', '137'
FROM kindergarten.kind_groups kg
WHERE kg.group_title = 'JUNIOR A'

UNION 

SELECT 'TOMAS', 'GROVSON', NULL, kg.group_id, '2018-01-05'::DATE, 'ID', '465-875-908', '212025', 'MOGILEV', 'PUSHKINA', '12', '1', '1'
FROM kindergarten.kind_groups kg
WHERE kg.group_title = 'INTERNATIONAL';

--Inserting into representatives
INSERT INTO kindergarten.representatives (first_name, last_name, patro_name, date_of_birth, document_type, ssn, zip, place, street, house, house_index, apartment, tel_number, email)
VALUES ('MARIYA', 'ZVONOVA', 'ZAHAROVNA', '1995-01-21', 'RESIDENCE PERMIT', 'FB4856908767789', '212000', 'MOGILEV', '30 LET POBEDY', '10', 'A', '12B', 375291871209, NULL),
	   ('LEONID', 'NIKITIN', 'VLADIMIROVICH', '1988-06-18', 'PASSPORT', 'MB3089786767789', '212038', 'MOGILEV', 'DIMITROVA', '5', '5', '137', 375292020326, 'LEONID.NIKITIN@GMAIL.COM'),
	   ('ANDREY', 'KOSHKIN', 'NIKOLAEVICH', '1980-03-08', 'PASSPORT', 'MS6678986767789', '212003', 'MOGILEV', 'TUROVA', '12', NULL, '54', 375222411817, NULL),
	   ('SVETLANA', 'NIKOLAEVA', 'SERGEEVNA', '1978-12-10', 'PASSPORT', 'PD0098600981234', '212014', 'MOGILEV', 'MIRA', '25', NULL, '12', 375333761245, 'SVATLANA_1978@MAIL.RU'),
	   ('DANA', 'TOMAS', NULL, '1990-05-10', 'RESIDENCE PERMIT', '876-009-012', '212025', 'MOGILEV', 'PUSHKINA', '12', '1', '1', 375446703512, 'DANA.TOMAS@GMAIL.COM');
	   
--Inserting into children_representatives
--I was inserting the data with WHERE first_name+last_name to make it easier for reading and to using structure 'double pipe' in aducation reason. And I totally agree that it is better to insert with SSN (SSN is natural key (unique). 
INSERT INTO kindergarten.children_representatives (rep_id, child_id)
SELECT r.rep_id, c.child_id 
FROM kindergarten.representatives r, kindergarten.children c
WHERE r.first_name||r.last_name = 'MARIYAZVONOVA' AND c.first_name||c.last_name = 'EKATERINAZVONOVA';

INSERT INTO kindergarten.children_representatives (rep_id, child_id)
SELECT r.rep_id, c.child_id 
FROM kindergarten.representatives r, kindergarten.children c
WHERE r.first_name||r.last_name = 'LEONIDNIKITIN' AND c.first_name||c.last_name = 'OLGANIKITINA';

INSERT INTO kindergarten.children_representatives (rep_id, child_id)
SELECT r.rep_id, c.child_id 
FROM kindergarten.representatives r, kindergarten.children c
WHERE r.first_name||r.last_name = 'DANATOMAS' AND c.first_name||c.last_name = 'TOMASGROVSON';

INSERT INTO kindergarten.children_representatives (rep_id, child_id)
SELECT r.rep_id, c.child_id 
FROM kindergarten.representatives r, kindergarten.children c
WHERE r.first_name||r.last_name = 'ANDREYKOSHKIN' AND c.first_name||c.last_name = 'GLEBNIKOLAEV';

INSERT INTO kindergarten.children_representatives (rep_id, child_id)
SELECT r.rep_id, c.child_id 
FROM kindergarten.representatives r, kindergarten.children c
WHERE r.first_name||r.last_name = 'SVETLANANIKOLAEVA' AND c.first_name||c.last_name = 'GLEBNIKOLAEV';

UPDATE kindergarten.representatives  --There was a mistake
SET last_name = 'GROVSON'
WHERE first_name||last_name = 'DANATOMAS';

--Inserting into benefits
INSERT INTO kindergarten.benefits (title, benefit_size)
VALUES ('LARGE FAMILY', 10.67),
	   ('SINGLE-PARENT FAMILY', 10.67),
	   ('LOW INCOME', 12.00);
	  
	  
--Inserting into children_benefits	  
INSERT INTO kindergarten.children_benefits (benefit_id, child_id)
SELECT b.benefit_id, c.child_id 
FROM kindergarten.benefits b, kindergarten.children c
WHERE b.title = 'LOW INCOME' AND c.first_name||c.last_name = 'GLEBNIKOLAEV'

UNION

SELECT b.benefit_id, c.child_id 
FROM kindergarten.benefits b, kindergarten.children c
WHERE b.title = 'SINGLE-PARENT FAMILY' AND c.first_name||c.last_name = 'TOMASGROVSON';

--Inserting into diets	
INSERT INTO kindergarten.diets (diet_title)
VALUES ('NO PEANUT IS ALLOWED'),
	   ('LOW SUGAR'),
	   ('NO CITRUS IS ALLOWED');
	  
--Inserting into children_diets
INSERT INTO kindergarten.children_diets (diet_id, child_id)
SELECT d.diet_id , c.child_id 
FROM kindergarten.diets d, kindergarten.children c
WHERE d.diet_title = 'NO PEANUT IS ALLOWED' AND c.first_name||c.last_name = 'EKATERINAZVONOVA'

UNION 

SELECT d.diet_id , c.child_id 
FROM kindergarten.diets d, kindergarten.children c
WHERE d.diet_title = 'LOW SUGAR' AND c.first_name||c.last_name = 'EKATERINAZVONOVA'

UNION

SELECT d.diet_id , c.child_id 
FROM kindergarten.diets d, kindergarten.children c
WHERE d.diet_title = 'LOW SUGAR' AND c.first_name||c.last_name = 'OLGANIKITINA'   

UNION

SELECT d.diet_id , c.child_id 
FROM kindergarten.diets d, kindergarten.children c
WHERE d.diet_title = 'NO CITRUS IS ALLOWED' AND c.first_name||c.last_name = 'OLGANIKITINA';

--Inserting into staff_position 
INSERT INTO kindergarten.staff_position (position_title)
VALUES ('HEAD'),
	   ('TEACHER'),
	   ('TEACHER HELPER'),
	   ('GUARD'),
	   ('ACCOUNTANT'),
	   ('PHYSICAL EDUCATION TEACHER'),
	   ('DOCTOR'),
	   ('PSYCHOLOGIST'),
	   ('ARTIST');
	  
--Inserting into staff 
ALTER TABLE kindergarten.staff  --I have not realize how to alter column to GENERATED ALWAYS AS.
DROP COLUMN email;

ALTER TABLE kindergarten.staff
ADD COLUMN email VARCHAR GENERATED ALWAYS AS (first_name||'.'||last_name||'.'||staff_id::TEXT||'@KINDERGARTEN.COM') STORED NOT NULL;

INSERT INTO kindergarten.staff (first_name, last_name, patro_name, date_of_birth, document_type, ssn, position_id, zip, place, street, house, house_index, apartment, tel_number)
SELECT 'ALEKSANDRA', 'VYSOTSKAYA', 'STANOSLAVOVNA', '1980-03-15', 'PASSPORT', 'RT4856908767789', sp.position_id, '212000', 'MOGILEV', '30 LET POBEDY', '12', NULL, '87', 375292876556
FROM kindergarten.staff_position sp
WHERE sp.position_title = 'HEAD';

INSERT INTO kindergarten.staff (first_name, last_name, patro_name, date_of_birth, document_type, ssn, position_id, zip, place, street, house, house_index, apartment, tel_number)
SELECT 'EVGENIY', 'RADZINSKIY', 'STEPANOV', '1994-06-21', 'PASSPORT', 'RB5556908767789', sp.position_id, '212008', 'MOGILEV', 'KRUPSKOY', '137', '3', '176', 375441287564
FROM kindergarten.staff_position sp
WHERE sp.position_title = 'TEACHER';

INSERT INTO kindergarten.staff (first_name, last_name, patro_name, date_of_birth, document_type, ssn, position_id, zip, place, street, house, house_index, apartment, tel_number)
SELECT 'STEPAN', 'RADZINSKIY', 'NIKOLAEVICH', '1990-02-20', 'PASSPORT', 'RQ5678708767789', sp.position_id, '212008', 'MOGILEV', 'KRUPSKOY', '137', '3', '176', 375441287565
FROM kindergarten.staff_position sp
WHERE sp.position_title = 'TEACHER';

INSERT INTO kindergarten.staff (first_name, last_name, patro_name, date_of_birth, document_type, ssn, position_id, zip, place, street, house, house_index, apartment, tel_number)
SELECT 'NINA', 'NIKOLAEVA', 'NIKOLAEVNA', '1976-02-20', 'PASSPORT', 'AS5678708767789', sp.position_id, '212008', 'MOGILEV', 'KRUPSKOY', '12', NULL, '1', 375297685465
FROM kindergarten.staff_position sp
WHERE sp.position_title = 'TEACHER';

INSERT INTO kindergarten.staff (first_name, last_name, patro_name, date_of_birth, document_type, ssn, position_id, zip, place, street, house, house_index, apartment, tel_number)
SELECT 'SVETLANA', 'ZAYCEVA', 'PETROVNA', '1999-08-11', 'RESIDENCE PERMIT', '6754SQ-67854', sp.position_id, '212038', 'MOGILEV', 'DIMITROVA', '132', NULL, '93', 375447896540
FROM kindergarten.staff_position sp
WHERE sp.position_title = 'PHYSICAL EDUCATION TEACHER';


--Inserting into groups_staff
INSERT INTO kindergarten.groups_staff (group_id, staff_id)
SELECT kg.group_id, s.staff_id 
FROM kindergarten.kind_groups kg, kindergarten.staff s
WHERE kg.group_title = 'RPE-JUNIOR A' AND s.first_name||s.last_name = 'EVGENIYRADZINSKIY';

INSERT INTO kindergarten.groups_staff (group_id, staff_id)
SELECT kg.group_id, s.staff_id 
FROM kindergarten.kind_groups kg, kindergarten.staff s
WHERE kg.group_title = 'JUNIOR A' AND s.first_name||s.last_name = 'STEPANRADZINSKIY';

INSERT INTO kindergarten.groups_staff (group_id, staff_id)
SELECT kg.group_id, s.staff_id 
FROM kindergarten.kind_groups kg, kindergarten.staff s
WHERE kg.group_title = 'INTERNATIONAL' AND s.first_name||s.last_name = 'NINANIKOLAEVA';

--Inserting into courses
INSERT INTO kindergarten.courses (courses_title)
VALUES ('MATH'),
	   ('ENGLISH'),
	   ('LOGIC');
	  
--Inserting into courses_staff
INSERT INTO kindergarten.courses_staff (course_id , staff_id)
SELECT c.course_id , s.staff_id 
FROM kindergarten.courses c, kindergarten.staff s
WHERE c.courses_title = 'MATH' AND s.first_name||s.last_name = 'EVGENIYRADZINSKIY';

INSERT INTO kindergarten.courses_staff (course_id , staff_id)
SELECT c.course_id , s.staff_id 
FROM kindergarten.courses c, kindergarten.staff s
WHERE c.courses_title = 'ENGLISH' AND s.first_name||s.last_name = 'STEPANRADZINSKIY';

INSERT INTO kindergarten.courses_staff (course_id , staff_id)
SELECT c.course_id , s.staff_id 
FROM kindergarten.courses c, kindergarten.staff s
WHERE c.courses_title = 'LOGIC' AND s.first_name||s.last_name = 'NINANIKOLAEVA';

--Inserting into children_courses
INSERT INTO kindergarten.children_courses (course_id, child_id)
SELECT cr.course_id, ch.child_id 
FROM kindergarten.courses cr, kindergarten.children ch
WHERE cr.courses_title = 'ENGLISH' AND ch.first_name||ch.last_name = 'EKATERINAZVONOVA';

INSERT INTO kindergarten.children_courses (course_id, child_id)
SELECT cr.course_id, ch.child_id 
FROM kindergarten.courses cr, kindergarten.children ch
WHERE cr.courses_title = 'MATH' AND ch.first_name||ch.last_name = 'TOMASGROVSON';

--Inserting into payments 
INSERT INTO kindergarten.payments (child_id, payment_period, amount)
SELECT ch.child_id, '2021-01-15', 102.80
FROM kindergarten.children ch
WHERE ch.first_name||ch.last_name = 'TOMASGROVSON';

INSERT INTO kindergarten.payments (child_id, payment_period, amount)
SELECT ch.child_id, '2021-01-15', 98.70
FROM kindergarten.children ch
WHERE ch.first_name||ch.last_name = 'EKATERINAZVONOVA';



-- Task 5 Alter all tables and add 'record_ts' field to each table. Make it not null and set its default value to current_date. Check that the value as been set for existing rows.

ALTER TABLE kindergarten.children 
ADD record_ts DATE DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.children
ALTER COLUMN record_ts SET NOT NULL; --The first my thoughts was "Why do we need NOT NULL with DEFAULT?" but then I analyzed and understood that it is possible to insert NULL value despite the fact that there DEFAULT.

ALTER TABLE kindergarten.benefits 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.children_benefits 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.payments 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.representatives 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.children_representatives 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.children_diets 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.diets 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.kind_groups 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.groups_staff 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.staff_position 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.children_courses 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.courses 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.courses_staff 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

ALTER TABLE kindergarten.staff 
ADD record_ts DATE NOT NULL DEFAULT CURRENT_DATE;

--Changing some not appropriate data types

ALTER TABLE kindergarten.representatives 
ALTER COLUMN tel_number TYPE VARCHAR;

ALTER TABLE kindergarten.staff 
ALTER COLUMN tel_number TYPE VARCHAR;



--After everything I was trying to create new 'CHECK' constrainment to look at 'CHECK' behaviour with existing rows. That is a reason why I did it at the end.
--ALTER TABLE kindergarten.payments 
--ADD CHECK ( extract(year FROM kindergarten.payments.payment_date) > 2021);

ALTER TABLE kindergarten.representatives 
ADD CHECK (tel_number ~ '^[+][0-9]+$' OR tel_number ~ '[0-9]+$'); --We are able to create more specific checking if customer would like it.

ALTER TABLE kindergarten.staff 
ADD CHECK (tel_number ~ '^[+][0-9]+$' OR tel_number ~ '[0-9]+$'); 



-- Setting the kindergarten schema
SET SEARCH_path TO kindergarten;