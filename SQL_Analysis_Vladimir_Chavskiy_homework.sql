--TASK 4. Prepare sql queries to resolve each of these questions. Visualize the results using any tool (e.g. Microsoft Excel).
--Present the result as a single sql file (name of the sql file should be SQL_Analysis_Name_Surname_homework)

--I used Tableau to visualize data.It is possible to aggregate data with PostgreSQL or we can aggregate data in Tableau.
--As I have an option I left two queries for each business question.

--Represent number of sales for each sale channel and compare (visually) gross profit for each sales channel for 1998-2001
COPY (SELECT t.calendar_year, c.channel_desc, s.amount_sold --non aggregated result
	  FROM sales s 
      JOIN times t ON s.time_id = t.time_id 
	  JOIN channels c ON s.channel_id = c.channel_id) TO 'D:/sales_channels.csv';
	  
COPY (SELECT t.calendar_year, c.channel_desc, SUM(s.amount_sold), count(*) AS quantity --aggregated result
	  FROM sales s 
      JOIN times t ON s.time_id = t.time_id 
	  JOIN channels c ON s.channel_id = c.channel_id
	  GROUP BY t.calendar_year, c.channel_desc) TO 'D:/sales_channels_aggregated.csv';

	 
	 
--Represent an age/gender histogram (one for all regions) and represent gender percentage for each region for 1998-2001
	 COPY (SELECT c.cust_gender, EXTRACT(YEAR FROM current_date) - c.cust_year_of_birth, c2.country_name --non aggregated result. I used this result to create charts
	  FROM customers c
      JOIN countries c2 ON c.country_id = c2.country_id) TO 'D:/gender_age_country.csv';
      
COPY (SELECT c.cust_gender, c2.country_name, count(*) --aggregated by gender/country result
	  FROM customers c
      JOIN countries c2 ON c.country_id = c2.country_id
      GROUP BY c.cust_gender, c2.country_name) TO 'D:/gender_country_aggregated.csv';
      
COPY (SELECT c.cust_gender, count(*) --aggregated by gender result
	  FROM customers c
      JOIN countries c2 ON c.country_id = c2.country_id
      GROUP BY c.cust_gender) TO 'D:/gender_aggregated.csv';

     

--Represent revenue, COGS and gross profit for each country for 2001
COPY (SELECT p.unit_cost, p.unit_price, c2.country_name, t2.calendar_year  --non aggregated data. I used this result to create charts
	  FROM profits p    --I would have to join several tables with filter to gather all needed data if there was not profits VIEW
      JOIN customers c ON p.cust_id = c.cust_id
      JOIN countries c2 ON c.country_id = c2.country_id
      JOIN times t2 ON p.time_id = t2.time_id) TO 'D:/cost_price.csv';
     
COPY (SELECT sum(p.unit_cost) AS cogs, sum(p.unit_price) - sum(p.unit_cost) AS profit, sum(p.unit_price) AS revenue, c2.country_name  --aggregated by country data for 2001.
	  FROM profits p
      JOIN customers c ON p.cust_id = c.cust_id
      JOIN countries c2 ON c.country_id = c2.country_id
      JOIN times t2 ON p.time_id = t2.time_id
      WHERE t2.calendar_year = 2001
      GROUP BY c2.country_name) TO 'D:/cost_price_aggregated.csv'; 
     
     
     
--Represent number of sales and revenue by days of week in 2001
COPY (SELECT s.amount_sold, t.day_number_in_week  --non aggregated data. I used this result to create charts
	  FROM sales s
      JOIN times t ON s.time_id = t.time_id
      WHERE t.calendar_year = 2001) TO 'D:/sales_day_num_week.csv';
     
COPY (SELECT count(*), sum(s.amount_sold), t.day_number_in_week  --aggregated data by day of week.
	  FROM sales s
      JOIN times t ON s.time_id = t.time_id
      WHERE t.calendar_year = 2001
      GROUP BY t.day_number_in_week) TO 'D:/sales_day_num_week_aggregated.csv';
