--TASK 4. Prepare sql queries to resolve each of these questions. Visualize the results using any tool (e.g. Microsoft Excel).
--Present the result as a single sql file (name of the sql file should be SQL_Analysis_Name_Surname_homework)

--I used Tableau to visualize data.It is possible to aggregate data with PostgreSQL or we can aggregate data in Tableau.
--As I have an option I left two queries for each business question.

--Represent number of sales for each sale channel and compare (visually) gross profit for each sales channel for 1998-2001
COPY (SELECT t.calendar_year, c.channel_desc, s.amount_sold --non aggregated result
	  FROM sales s 
      JOIN times t ON s.time_id = t.time_id 
	  FULL JOIN channels c ON s.channel_id = c.channel_id) TO 'D:/sales_channels.csv' DELIMITER ',' CSV HEADER;

COPY (SELECT calendar_year, channel_desc, COALESCE (SUM(amount_sold), 0), count(time_id) AS quantity --aggregated result
	  FROM sales s
	  RIGHT JOIN (
			  SELECT DISTINCT t.calendar_year, c.channel_desc, c.channel_id 
		      FROM times t 
			  CROSS JOIN channels c                                                  --CROSS lets us to get data about all periods and channels even if there are no sales
			  WHERE t.calendar_year IN (1998, 1999, 2000, 2001)) AS years_channels
	  			  ON s.channel_id = years_channels. channel_id AND EXTRACT (YEAR FROM s.time_id) = years_channels.calendar_year
	  GROUP BY calendar_year, channel_desc
	  ORDER BY calendar_year, channel_desc) TO 'D:/sales_channels_aggregated.csv' DELIMITER ',' CSV HEADER;
	 
COPY (SELECT t.calendar_year, c.channel_desc, COALESCE (SUM(s.amount_sold),0) --aggregated result
	  FROM sales s 
      JOIN times t ON s.time_id = t.time_id 
	  FULL JOIN channels c ON s.channel_id = c.channel_id --Query return channel only one time even if there are no sales in a chennel in 2,3 or 4 periond/yers
	  GROUP BY t.calendar_year, c.channel_desc
	  ORDER BY 1,2) TO 'D:/sales_channels_aggregated_ap2.csv' DELIMITER ',' CSV HEADER;;

	 
	 
--Represent an age/gender histogram (one for all regions) and represent gender percentage for each region for 1998-2001
	 COPY (SELECT c.cust_gender, EXTRACT(YEAR FROM current_date) - c.cust_year_of_birth, c2.country_name --non aggregated result. I used this result to create charts
	  FROM customers c
      JOIN countries c2 ON c.country_id = c2.country_id) TO 'D:/gender_age_country.csv' DELIMITER ',' CSV HEADER;;
      
COPY (SELECT c.cust_gender, c2.country_name, count(*) --aggregated by gender/country result
	  FROM customers c
      JOIN countries c2 ON c.country_id = c2.country_id
      GROUP BY c.cust_gender, c2.country_name) TO 'D:/gender_country_aggregated.csv' DELIMITER ',' CSV HEADER;;
      
COPY (SELECT c.cust_gender, count(*) --aggregated by gender result
	  FROM customers c
      JOIN countries c2 ON c.country_id = c2.country_id
      GROUP BY c.cust_gender) TO 'D:/gender_aggregated.csv' DELIMITER ',' CSV HEADER;;

     

--Represent revenue, COGS and gross profit for each country for 2001
COPY (SELECT p.unit_cost, p.unit_price, c2.country_name, t2.calendar_year  --non aggregated data. I used this result to create charts
	  FROM profits p    --I would have to join several tables with filter to gather all needed data if there was not profits VIEW
      JOIN customers c ON p.cust_id = c.cust_id
      JOIN countries c2 ON c.country_id = c2.country_id
      JOIN times t2 ON p.time_id = t2.time_id) TO 'D:/cost_price.csv' DELIMITER ',' CSV HEADER;;
     
COPY (SELECT sum(p.unit_cost) AS cogs, sum(p.unit_price) - sum(p.unit_cost) AS profit, sum(p.unit_price) AS revenue, c2.country_name  --aggregated by country data for 2001.
	  FROM profits p
      JOIN customers c ON p.cust_id = c.cust_id
      JOIN countries c2 ON c.country_id = c2.country_id
      JOIN times t2 ON p.time_id = t2.time_id
      WHERE t2.calendar_year = 2001
      GROUP BY c2.country_name) TO 'D:/cost_price_aggregated.csv' DELIMITER ',' CSV HEADER;; 
     
     
     
--Represent number of sales and revenue by days of week in 2001
COPY (SELECT s.amount_sold, t.day_number_in_week  --non aggregated data. I used this result to create charts
	  FROM sales s
      JOIN times t ON s.time_id = t.time_id
      WHERE t.calendar_year = 2001) TO 'D:/sales_day_num_week.csv' DELIMITER ',' CSV HEADER;;
     
COPY (SELECT count(*), sum(s.amount_sold), t.day_number_in_week  --aggregated data by day of week.
	  FROM sales s
      JOIN times t ON s.time_id = t.time_id
      WHERE t.calendar_year = 2001
      GROUP BY t.day_number_in_week) TO 'D:/sales_day_num_week_aggregated.csv' DELIMITER ',' CSV HEADER;;
