DROP FUNCTION  customer_rent_info_v2;

CREATE OR REPLACE FUNCTION customer_rent_info_v2 (client_id integer, 
											   left_boundary TEXT DEFAULT '2001-01-01', 
											   right_boundary TEXT DEFAULT '2021-06-01')
RETURNS TABLE (metric_name TEXT, metric_value TEXT)

LANGUAGE plpgsql

AS $_$

DECLARE

	left_boundary_d DATE;
	right_boundary_d DATE;

BEGIN
	
	left_boundary_d := to_date(left_boundary, 'YYYY-MM-DD'); --We have discussed this approach at QA and I decided to use it
	right_boundary_d := to_date(right_boundary, 'YYYY-MM-DD');
	
	IF NOT EXISTS (SELECT * FROM customer c2 WHERE c2.customer_id = client_id) THEN  --That is RAISE in case if there is not a customer with given ID; 
		RAISE NOTICE 'There is not a customer with given ID';
		RETURN;
	END IF;

	
	RETURN query
	
	SELECT * FROM (
	SELECT 'customer''s info',
		   UPPER(LEFT(c.first_name,1))||LOWER(SUBSTR(c.first_name,2))||' '||
		   UPPER(LEFT(c.last_name,1))||LOWER(SUBSTR(c.last_name,2))||', '||
		   LOWER(c.email)
	FROM customer c 
	WHERE c.customer_id = client_id

	UNION ALL
	
	SELECT 'num. of films rented', COALESCE(COUNT(*),0)::TEXT
	FROM rental r 
	WHERE r.rental_date BETWEEN left_boundary_d AND right_boundary_d
		 AND r.customer_id = client_id

	UNION ALL
	
	SELECT 'rented films'' titles',
			COALESCE(STRING_AGG(DISTINCT INITCAP(f.title),', '), 'There is not any rental in this period of time')::TEXT
	FROM rental r 
	JOIN inventory i ON r.inventory_id = i.inventory_id 
	JOIN film f ON i.film_id = f.film_id 
	WHERE r.rental_date BETWEEN left_boundary_d AND right_boundary_d
		 AND r.customer_id = client_id

	UNION ALL 
	
	SELECT 'num. of payments', COALESCE(COUNT(*),0)::TEXT
	FROM payment p 
	WHERE p.payment_date BETWEEN left_boundary_d AND right_boundary_d
		 AND p.customer_id = client_id
	
	UNION ALL
	
	SELECT 'customer''s info', COALESCE(SUM(p.amount), 0)::TEXT
	FROM payment p 
	WHERE p.payment_date BETWEEN left_boundary_d AND right_boundary_d
		 AND p.customer_id = client_id) asas;
	
RETURN;
END 
$_$;

SELECT * FROM customer_rent_info_v2 (5, '2017-01-31', '2017-02-10')
SELECT * FROM customer_rent_info_v2 (5, '2000-01-31', '2017-02-10')
SELECT * FROM customer_rent_info_v2 (5, '2000-01-31', '2020-02-10')
SELECT * FROM customer_rent_info_v2 (10)