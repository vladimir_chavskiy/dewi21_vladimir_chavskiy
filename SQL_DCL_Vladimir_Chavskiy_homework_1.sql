--TASK 1. Figure out what security precautions are already used in your 'dvd_rental' database; -- send description

/*
PG_SIGNAL_BACKEND lets roles with no superuser rights send signals to other backends which do not belong to superuser roles. 
The last part of a previous sentence means even if a role has membership in PG_SIGNAL_BACKEND it are not able to send signals
to superuser processes. For now it might be CANCEL query or TERMINATE session signals.

PG_READ_SERVER_FILES allows roles to read files from any space a database have access where. It allows to read with COPY
and other access file functions.

PG_WRITE_SERVER_FILES allows roles to write and copy files from any space a database have access where. It allows to write
with COPY and other access file functions.

PG_EXECUTE_SERVER_PROGRAM allows roles to execute programs on a server as an authorized in DATABASE user as COPY and other
function on a server side do.

PG_READ_ALL_STATS allows roles to read all statistic VIEWS pg_stat_<name> and use all extensions even those visible only to SUPERUSER.

PG_MONITOR allows read and execute monitoring VIEWs and functions. This role has a membership in pg_read_all_settings, pg_read_all_stats
and pg_stat_scan_tables roles.

PG_READ_ALL_SETTINGS allows roles to read all configuration variables even those visible only to SUPERUSER.

PG_STAT_SCAN_TABLES allows roles to execute monitoring functions which can lock tables with ACCESS SHARE. Please notice, it might by a
long time blocking.

POSTGRES. A new installed system always has predefined role. This role is a superuser and normally it is called postgres. We have to 
connect to DB with this role to create other roles.

The 'dvd_rental' database uses client_encoding 'UTF8'

There are two rules (pg_settings_u and pg_settings_n)about the rules for rewriting requests.
*/

